<?php

use Illuminate\Database\Seeder;

use App\Prefix as Model;
use Carbon\Carbon;


class Prefix extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $today = Carbon::now();
        $c = "09";
        $data = [
            // [ 'CarrierName' => 'Smart', 'Status' => 1, 'created_at' => $today ],
            // [ 'CarrierName' => 'Globe', 'Status' => 1, 'created_at' => $today ]
            [ 'Code' => '0817', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0973', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '09173', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '09178', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '09175', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '09253', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '09257', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '09176', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '09255', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '09258', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0922', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0931', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0941', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0923', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0932', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0942', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0924', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0933', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0943', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0925', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0934', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0944', 'CarrierID' => 3, 'created_at' => $today ],
            [ 'Code' => '0907', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0912', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0946', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0909', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0930', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0948', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0910', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0938', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0950', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0813', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0913', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0919', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0928', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0947', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0981', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0908', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0914', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0920', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0929', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0949', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0989', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0911', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0918', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0921', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0939', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0970', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0998', 'CarrierID' => 1, 'created_at' => $today ],
            [ 'Code' => '0904', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0916', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0935', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0965', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0976', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0994', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0905', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0917', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0936', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0966', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0977', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0995', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0906', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0926', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0945', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0967', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0978', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0997', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0915', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0927', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0956', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0975', 'CarrierID' => 2, 'created_at' => $today ],
            [ 'Code' => '0979', 'CarrierID' => 2, 'created_at' => $today ],
            
        ];

        Model::insert($data);
    }
}
