<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserInfo;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->username = 'admin';
        $user->email = 'admin@gateway.com';
        $user->password = bcrypt('admin123');
        $user->type = 1;
        $user->is_active = 1;
        $user->save();
        
        // $userInfo = UserInfo;
        // $userInfo->CompanyName = '';
        // $userInfo->UserID = $user->id;
        // $userInfo->Interval = 0;
        // $userinfo->Credits = 0;
        // $userInfo->save();
    }
}
