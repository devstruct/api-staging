<?php

use Illuminate\Database\Seeder;
use App\ClientTransaction;
use App\Carrier;
use Acme\Common\DataFields\Carrier as CarrierDataField;
use Acme\Common\DataFields\ClientTransaction as ClientTransactionDataField;

class MassUpdateCarrierIDAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carrier = Carrier::where(CarrierDataField::CARRIER_NAME, 'Admin')
                        ->first();
        ClientTransaction::whereIn(ClientTransactionDataField::TYPE, [0, 1, 5])
                    ->update([ClientTransactionDataField::CARRIER_ID => $carrier->CarrierID]);
    }
}
