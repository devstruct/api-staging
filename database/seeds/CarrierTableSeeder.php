<?php

use Illuminate\Database\Seeder;
use App\Carrier;
use Carbon\Carbon;

class CarrierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $today = Carbon::now();
        $data = [
            [ "CarrierID" => 1 ,'CarrierName' => 'Smart', 'Status' => 1, 'created_at' => $today ],
            [ "CarrierID" => 2 ,'CarrierName' => 'Globe', 'Status' => 1, 'created_at' => $today ],
            [ "CarrierID" => 3 ,'CarrierName' => 'Sun', 'Status' => 1, 'created_at' => $today ],
            [ "CarrierID" => 99 ,'CarrierName' => 'System', 'Status' => 1, 'created_at' => $today ]
        ];

        Carrier::insert($data);
    }
}
