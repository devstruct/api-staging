<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminUserSeeder::class);
        $this->call(CarrierTableSeeder::class);
        $this->call(Prefix::class);
        $this->call(GatewayTableSeeder::class);
        $this->call(ProductCodeTableSeeder::class);
    }
}
