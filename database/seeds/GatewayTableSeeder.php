<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Gateway;

class GatewayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $today = Carbon::now();
        $data = [
            [ 
                'Name' => 'Globe01' ,
                'CarrierID' => 2, 
                'SIM' => '09155243797', 
                'Type' => 1, 
                'Balance' => '1000000', 
                'Minimum' => 100,
                'Priority' => 1,
                'Status' => 1,
                'created_at' => $today 
            ],
            [ 
                'Name' => 'Smart01' ,
                'CarrierID' => 1, 
                'SIM' => '09997459245', 
                'Type' => 1, 
                'Balance' => '800000', 
                'Minimum' => 100,
                'Priority' => 1,
                'Status' => 1,
                'created_at' => $today 
            ]
        ];
        Gateway::insert($data);
    }
}
