<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\ProductCode;

class ProductCodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $today = Carbon::now();
        $data = [
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '15', 
                'Name' => 'Regular Load 15', 
                'Amount' => 15,
                'AmountCharged' => 14.37,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '30', 
                'Name' => 'Regular Load 30', 
                'Amount' => 30,
                'AmountCharged' => 28.07,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '50', 
                'Name' => 'Regular Load 50', 
                'Amount' => 50,
                'AmountCharged' => 46.78,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '60', 
                'Name' => 'Regular Load 60', 
                'Amount' => 60,
                'AmountCharged' => 56.14,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '100', 
                'Name' => 'Regular Load 100', 
                'Amount' => 100,
                'AmountCharged' => 93.56,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '115', 
                'Name' => 'Regular Load 115', 
                'Amount' => 115.00,
                'AmountCharged' => 107.59,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '200', 
                'Name' => 'Regular Load 200', 
                'Amount' => 200,
                'AmountCharged' => 187.12,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '300', 
                'Name' => 'Regular Load 300', 
                'Amount' => 300,
                'AmountCharged' => 280.68,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '500', 
                'Name' => 'Regular Load 500', 
                'Amount' => 500,
                'AmountCharged' => 467.80,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '1000', 
                'Name' => 'Regular Load 1000', 
                'Amount' => 1000,
                'AmountCharged' => 935.60,
                'Status' => 1 
            ],

            /* Smart Prepaid  */
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '6406', 
                'Code' => 'Text50', 
                'Name' => 'Big Unlitext 50', 
                'Amount' => 50.00,
                'AmountCharged' => 46.78,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '6406', 
                'Code' => 'Call100', 
                'Name' => 'Big Calls 100', 
                'Amount' => 100,
                'AmountCharged' => 93.56,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => 'AT30', 
                'Name' => 'All Text 30 Plus', 
                'Amount' => 30,
                'AmountCharged' => 28.07,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => '10', 
                'Name' => 'All Text 10', 
                'Amount' => 10,
                'AmountCharged' => 9.36,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => 'ALLIN99', 
                'Name' => 'All-In 99', 
                'Amount' => 99,
                'AmountCharged' => 92.62,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '343', 
                'Code' => 'AI250', 
                'Name' => 'Mega All-In 250', 
                'Amount' => 250,
                'AmountCharged' => 233.90,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '6707', 
                'Code' => 'UCT25', 
                'Name' => 'Unli Call & Text 25', 
                'Amount' => 25,
                'AmountCharged' => 23.39,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '6707', 
                'Code' => 'UCT30', 
                'Name' => 'Unli Call & Text 30', 
                'Amount' => 30,
                'AmountCharged' => 28.07,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '6707', 
                'Code' => 'UCT50', 
                'Name' => 'Unli Call & Text 50', 
                'Amount' => 50,
                'AmountCharged' => 46.78,
                'Status' => 1 
            ],

            /*Talk 'N Text*/
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'GT10', 
                'Name' => 'GaanTxt 10', 
                'Amount' => 10,
                'AmountCharged' => 9.36,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'GT20', 
                'Name' => 'GaanTxt 20', 
                'Amount' => 20,
                'AmountCharged' => 18.71,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'GA15', 
                'Name' => 'Gaan All-in-one 15', 
                'Amount' => 15,
                'AmountCharged' => 14.03,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'GA20', 
                'Name' => 'Gaan All-in-one 20', 
                'Amount' => 20,
                'AmountCharged' => 18.71,
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'GA30',  
                'Amount' => 30,
                'AmountCharged' => 28.07,
                'Name' => 'Gaan All-in-one 30',
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'GU15',  
                'Amount' => 15,
                'AmountCharged' => 14.03,
                'Name' => 'Gaan UnliTxt Plus 15',
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'GU20',  
                'Amount' => 20.00,
                'AmountCharged' => 18.71,
                'Name' => 'Gaan UnliTxt Plus 20',
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'GU30',  
                'Amount' => 30.00,
                'AmountCharged' => 28.07,
                'Name' => 'Gaan UnliTxt Plus 30',
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'TOT10',  
                'Amount' => 10.00,
                'AmountCharged' => 9.36,
                'Name' => 'Patok-O-Tex 10',
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'T20',  
                'Amount' => 20.00,
                'AmountCharged' => 18.71,
                'Name' => 'UnliTalkPlus20',
                'Status' => 1 
            ],
            [ 
                'CarrierID' => 1, 
                'RegisterTo' => '4540', 
                'Code' => 'T100',  
                'Amount' => 100.00,
                'AmountCharged' => 93.56,
                'Name' => 'UnliTalkPlus100',
                'Status' => 1 
            ],

            /* GLOBE*/
            [ 
                'CarrierID' => 2, 
                'RegisterTo' => '100', 
                'Code' => '10',  
                'Amount' => 10,
                'AmountCharged' => 9.55,
                'Name' => 'Regular Load 10',
                'Status' => 1 
            ],
        ];
        ProductCode::insert($data);
    }
}
