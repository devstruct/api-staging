<?php

use Illuminate\Database\Seeder;
use App\ClientTransaction;

class ModifyTypeValueClientTransactions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ClientTransaction::where('ProductCode', 'ADJUST CREDITS')->update(['Type' => '1', 'ProductCode' => 'REMOVE CREDITS']);
    }
}
