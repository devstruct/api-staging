<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagelogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messagelog', function (Blueprint $table) {
            $table->increments('Id');
            $table->datetime('SendTime')->nullable();
            $table->datetime('ReceiveTime')->nullable();
            $table->integer('StatusCode')->nullable();
            $table->string('StatusText')->nullable();
            $table->string('MessageTo')->nullable();
            $table->string('MessageFrom')->nullable();
            $table->text('MessageText')->nullable();
            $table->string('MessageType')->nullable();
            $table->string('MessageId')->nullable();
            $table->text('MessageGuid')->nullable();
            $table->text('MessageInfo')->nullable();
            $table->string('ErrorCode')->nullable();
            $table->text('ErrorText')->nullable();
            $table->string('Gateway')->nullable();
            $table->integer('MessageParts')->nullable();
            $table->text('MessagePDU')->nullable();
            $table->string('Connector')->nullable();
            $table->string('UserId')->nullable();
            $table->text('UserInfo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messagelog');
    }
}
