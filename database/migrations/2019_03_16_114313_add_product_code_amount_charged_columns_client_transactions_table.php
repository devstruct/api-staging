<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductCodeAmountChargedColumnsClientTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_transactions', function(Blueprint $table){
            $table->string('ProductCode')->after('RecipientNumber')->nullable();
            $table->double('AmountCharged')->after('Amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_transactions', function(Blueprint $table){
            $table->dropColumn('ProductCode');
            $table->dropColumn('AmountCharged');
        });
    }
}
