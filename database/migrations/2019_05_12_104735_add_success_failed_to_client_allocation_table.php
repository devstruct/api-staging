<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSuccessFailedToClientAllocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_allocations', function (Blueprint $table) {
            //
            $table->integer('Failed')->after('EndDate')->default(0);
            $table->integer('Success')->after('EndDate')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_allocations', function (Blueprint $table) {
            //
            $table->dropColumn('Failed');
            $table->dropColumn('Success');
        });
    }
}
