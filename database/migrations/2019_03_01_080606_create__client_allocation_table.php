<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientAllocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_allocations', function (Blueprint $table) {
            $table->increments('AllocationID');
            $table->integer('ClientID');
            $table->integer('Interval');
            $table->double('Budget');
            $table->double('Consumed');
            $table->double('Rate');
            $table->dateTime('StartDate');
            $table->dateTime('EndDate');
            $table->integer('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_allocations');
    }
}
