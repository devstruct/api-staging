<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetStartEndDateNullableClientAllocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_allocations', function(Blueprint $table)
        {
            $table->dateTime('StartDate')->nullable()->default(null)->change();
            $table->dateTime('EndDate')->nullable()->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_allocations', function(Blueprint $table)
        {
            $table->dateTime('StartDate')->change();
            $table->dateTime('EndDate')->change();
        });
    }
}
