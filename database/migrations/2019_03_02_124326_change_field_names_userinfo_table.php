<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldNamesUserinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userinfo', function (Blueprint $table) {
            $table->renameColumn('id', 'UserInfoID');
            $table->renameColumn('userId', 'UserID');
            $table->renameColumn('type', 'Type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('userinfo', function (Blueprint $table) {
            $table->renameColumn('UserInfoID', 'id');
            $table->renameColumn('UserID', 'userId');
            $table->renameColumn('Type', 'type');
        });
    }
}
