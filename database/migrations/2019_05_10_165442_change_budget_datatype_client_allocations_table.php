<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBudgetDatatypeClientAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('client_allocations', function (Blueprint $table) {
            //
            $table->decimal('Budget' , 11 , 2)->change();
            $table->decimal('Consumed' , 11 , 2)->change();
            $table->decimal('Rate' , 11 , 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
