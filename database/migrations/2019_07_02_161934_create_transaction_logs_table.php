<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_logs', function (Blueprint $table) {
            $table->increments('TransactionLogID');
            $table->integer('TransactionID');
            $table->decimal('Charged' , 11 , 2);
            $table->decimal('GSMBalance' , 11 , 2);
            $table->integer('GatewayID');
            $table->decimal('Discrepancy' , 11 , 2);
            $table->integer('Status');
            $table->integer('IsRead');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_logs');
    }
}
