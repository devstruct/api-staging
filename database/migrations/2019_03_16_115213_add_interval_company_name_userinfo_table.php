<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntervalCompanyNameUserinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userinfo', function(Blueprint $table){
            $table->integer('Interval')->after('UserID')->default(1);
            $table->string('CompanyName')->after('UserID')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('userinfo', function(Blueprint $table){
            $table->dropColumn('Interval');
            $table->dropColumn('CompanyName');
        });
    }
}
