<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldNamesClientTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_transactions', function (Blueprint $table) {
            $table->renameColumn('id', 'TransactionID');
            $table->renameColumn('clientId', 'ClientID');
            $table->renameColumn('recipientNumber', 'RecipientNumber');
            $table->integer('carrierName')->change();
            $table->renameColumn('amount', 'Amount');
            $table->renameColumn('type', 'Type');
            $table->renameColumn('carrierName', 'CarrierID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_transactions', function (Blueprint $table) {
            $table->renameColumn('TransactionID', 'id');
            $table->renameColumn('ClientID', 'clientId');
            $table->renameColumn('RecipientNumber', 'recipientNumber');
            $table->renameColumn('CarrierID', 'carrierName');
            $table->string('CarrierID')->change();
            $table->renameColumn('Amount', 'amount');
            $table->renameColumn('Type', 'type');
        });
    }
}
