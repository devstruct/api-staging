<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messageout', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('MessageTo')->nullable();
            $table->string('MessageFrom')->nullable();
            $table->text('MessageText')->nullable();
            $table->string('MessageType')->nullable();
            $table->text('MessageGuid')->nullable();
            $table->text('MessageInfo')->nullable();
            $table->string('Gateway')->nullable();
            $table->string('UserId')->nullable();
            $table->text('UserInfo')->nullable();
            $table->integer('Priority')->nullable();
            $table->dateTime('Scheduled')->nullable();
            $table->integer('ValidityPeriod')->nullable();
            $table->tinyInteger('IsSent')->default(0);
            $table->tinyInteger('IsRead')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messageout');
    }
}
