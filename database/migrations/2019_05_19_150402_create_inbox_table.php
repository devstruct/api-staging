<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inbox', function (Blueprint $table) {
            $table->increments('Id');
            $table->datetime('SendTime')->nullable();
            $table->datetime('ReceiveTime')->nullable();
            $table->string('MessageFrom')->nullable();
            $table->string('MessageTo')->nullable();
            $table->string('SMSC')->nullable();
            $table->text('MessageText')->nullable();
            $table->string('MessageType')->nullable();
            $table->integer('MessageParts')->nullable();
            $table->text('MessagePDU')->nullable();
            $table->string('Gateway')->nullable();
            $table->string('UserId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inbox');
    }
}
