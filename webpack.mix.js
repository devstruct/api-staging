const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/admin.scss', 'public/css/admin')
   .sass('resources/sass/client.scss', 'public/css/client')
   .sass('resources/sass/home.scss', 'public/css')
   .sass('resources/sass/login.scss', 'public/css')
   .sass('resources/sass/laravel-pagination.scss', 'public/css')
   .sass('resources/sweetalert2/src/sweetalert2.scss', 'public/css/sweetalert.css')
   .css('node_modules/font-awesome/css/font-awesome.css', 'public/css');


mix.copy('node_modules/font-awesome/fonts', 'public/fonts/');