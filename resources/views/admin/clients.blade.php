@extends('admin.layout.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                        <div class="row">
                            <div class="col-sm-2">
                                <h4 class="card-title ">Clients</h4>
                            </div>
                            {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
                            <div class="col-sm-4">
                                {{ Form::open([ 'class' => 'form-inline']) }}
                                    <div class="col-sm-12 margin-bot-4">
                                        {{ Form::label('Start Date', '', ['class' => 'left-indent width-20']) }}
                                        {{ Form::date('dates', null, [ 'id' => 'start-date', 'class' => 'form-control left-indent', 'readonly' => false]) }}
                                    </div>
                                    <div class="col-sm-12 margin-bot-4">
                                        {{ Form::label('End Date', '', ['class' => 'left-indent width-20']) }}
                                        {{ Form::date('dates', null, [ 'id' => 'end-date', 'class' => 'form-control left-indent', 'readonly' => false]) }}
                                    </div>
                                    <div id="div-filter" class="col-sm-12">
                                        <button type="button" id="button-filter" class="btn btn-labeled btn-sm custom-primary-btn" data-toggle="modal">
                                            <span class="btn-label">Apply Filter</span>
                                        </button>
                                    </div>
                                {{ Form::close() }}
                        </div>
                            <div class="col-sm-6">
                                <div class="custom-card-wrapper">
                                    <div class="custom-card">
                                        <div class="card-header">
                                            <p><strong> Total Credits Loaded to All</strong></p>
                                            <p id="total-loaded-credits" class="right-align no-margin">0</p>
                                        </div>
                                    </div>
                                    <div class="custom-card">
                                        <div class="card-header">
                                            <p><strong> Total Successful Transaction</strong></p>
                                            <p class="right-align no-margin" id="total-successful-transactions" >0</p>
                                        </div>
                                    </div>
                                    <div class="custom-card">
                                        <div class="card-header">
                                            <p><strong> Total Failed Transaction</strong></p>
                                            <p class="right-align no-margin" id="total-failed-transactions">0</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <div class="table-responsive">
                                <button type="button" class="btn btn-labeled btn-sm right-align custom-primary-btn" data-toggle="modal" onClick="AddModuleForm()">
                                    <span class="btn-label">Add client</span>
                                </button>
                                <table id="list-table" class="table custom-table">
                                    <thead class=" text-primary">
                                    <th>Company Name</th>
                                    <th>Username</th>
                                    <th>Last Login</th>
                                    <th>Credits Loaded</th>
                                    <th>Credits Remaining</th>
                                    <th>Successful Transactions</th>
                                    <th>Failed Transactions</th>
                                    <th width="8%">Action</th>
                                    </thead>
                                    <tbody id="list-result"></tbody>
                                </table>
                                <nav id="pagination" aria-label="Client table navigation">
                                    <ul id="client-pagination" class="pagination justify-content-end lp-pagination">
                                    </ul> 
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
<!-- Modal -->
@include('admin.add-client-modal')
@include('admin.credentials-modal')
@endsection
@section('scripts')
    <script src="{{ asset('js/clients.js') }}"></script>
    <script src="{{ asset('js/clients.UI.js') }}"></script>
@endsection
@section('jquery-tmpl')
    <script type="text/x-jQuery-tmpl" id="list-tmpl">
        <tr>
            <td>${ info.CompanyName }</td>
            <td>${ username }</td>
            <td>${ last_login }</td>
            <td>
                ${ loaded }
            </td>
            <td>
                 ${ balance }
            </td>
            <td>
                @{{if active_allocation != null }}
                    ${ active_allocation.Success }
                @{{else}}
                    0
                @{{/if}}
            </td>
            <td>
                @{{if active_allocation != null }}
                    ${ active_allocation.Failed }
                @{{else}}
                    0
                @{{/if}}
            </td>
            <td>
                {{-- <button type="button" data-id="${ id }" class="btn btn-raised btn-warning btn-sm" onClick="View(${ id })">Edit</button>
                <button type="button" data-id="${ id }" class="btn btn-raised btn-danger btn-sm" onClick="Remove(${ id })">Remove</button> --}}
                {{-- <a href="{!!route('admin.client.show')!!}/${ id }">Manage</a> --}}
                <div class="btn-group">
                    <button type="button" data-id="${ id }" title="View client info and transactions" class="btn custom-btn btn-raised btn-primary btn-sm" onClick="View('{!!route('admin.client.show')!!}/${ id }')"><i class="fa fa-wrench"></i></button>
                    <button type="button" data-id="${ id }" title="View client credentials"  class="btn custom-btn btn-raised btn-warning btn-sm" onClick="ViewCredentials(${ id })"><i class="fa fa-key"></i></button>
                    {{-- <button type="button" data-id="${ id }" title="Allocation list & payments"  class="btn custom-btn btn-raised btn-warning btn-sm" onClick="GoToHistory('{!!route('admin.clientallocation.showallbyclientid')!!}/${ id }')">View Allocations</button> --}}
                    @{{if is_active == 1 }}
                    {{-- <button type="button" data-id="${ id }" title="Deactivate" class="btn custom-btn btn-raised btn-success btn-sm" onClick="Remove(${ id }, '${ info.CompanyName }')"><i class="fa fa-toggle-off"></i></button> --}}
                    @{{else}}
                    {{-- <button type="button" data-id="${ id }" title="Activate" class="btn custom-btn btn-raised btn-sm" onClick="Activate(${ id }, '${ info.CompanyName }')"><i class="fa fa-toggle-on"></i></button> --}}
                    @{{/if}}
                </div>  
            </td>
        </tr>
    </script>

    <script type="text/x-jQuery-tmpl" id="client-pagination-tmpl">
        <li class="page-item ${ mode }">
            <a class="page-link" href="${ link }">${ name }</a>
        </li>
    </script>
@endsection