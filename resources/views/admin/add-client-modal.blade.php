<div class="modal in" id="client-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="adjustModalTitle">Add Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="tips-message"></div>
            <form class="form-inline">
                <div class="form-group bmd-form-group full-width">
                    <label for="company_name" class="bmd-label-floating left-indent">Company name</label>
                    <input type="text" class="form-control full-width field vrequired" label="Company name" id="company_name">
                </div>
                <div class="form-group bmd-form-group full-width">
                        <label for="username" class="bmd-label-floating left-indent">Username</label>
                        <input type="text" class="form-control full-width field vrequired" label="Username" id="username">
                    </div>
                <div class="form-group bmd-form-group full-width">
                    <label for="email" class="bmd-label-floating left-indent">Email</label>
                    <input type="text" class="form-control full-width field vrequired" label="Email" id="email">
                </div>
                <div class="form-group">
                    <label for="password" class="bmd-label-floating left-indent">Password</label>
                    <input type="password" class="form-control field vrequired" id="password" label="Password" disabled>
                    <button type="button" class="btn-toggle-pass cust-icon" onClick="TogglePassword()">
                        <i class="fa fa-eye"></i>
                    </button>
                </div>
                {!! Form::hidden('id', isset($client) ? $client->id : null , ['id' => 'id', 'class' => 'field']) !!}
                {!! Form::hidden('type',2 , ['id' => 'type', 'class' => 'field']) !!}
                {!! Form::hidden('is_active',1 , ['id' => 'is_active', 'class' => 'field']) !!}
                <span class="form-group bmd-form-group">
                    <button id="generate-password-btn" type="button" class="btn btn-secondary btn-sm" onClick="GeneratePassword()">
                        Generate
                    </button>
                </span>
            </form>
        </div>
        <div class="modal-header" id="client-modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Credit Settings</h5>
        </div>
        <div class="modal-body">
            {{ Form::open(['class' => 'form-inline']) }}
                <div class="form-group bmd-form-group">
                    {!! Form::label('budget-lbl', 'Budget', [ 'class' => 'bmd-label-floating left-indent' ]) !!}
                    {!! Form::number('credits', 0, ['id' => 'credits', 'class' => 'form-control field vrequired', 'label' => 'Credits']) !!}
                </div>
                <div class="form-group bmd-form-group">
                    <label for="interval" class="bmd-label-floating left-indent">Interval</label>
                    {!! Form::select('interval', $options['interval'], 0, ['id' => 'interval', 'class' => 'form-control left-indent field vrequired']) !!}
                </div>
            {{ Form::close() }}
           <!--  <div class="form-group bmd-form-group">
                {!! Form::label('add-credits-lbl', 'Add Credits', [ 'class' => 'bmd-label-floating left-indent' ]) !!}
                {!! Form::number('plus_credits', 0, ['id' => 'plus_credits', 'class' => 'form-control field vrequired', 'label' => 'Add Credits']) !!}
            </div>
            <div class="form-group bmd-form-group">
                {!! Form::label('remove-credits-lbl', 'Remove Credits', [ 'class' => 'bmd-label-floating left-indent' ]) !!}
                {!! Form::number('deduct_credits', 0, ['id' => 'deduct_credits', 'class' => 'form-control field vrequired', 'label' => 'Remove Credits']) !!}
            </div> -->
            {!! Form::hidden('allocation_mode', null, ['id' => 'is_recurring', 'class'=> 'field vrequired' , 'value'=>"1"]) !!}
            <!--
            {!! Form::label('allocation-mode-lbl', 'Recurring', [ 'class' => 'bmd-label-floating left-indent' ]) !!}<br>
            <input type="radio" value="1" class="mode_radio" id="mode-on" name="allocation_mode" onclick="ChangeAllocationMode(1)">
            {!! Form::label('allocation-mode-lbl', 'On', [ 'class' => 'bmd-label-floating' ]) !!}
            <input type="radio" value="0" class="mode_radio" id="mode-off" name="allocation_mode" onclick="ChangeAllocationMode(0)">
            {!! Form::label('allocation-mode-lbl', 'Off', [ 'class' => 'bmd-label-floating' ]) !!}
            -->
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-btn">Save changes</button>
        </div>
    </div>
    </div>
</div>