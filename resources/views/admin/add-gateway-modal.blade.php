<div class="modal in" id="module-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="adjustModalTitle">Add Gateway</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="tips-message"></div>
                {!! Form::open([ 'class' => '"form-inline']) !!}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group bmd-form-group">
                            <label for="name" class="bmd-label-floating left-indent">Name</label>
                            {{ Form::text('name', null, ['id' => 'name', 'label' => 'Name', 'class' => 'form-control field vrequired']) }}
                        </div>
                        <div class="form-group bmd-form-group full-width">
                            <label for="address" class="bmd-label-floating left-indent">IP Address</label>
                            {{ Form::text('address', null, ['id' => 'address', 'label' => 'IP Address', 'class' => 'form-control field']) }}
                        </div>
                        <div class="form-group bmd-form-group full-width">
                            <label for="sim" class="bmd-label-floating left-indent">SIM Number</label>
                            {{ Form::text('sim', null, ['id' => 'sim', 'label' => 'SIM Number', 'class' => 'form-control field vrequired']) }}
                        </div>
                        <div class="form-group bmd-form-group full-width">
                            <label for="balance" class="bmd-label-floating left-indent">Balance</label>
                            {{ Form::number('balance', null, ['id' => 'balance', 'label' => 'Balance', 'class' => 'form-control field vrequired']) }}
                        </div>
                        <div class="form-group bmd-form-group full-width">
                            <label for="minimum" class="bmd-label-floating left-indent">Minimun</label>
                            {{ Form::number('minimum', null, ['id' => 'minimum', 'label' => 'Minimun', 'class' => 'form-control field vrequired']) }}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group bmd-form-group">
                            <label for="priority" class="bmd-label-floating left-indent">Priority</label>
                            {{ Form::number('priority', null, ['id' => 'priority', 'label' => 'Priority', 'class' => 'form-control field vrequired']) }}
                        </div>
                        <div class="form-group bmd-form-group">
                            <label for="carrier_id" class="bmd-label-floating left-indent">Carrier</label>
                            {{ Form::select('carrier_id', $options['carriers'], '1', ['id' => 'carrier_id', 'class' => 'form-control field vrequired']) }}
                        </div>
                        <div class="form-group bmd-form-group">
                            <label for="type" class="bmd-label-floating left-indent">Type</label>
                            {{ Form::select('type', $options['type'], '1', ['id' => 'type', 'class' => 'form-control field vrequired']) }}
                        </div>
                        <div class="form-group bmd-form-group">
                            <label for="status" class="bmd-label-floating left-indent">Status</label>
                            {{ Form::select('status', $options['status'], '1', ['id' => 'status', 'class' => 'form-control field vrequired']) }}
                        </div>
                    </div>
                    {{ Form::hidden('id', '' , ['id' => 'id', 'class' => 'field']) }}
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-btn">Save changes</button>
            </div>
        </div>
    </div>
</div>