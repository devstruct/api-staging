@extends('admin.layout.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                                <h4 class="card-title ">Payment History - {{ $client->info->CompanyName }}</h4>
                        </div>
                        <div class="card-body">
                            {{ Form::open([ 'class' => 'form-inline']) }}
                                {{ Form::hidden('client-id', $client->id, ['id' => 'client-id', 'class' => 'field' ])}}
                                {{-- {{ Form::label('Start Date') }}
                                {{ Form::text('dates', null, [ 'id' => 'start-date', 'class' => 'field form-control half-width left-indent', 'readonly' => true]) }}
                                {{ Form::label('End Date', '', ['class' => 'left-indent']) }}
                                {{ Form::text('dates', null, [ 'id' => 'end-date', 'class' => 'field form-control half-width left-indent', 'readonly' => true]) }} --}}
                            {{ Form::close() }}
                            </br>
                            <div class="table-responsive">
                                <table id="list-table" class="table custom-table">
                                    <thead class=" text-primary">
                                    <th>Allocation</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    </thead>
                                    <tbody id="list-result"></tbody>
                                </table>
                                
                                <nav id="pagination" aria-label="Client table navigation">
                                    <ul id="pagination-content" class="pagination justify-content-end lp-pagination">
                                    </ul> 
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
<!-- Modal -->
@include('admin.allocation.add-payment-modal')
@endsection
@section('scripts')
    <script src="{{ asset('js/payment.js') }}"></script>
    <script src="{{ asset('js/payment.UI.js') }}"></script>
@endsection
@section('jquery-tmpl')
    <script type="text/x-jQuery-tmpl" id="list-tmpl">
        <tr>
            <td>${ StartDate } - ${ EndDate }</td>
            <td>${ Amount }</td>
            <td>${ Date }</td>
        </tr>
    </script>
@endsection