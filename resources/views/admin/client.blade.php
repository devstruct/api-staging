@extends('admin.layout.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                {{-- <button type="button" class="btn btn-labeled btn-sm right-align custom-primary-btn" data-toggle="modal" onClick="AddModuleForm()">
                                <span class="btn-label"><i class="fa fa-plus"></i></span>
                            </button> --}}
                            {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
                            <div class="col-sm-9">
                                <div class="">
                                    <div class="custom-card">
                                        <div class="card-header">
                                            <p><strong> Total Credits Loaded</strong></p>
                                            <p id="total-loaded-credits" class="right-align no-margin">0</p>
                                        </div>
                                    </div>
                                    <div class="custom-card">
                                        <div class="card-header">
                                            <p><strong> Remaining Credits</strong></p>
                                            <p id="total-balance-credits" class="right-align no-margin">0</p>
                                        </div>
                                    </div>
                                    <div class="custom-card">
                                        <div class="card-header">
                                            <p><strong> Total Successful Transactions</strong></p>
                                            <p class="right-align no-margin" id="total-successful-transactions">0</p>
                                        </div>
                                    </div>
                                    <div class="custom-card">
                                        <div class="card-header">
                                            <p><strong> Total Failed Transactions</strong></p>
                                            <p class="right-align no-margin" id="total-failed-transactions">0</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                    <h4 class="card-title ">CLIENT: {{ $client->info->CompanyName }}</h4>
                                    <br>
                                    <button type="button" class="btn btn-labeled btn-sm left-align custom-primary-btn full-width" data-toggle="modal" onClick="Initialize({{  $client->id }})">
                                            <span class="btn-label">View settings</span>
                                    </button>
                                    <button type="button" class="btn btn-labeled btn-sm left-align custom-primary-btn full-width" data-toggle="modal" onClick="AdjustCredits();">
                                        <span class="btn-label">Adjust Credits</span>
                                    </button>
                                    <button type="button" class="btn btn-labeled btn-sm left-align custom-primary-btn full-width" data-toggle="modal" onClick="ViewCreditHistory('{{ route('admin.client.credit.history', $client->id ) }}');">
                                        <span class="btn-label">View Credits History</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body remove-padding-top">
                            
                            <div class="table-responsive">
                                    {{ Form::open(['class' => 'form-inline indent-bottom']) }}
                                        {{ Form::label('Start Date') }}
                                        {{ Form::date('dates', null, [ 'id' => 'start-date', 'class' => 'form-control half-width left-indent', 'readonly' => false]) }}
                                        {{ Form::label('End Date', '', ['class' => 'left-indent']) }}
                                        {{ Form::date('dates', null, [ 'id' => 'end-date', 'class' => 'form-control half-width left-indent', 'readonly' => false]) }}
                                        {{ Form::label('Type', '', ['class' => 'left-indent']) }}
                                        {{ Form::select('', $options['transaction_type'], null, [ 'id' => 'trans-type', 'class' => 'form-control left-indent'])}}
                                        {{ Form::label('Carrier', '', ['class' => 'left-indent']) }}
                                        {{ Form::select('', $options['carriers'], null, [ 'id' => 'trans-carrier', 'class' => 'form-control left-indent'])}}
                                        <button id="apply-filters-btn" type="button" class="btn btn-labeled btn-sm custom-primary-btn left-indent" data-toggle="modal">
                                            <span class="btn-label">Apply Filters</span>
                                        </button>
                                    {{ Form::close() }}
                                <table id="transactions-list-table" class="table custom-table">
                                    <thead class=" text-primary">
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Transaction ID</th>
                                    <th>Mobile Number</th>
                                    <th>Product Code</th>
                                    <th>Load Charged</th>
                                    <th>Amount</th>
                                    <th>Balance</th>
                                    <th>Network Transaction Code</th>
                                    <th>Carrier</th>
                                    <th>Status</th>
                                    </thead>
                                    <tbody id="transactions-list-result"></tbody>
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td align="right">Total : </td>
                                            <td><span class="blue" id="total_credits_charged">0</span></td>
                                            <td>
                                                <span class="blue" id="total_credits">0</span>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <nav id="pagination" aria-label="Client table navigation">
                                    <ul id="client-pagination" class="pagination justify-content-end lp-pagination">
                                    </ul> 
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
<!-- Modal -->
@include('admin.add-client-modal')
@include('admin.adjust-credits-modal')
@endsection
@section('scripts')
    <script src="{{ asset('js/clients.js') }}"></script>
    <script src="{{ asset('js/client-individual.UI.js') }}"></script>
@endsection
@section('jquery-tmpl')
    <script type="text/x-jQuery-tmpl" id="transactions-list-tmpl">
        <tr>
            <td>${ date }</td>
            <td>${ time }</td>
            <td><span class="${ Class }">${ TransactionID }</span></td>
            <td>${ RecipientNumber }</td>
            <td><span class="${ Class }">${ ProductCode }</span></td>
            <td><span class="${ Class }">${ AmountCharged }</span></td>
            <td><span class="${ Class }">${ Amount }</span></td>
            <td><span class="${ Class }">${ Balance }</span></td>
            <td><span class="${ Class }">${ Reference }</span></td>
            <td>${ carrier.CarrierName }</td>
            <td><span class="${ Class }">${ StatusName }</span></td>
        </tr>
    </script>
@endsection