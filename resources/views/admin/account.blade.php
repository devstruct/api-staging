@extends('admin.layout.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title ">Account</h4>
                        </div>
                        <div class="card-body">
                            <form class="form half-width">
                                <div class="form-group">
                                    <div class="form-group bmd-form-group full-width">
                                        <label for="username" class="bmd-label-floating left-indent">Username</label>
                                        <input type="text" class="form-control field vrequired" id="username" value="{!! Auth::user()->username !!}" label="Username">
                                    </div>
                                     <div class="form-group bmd-form-group full-width">
                                        <label for="email" class="bmd-label-floating left-indent">Email</label>
                                        <input type="email" class="form-control field vrequired" id="email" value="{!! Auth::user()->email !!}" label="Email">
                                    </div>
                                    <div class="form-group bmd-form-group full-width">
                                        <label for="password" class="bmd-label-floating left-indent">Password</label>
                                        <input type="password" class="form-control field" id="password" label="Password">
                                    </div>
                                    <div class="form-group bmd-form-group full-width">
                                        <label for="password_confirmation" class="bmd-label-floating left-indent">Confirm password</label>
                                        <input type="password" class="form-control field" id="password_confirmation" label="Confirm password">
                                    </div>
<!--                                     <button type="button" class="btn-toggle-pass cust-icon" onClick="TogglePassword()">
                                        <i class="fa fa-eye"></i>
                                    </button> -->
                                </div>
                                {!! Form::hidden('id', Auth::user()->id , ['id' => 'id', 'class' => 'field']) !!}
                                {!! Form::hidden('type',1 , ['id' => 'type', 'class' => 'field']) !!}
                                {!! Form::hidden('is_active',1 , ['id' => 'is_active', 'class' => 'field']) !!}
                                <span class="form-group bmd-form-group">
                                    <button type="button" class="btn btn-primary" id="save-btn">Save</button>
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
<!-- Modal -->
@endsection
@section('scripts')
    <script src="{{ asset('js/account.js') }}"></script>
    <script src="{{ asset('js/account.UI.js') }}"></script>
@endsection