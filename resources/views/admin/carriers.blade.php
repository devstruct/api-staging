@extends('admin.layout.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Carriers</h4>
                            <button type="button" class="btn btn-labeled btn-success btn-sm right-align" data-toggle="modal" onClick="AddModuleForm()">
                                <span class="btn-label"><i class="fa fa-plus"></i></span>
                            </button>
                            {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="list-table" class="table">
                                    <thead class=" text-primary">
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th width="25%">Action</th>
                                    </thead>
                                    <tbody id="list-result"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
<!-- Modal -->
    <div class="modal in" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Add Carrier</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                    <label for="formGroupExampleInput" class="bmd-label-floating left-indent">Name</label>
                    <input type="text" class="form-control vrequired" vfield="Name" id="client-name">
                    </div>
                    <div class="form-group bmd-form-group">
                    <label for="formGroupExampleInput2" class="bmd-label-floating left-indent">Email</label>
                    <input type="text" class="form-control vrequired" vfield="Email" id="client-email">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="save-btn">Save changes</button>
            </div>
        </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/clients.js') }}"></script>
    <script src="{{ asset('js/clients.UI.js') }}"></script>
@endsection
@section('jquery-tmpl')
    <script type="text/x-jQuery-tmpl" id="list-tmpl">
        <tr>
            <td>${ account.name }</td>
            <td>${ account.email }</td>
            <td>
                <button type="button" data-id="${ UserID }" class="btn btn-raised btn-warning btn-sm">Edit</button>
                <button type="button" data-id="${ UserID }" class="btn btn-raised btn-danger btn-sm">Remove</button>
            </td>
        </tr>
    </script>
@endsection