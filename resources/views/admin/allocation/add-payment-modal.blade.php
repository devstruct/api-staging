<div class="modal in" id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="form-group bmd-form-group">
                {!! Form::label('allocation-lbl', 'Allocation', []) !!}<br>
                {!! Form::hidden('id', null, ['id' => 'id', 'class' => 'field']) !!}
                {!! Form::hidden('allocation-id', null, ['id' => 'allocation-id', 'class' => 'field vrequired']) !!}
                <strong><span id="allocation-dates"></span></strong>
            </div>
            <div class="form-group bmd-form-group">
                {!! Form::label('date-lbl', 'Date', [ 'class' => 'bmd-label-floating' ]) !!}
                {!! Form::date('date', 0, ['id' => 'date', 'class' => 'form-control field vrequired', 'label' => 'Date', 'required' => true]) !!}
            </div>
            <div class="form-group bmd-form-group">
                {!! Form::label('amount-lbl', 'Amount', [ 'class' => 'bmd-label-floating' ]) !!}
                {!! Form::number('amount', 0, ['id' => 'amount', 'class' => 'form-control field vrequired', 'label' => 'Amount', 'required' => true]) !!}
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save-payment-btn">Add</button>
        </div>
    </div>
    </div>
</div>