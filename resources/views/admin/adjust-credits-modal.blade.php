<div class="modal in" id="adjust-credits-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Adjust Credits</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="form-group bmd-form-group">
                {!! Form::label('add-credits-lbl', 'Enter Amount', [ 'class' => 'bmd-label-floating left-indent' ]) !!}
                {!! Form::number('plus_credits', 0, ['id' => 'adjust-credits', 'class' => 'form-control field ', 'label' => 'Credits']) !!}
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="adjust-btn">Add</button>
        <button type="button" class="btn btn-primary" id="remove-btn">Remove</button>
        </div>
    </div>
    </div>
</div>