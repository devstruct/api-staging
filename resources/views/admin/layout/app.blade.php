<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SMS Gateway') }}</title>
    <!-- CSS Files -->
    <link href="{{ asset('css/material-dashboard-dark-v2.0.1/material-dashboard.css?v=2.1.0') }}" rel="stylesheet" />
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/laravel-pagination.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
</head>
<body class="dark-edition">
    <div id="app" class="wrapper ">
        @include('admin.layout.navigation')
        <div class="main-panel">
        <!-- Navbar -->
            @include('admin.layout.navbar')
            @yield('content')

        </div>
    </div>

    @yield('modals')
    <!--   Core JS Files   -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery/jquery.tmpl.js') }}"></script>
    <script src="{{ asset('js/material-dashboard-dark-v2.0.1/core/popper.min.js') }}"></script>
    <script src="{{ asset('js/material-dashboard-dark-v2.0.1/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('js/material-dashboard-dark-v2.0.1/core/bootstrap-material-design.min.js') }}"></script>
    {{-- <script src="https://unpkg.com/default-passive-events"></script> --}}
    
    <!-- Place this tag in your head or just before your close body tag. -->
    {{-- <script async defer src="https://buttons.github.io/buttons.js"></script> --}}
    <!--  Google Maps Plugin    -->
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> --}}
    <!-- Chartist JS -->
    <script src="{{ asset('js/material-dashboard-dark-v2.0.1/plugins/chartist.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('js/material-dashboard-dark-v2.0.1/plugins/bootstrap-notify.js') }}"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('js/material-dashboard-dark-v2.0.1/material-dashboard.js?v=2.1.0') }}"></script>
    <script src="{{ asset('js/pagination/laravel-pagination.js') }}"></script>
    <script type="text/javascript">
        var BASE_URL = '{{ config("app.url") }}';
    </script>
    @yield('scripts')
</body>
</html>

@yield('jquery-tmpl')
