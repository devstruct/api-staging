<div class="modal in" id="module-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="adjustModalTitle">Add Product Code</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="tips-message"></div>
            {!! Form::open([ 'class' => '"form-inline']) !!}
                    <div class="form-group bmd-form-group">
                        <label for="carrier_id" class="bmd-label-floating left-indent">Carrier</label>
                        {{ Form::select('carrier_id', $options['carriers'], 1, ['id' => 'carrier_id', 'class' => 'form-control field vrequired' , 'label' => 'Carrier']) }}
                    </div>
                     <div class="form-group bmd-form-group full-width">
                        <label for="registerTo" class="bmd-label-floating left-indent">Register To</label>
                        {{ Form::text('registerTo', null, ['id' => 'register_to', 'label' => 'Register To', 'class' => 'form-control field vrequired']) }}
                    </div>
                    <div class="form-group bmd-form-group full-width">
                        <label for="sim" class="bmd-label-floating left-indent">Name</label>
                        {{ Form::text('name', null, ['id' => 'name', 'label' => 'Name', 'class' => 'form-control field vrequired']) }}
                    </div>
                    <div class="form-group bmd-form-group full-width">
                        <label for="sim" class="bmd-label-floating left-indent">Code</label>
                        {{ Form::text('code', null, ['id' => 'code', 'label' => 'Code', 'class' => 'form-control field vrequired']) }}
                    </div>
                    <div class="form-group bmd-form-group full-width">
                        <label for="template" class="bmd-label-floating left-indent">USSD Template</label>
                        {{ Form::text('ussd_template', null, ['id' => 'ussd_template', 'label' => 'USSD Template', 'class' => 'form-control field']) }}
                    </div>
                    <div class="form-group bmd-form-group full-width">
                        <label for="balance" class="bmd-label-floating left-indent">Amount</label>
                        {{ Form::number('amount', null, ['id' => 'amount', 'label' => 'Amount', 'class' => 'form-control field vrequired']) }}
                    </div>
                    <div class="form-group bmd-form-group full-width">
                        <label for="minimum" class="bmd-label-floating left-indent">Amount Charged</label>
                        {{ Form::number('amountCharged', null, ['id' => 'amount_charged', 'label' => 'Amount Charged', 'class' => 'form-control field vrequired']) }}
                    </div>
                {{ Form::hidden('id', '' , ['id' => 'id', 'class' => 'field']) }}
            {!! Form::close() !!}
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-btn">Save changes</button>
        </div>
    </div>
    </div>
</div>