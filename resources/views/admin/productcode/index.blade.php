@extends('admin.layout.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title ">Product Code</h4>
                        </div>
                        <div class="card-body">
                            <button type="button" class="btn btn-labeled btn-sm right-align custom-primary-btn" data-toggle="modal" onClick="AddModuleForm()">
                                <span class="btn-label">Add Product Code</span>
                            </button>
                            <div class="table-responsive">
                                 {{ Form::open(['class' => 'form-inline indent-bottom']) }}
                                        {{ Form::label('Search') }}
                                        {{ Form::text('text', null, [ 'id' => 'search', 'class' => 'form-control half-width left-indent', 'readonly' => false]) }}
                                        {{ Form::label('Carrier', '', ['class' => 'left-indent']) }}
                                        {{ Form::select('', $options['carriers'], null, [ 'id' => 'search_carrier', 'class' => 'form-control left-indent'])}}
                                        <button id="apply-filters-btn" type="button" class="btn btn-labeled btn-sm custom-primary-btn left-indent" data-toggle="modal">
                                            <span class="btn-label">Apply Filters</span>
                                        </button>
                                    {{ Form::close() }}
                                <table id="list-table" class="table custom-table">
                                    <thead class=" text-primary">
                                    <th>Carrier</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>USSD Template</th>
                                    <th>Register To</th>
                                    <th>Amount</th>
                                    <th>Amount Charged</th>
                                    <th width="8%">Action</th>
                                    </thead>
                                    <tbody id="list-result"></tbody>
                                </table>
                                <nav id="pagination" aria-label="Client table navigation">
                                    <ul id="productcode-pagination" class="pagination justify-content-end lp-pagination">
                                    </ul> 
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
<!-- Modal -->
@include('admin.productcode.form')
@endsection
@section('scripts')
    <script src="{{ asset('js/productcode.js') }}"></script>
    <script src="{{ asset('js/productcode.UI.js') }}"></script>
@endsection
@section('jquery-tmpl')
    <script type="text/x-jQuery-tmpl" id="list-tmpl">
        <tr>
            <td>${ Carrier }</td>
            <td>${ Code }</td>
            <td>${ Name }</td>
            <td>${ USSDTemplate }</td>
            <td>${ RegisterTo }</td>
            <td>${ Amount }</td>
            <td>${ AmountCharged }</td>
            <td>
                <div class="btn-group">
                    <button type="button" data-id="${ id }" title="Edit" class="btn btn-raised btn-primary btn-sm custom-btn" onClick="Initialize(${ ProductCodeID })"><i class="fa fa-edit"></i></button>
                    <button type="button" data-id="${ id }" title="Remove" class="btn btn-raised btn-danger btn-sm custom-btn" onClick="Remove(${ ProductCodeID }, '${ Code }')"><i class="fa fa-times"></i></button>
                </div>  
            </td>
        </tr>
    </script>
@endsection