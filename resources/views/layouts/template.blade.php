<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Loading System') }}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Material Dashboard Template -->
        <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <!-- CSS Files -->
    <link href="{{ asset('css/material-dashboard-dark-v2.0.1/material-dashboard.css?v=2.1.0') }}" rel="stylesheet" />
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
</head>
<body class="dark-edition">
    <div id="app" class="wrapper ">
            @yield('content')
        <footer class="footer">
            <div class="container-fluid">
                <nav class="float-left">
                <ul>
                    <li>
                    <a href="https://www.creative-tim.com">
                        Creative Tim
                    </a>
                    </li>
                    <li>
                    <a href="https://creative-tim.com/presentation">
                        About Us
                    </a>
                    </li>
                    <li>
                    <a href="http://blog.creative-tim.com">
                        Blog
                    </a>
                    </li>
                    <li>
                    <a href="https://www.creative-tim.com/license">
                        Licenses
                    </a>
                    </li>
                </ul>
                </nav>
                <div class="copyright float-right" id="date">
                <i class="material-icons">favorite</i> by
                <a href="https://www.creative-tim.com" target="_blank">Cool DEV</a> for a better web.
                </div>
            </div>
        </footer>
    </div>
    @yield('scripts')
</body>
</html>
