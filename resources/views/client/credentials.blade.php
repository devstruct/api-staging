@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Client Credentials') }}</div>

                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">CLIENT ID:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control"  value="{{ $client->id }}" readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">CLIENT SECRET:</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="email" value="{{ $client->secret }}" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>
@endsection
