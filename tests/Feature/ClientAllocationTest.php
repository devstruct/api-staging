<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use DB;

class ClientAllocationTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private function getClientID()
    {
        $key_info = DB::table('client_allocations')
                     ->select("*")
                     ->first();

        return $key_info->ClientID;
    }

    public function testClientAllocationGetByClientID()
    {
        $id = $this->getClientID();
        $this->withoutMiddleware();
        $response = $this->json('GET', '/clientallocation/getByClientID/'.$id, []);
        $response->assertStatus(200)->assertJson([
                'error' => false,
            ]);
    }

    public function testGetLoadedCredit()
    {
        $this->withoutMiddleware();
        $response = $this->json('GET', '/clientallocation/getloadedcredit', []);
        $response->assertStatus(200)->assertJson([
                'error' => false,
            ]);
    }
}
