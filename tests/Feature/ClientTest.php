<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Acme\Repositories\UserRepository;

use DB;

class ClientTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    private $username = "testcase";

    private function getClientID()
    {
        $key_info = DB::table('users')
                     ->select("*")
                     ->where("username","!=","admin")
                     ->first();

        return $key_info->id;
    }

    private function getAllocationClientID()
    {
        $key_info = DB::table('client_allocations')
                     ->select("*")
                     ->first();

        return $key_info->ClientID;
    }

    private function getClientUsername($username)
    {
        $key_info = DB::table('users')
                     ->select("*")
                     ->where("username","=",$username)
                     ->first();

        return $key_info;
    }

    public function testList()
    {
        $this->withoutMiddleware();
        $response = $this->json('GET', '/clients/list', []);
        $response->assertStatus(200)->assertJson([
                'error' => false,
            ]);
    }

     public function testInitialize()
    {
        $id = $this->getClientID();
        $this->withoutMiddleware();
        $response = $this->json('GET', '/clients/initialize/'.$id, []);
        $response->assertStatus(200)->assertJson([
                'error' => false,
            ]);
    }

    public function testCRUD()
    {
        $this->withoutMiddleware();
        $id = "";
        $data = [
            "company_name"=> "yyyyy",
            "credits"=> "10000",
            "email"=> "yyyyyy@gmail.com",
            "id"=> $id,
            "interval"=> 2,
            "is_active"=> 1,
            "password"=> "aiy9hgln",
            "password_confirmation"=> "aiy9hgln",
            "type"=> 2,
            "username"=> $this->username
        ];

        #SAVE
         $response = $this->json('POST', '/clients/save', $data);
         $response->assertStatus(200)
            ->assertJson([
                'error' => false,
            ]);
        
         $result = json_decode($response->getContent());

         $info = $this->getClientUsername($data['username']);
         $id = $info->id;

         #INITIALIZE
         $response = $this->json('GET', '/clients/initialize/'.$id, []);
         $response->assertStatus(200)->assertJson([
            'error' => false,
         ]);

         $result = json_decode($response->getContent());

         $data["id"] = $id;
         $data["email"] = "yyyyyy@gmail.com";
        

         #UPDATE
        $response = $this->json('POST', '/clients/save', $data);
        $response->assertStatus(200)
            ->assertJson([
                'error' => false,
            ]);

        $result = json_decode($response->getContent());
        $info = $this->getClientUsername($data['username']);
        if($info->email != $data["email"])
        {
            $this->assertTrue(false);
        }
        
        #DELETE
        $response = $this->json('POST', '/clients/delete', [
            "id" => $id
        ]);
        $response->assertStatus(200)->assertJson([
            'error' => false,
        ]);

        $repo = new UserRepository;
        $repo->destroyByClientID($id);

    }

    public function testGetcredentials()
    {
        $this->withoutMiddleware();
        $id = $this->getClientID();
        $response = $this->json('POST', '/clients/getcredentials', [
            "id" => $id
        ]);

         $response->assertStatus(200)->assertJson([
            'error' => false,
        ]);
    }

    public function testAllocationlist()
    {
        $this->withoutMiddleware();
        $id = $this->getAllocationClientID();

        $credits = 100;
        $response = $this->json('POST', '/clients/adjustclientcredits', [
            "id" => $id,
            "adjust-credits" => $credits,
            "mode" => "add",
        ]);

        $response->assertStatus(200)->assertJson([
            'error' => false,
        ]);

        $credits = 0-100;
        $response = $this->json('POST', '/clients/adjustclientcredits', [
            "id" => $id,
            "adjust-credits" => $credits,
            "mode" => "add",
        ]);

        $response->assertStatus(200)->assertJson([
            'error' => false,
        ]);

    }
}
