<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APITest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testProcessTransactionSuccess()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU3OTczNmRiN2EwZWEzMDE4OGIwMjg3MzNiYTI4YmVjYjkwOWExYmYzOTcwNzYwY2YwODBjNzc5YWVhMTgxY2QzOGVlNGRiNjY1YmRkYjAyIn0.eyJhdWQiOiIyIiwianRpIjoiZTc5NzM2ZGI3YTBlYTMwMTg4YjAyODczM2JhMjhiZWNiOTA5YTFiZjM5NzA3NjBjZjA4MGM3NzlhZWExODFjZDM4ZWU0ZGI2NjViZGRiMDIiLCJpYXQiOjE1NTU0MTU2NDEsIm5iZiI6MTU1NTQxNTY0MSwiZXhwIjoxNTg3MDM4MDQxLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.C-Np3cB3aIi3DgVBE6aiXqT1GXU7C6Pz9DU4lDncco411OYAYErOt52Ihrl3qhoAheLpPqy_k69yp-Af_g6uqyWw0ntDeUIwZIXW2sOlEV5f3KU1pVBtOic1RAfRZnYPp-xUXSC5c4xAfdPnEO7wtB0kMCZeYNyuQsceBZr_FbyEYG8lzG8EOve161nK9r0jI9c7-R9iG7aVDt2X-corAakq_c1qxR1tscCZ9XGuhgekcqJaorH_FfGhL-bGQfNnkhHpkfzC91EqIj9nhGkGboUbgENJENL4l9l7My5qevI-hgWdI2J423TRmjpdIhAubO8T0YDmZDoRDfG4tBoGA7ezVSkWW0hflsIxtLuszawjXErN_cguT9mjOAQlp6bCfzUiXGEsgXgP86AZPKlK9QOThC0m7UOsfCLZd6SiumuHjckGDdnl98iWLMdzW6GbSCqtXSoK646QAeHjc14aR9NhsZ5Qnfd7jDJWwyQW6HcqES6rr1_NIa0sCjil63nA-kwZUqXxL2ttZRQ0OXsy2g3nLXXZLHte15Txg-M4s6GRIy47UIip-TcVu0fqjQChQvueXqRPETaj6M2UyAZAD6Y0-V7QtllPBJOw6cl9xdeIE_lQ4QcqMX4P9QAqQz8xaWI0I_Rb9kWrcnVaX0DB9mglZ7DjL0x7BcpcHhaIPVU',
            'Accept' => 'application/json'
        ])
        ->json('POST', '/api/transactions/process', [
            'recipient_number' => '09388257463',
            'register_to' => '343',
            'product_code' => '15',
            'test'=> 1,
        ]);

            $response->assertStatus(200)
            ->assertJson([
                'error' => false,
                'message' => "Success.",
                'errorCodes' => ["200"],
            ]);


    }

    
}
