<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCredit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clientId', 'balance', 'consumed',
    ];
}
