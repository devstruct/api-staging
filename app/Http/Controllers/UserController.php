<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Acme\Repositories\UserRepository as User;
use Acme\Common\DataResult as DataResult;
use Acme\Common\DataFields\User as DataField;
use Acme\Common\Constants as Constants;
use Acme\Common\CommonFunction;

class UserController extends Controller
{
    protected $user;
    protected $result;

    use CommonFunction;
    public function __construct(User $user, DataResult $result)
    {
        // set the model
        $this->user = $user;
        $this->result = $result;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getFields()
    {
        $result = new DataResult;
        
        try{
            $result->data = $this->user->getFields();
            $result->success = true;
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }
        return $result;
    }

    public function clients()
    {
        return view('admin.clients')->with();
    }

    public function clientList()
    {
        $result = new DataResult;

        try{
            $data = $this->user->getClients();
            $result->data = $data;
            $result->error = false;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = new DataResult;

        try{
            $data = $request->all();
            $validator = Validator::make($request->all(), $this->user->clientRules($data));
            if($validator->fails()) {
                $result->error = true;
                $result->message = $this->proccessErrorMessage($validator->errors());
                return response()->json($result);
            }

            if($data[DataField::TYPE] == 2) {
                $result->data = $this->user->saveClient($request);
            } else {
                $result->data = $this->user->save($request);
            }

            $result->message = "Success!";
            $result->error = false;
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.account');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
