<?php

namespace App\Http\Controllers;

use Auth;
use Acme\Common\Entity\User as UserEntity;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $CurrentUser = null;
    public $isLoggedIn = false;

    public function getCurrentUser()
    {
        $current_user = Auth::user();

        $user = new UserEntity;
        $user->Set(Auth::user());
        
        return $user;
    }

}
