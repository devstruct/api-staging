<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Acme\Repositories\UserRepository as User;
use Acme\Common\DataResult as DataResult;
use Acme\Common\DataFields\User as DataField;
use Acme\Common\DataFields\UserInfo as InfoDataField;
use Acme\Common\DataFields\AllocationTransaction as AllocationTransactionDataField;
use Acme\Common\Constants as Constants;
use Acme\Common\Errors as Errors;
use Acme\Common\CommonFunction;
use App\Carrier;

class ClientController extends Controller
{
    use CommonFunction;

    protected $user;
    protected $result;

    public function __construct(User $user, DataResult $result)
    {
        // set the model
        $this->user = $user;
        $this->result = $result;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $options['interval'] = config('allocation.interval');
        return view('admin.clients')->with([
            'pageTitle' => 'Clients',
            'options' => $options
        ]);
    }

    public function showCreditHistory($id)
    {
        $user = $this->user->getbyId($id);
        return View('admin.credit-history')->with('client', $user);
    }

    public function getFields()
    {   
        $result = new DataResult;

        try{
            $result->data = $this->user->getFields();
            $result->success = true;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }
        return response()->json($result, 200);
    }

    public function list(Request $request)
    {
        $result = new DataResult;

        try{

            $data = $this->user->getClientList($request);
            $result->data = $data;
            $result->error = false;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('client.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = new DataResult;

        try{
            $data = $request->all();

            $validator = Validator::make($request->all(), $this->user->clientRules($data));
            if($validator->fails()) {
                $result->error = true;
                $result->message = $this->proccessErrorMessage($validator->errors());
                return response()->json($result);
            }

            if($data[DataField::TYPE] == 2) {
                $result->data = $this->user->saveClient($request);
            } else {
                $result->data = $this->user->save($request);
            }

            $result->message = "Success!";
            $result->error = false;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result , 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->user->show($id);
        $options['carriers'] = Carrier::get()->pluck('CarrierName', 'CarrierID');
        $options['interval'] = config('allocation.interval');
        $options['transaction_type'] = config('transactions.type');
        $options['carriers'] = ['' => 'All'] + $this->objectToArray($options['carriers']);
        $options['transaction_type'] = ['' => 'All'] + $this->objectToArray($options['transaction_type']);

        return view('admin.client')->with([
            'client' => $data,
            'options' => $options
        ]);
    }

    public function initializeAccount()
    {
        $current_user = $this->getCurrentUser();
        $id = $current_user->UserID;
        $data = $this->user->show($id);
        $options['carriers'] = Carrier::get()->pluck('CarrierName', 'CarrierID');
        $options['interval'] = config('allocation.interval');
        $options['transaction_type'] = config('transactions.type');
        $options['carriers'] = ['' => 'All'] + $this->objectToArray($options['carriers']);
        $options['transaction_type'] = ['' => 'All'] + $this->objectToArray($options['transaction_type']);
        
        return view('client.transactions')->with([
            'client' => $data,
            'options' => $options
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = new DataResult;
        try{

            $row  = $this->user->show($id);
            $input = $this->user->processResult($row);
            
            $result->data = $input;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $result = new DataResult;
        try{

            $data = $request->all();
            $result->data = $this->user->deactivateAccount($data[Constants::ID]);

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result , 200);
    }

    public function activate(Request $request)
    {
        $result = new DataResult;
        try{

            $data = $request->all();
            $result->data = $this->user->activateAccount($data[Constants::ID]);

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result , 200);
    }

    public function showClientCredentials(Request $request)
    {
        $result = new DataResult;

        try{
            $data = $request->all();
            $result->data = $this->user->getClientCredentials($data[Constants::ID]);
        }catch(Exception $e) {
           $result = $this->RequestError($e);
        }

        return response()->json($result , 200);
    }

    public function allocationTransactionList(Request $request)
    {
        try{
            $data = $this->user->allocationTransactionList($request);
            $this->result->data = $data;
            $this->result->error = false;
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($this->result, 200);
    }

    public function adjustClientCredits(Request $request)
    {
        $result = new DataResult;

        try{
            $data = $request->all();
            $isAdjust = $this->user->adjustClientCredit($data);

            $result->data = null;
            $result->error = !$isAdjust;

            if($result->error)
            {
                $result->message = Errors::NO_ALLOCATION_FOUND;
            }

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }
}
