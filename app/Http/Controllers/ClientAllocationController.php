<?php

namespace App\Http\Controllers;

use Exception;
use Auth;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Acme\Common\DataResult as DataResult;
use Acme\Repositories\ClientAllocationRepository as Repository;
use Acme\Repositories\UserRepository as User;

use Acme\Common\CommonFunction;
use Acme\Common\Constants as Constants;
use Laravel\Passport\Bridge\UserRepository;

class ClientAllocationController extends Controller
{
    use CommonFunction;

    protected $repository;

    public function __construct(Repository $repository, User $user)
    {
        $this->repository = $repository;
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showByClient($id)
    {
        $client = $this->user->getByID($id);
        $allocations = $this->repository->getAsOptions($id);
        return view('admin.allocation.index')
                ->with('client', $client)
                ->with('allocations', $allocations);
    }

    public function getAsOptions($id)
    {
        $result = new DataResult;

        try{
            $data = $this->repository->getAsOptions($id);
            $result->data = $data;
        }
        catch(Exception $e)
        {
            $result = $this->RequestError($e);
        }
        
        return response()->json($result, 200);
    }

    public function getAllByClient($id)
    {
        $result = new DataResult;

        try{
            $data = $this->repository->getAllByClientID($id);
            $result->data = $data;
        }
        catch(Exception $e)
        {
           $result = $this->RequestError($e);
        }
        
        return response()->json($result, 200);
    }

    public function getByClientID($id)
    {
        $result = new DataResult;

        try{
            $data = $this->repository->getByClientID($id);
            $result->data = $data;
        }
        catch(Exception $e)
        {
            $result = $this->RequestError($e);
        }
        
        return response()->json($result, 200);
    }

    public function getLoadedCredits(Request $request)
    {
        $result = new DataResult;

        try{
            $data = $this->repository->getLoadedCredits($request);
            $result->data = $data;
        
        }catch(Exception $e) {
           $result = $this->RequestError($e);
        }
    
        return response()->json($result, 200);
    }

    public function getCurrentAllocation($client_id)
    {
        $result = new DataResult;

        try{
            $current_date = Carbon::now()->format(Constants::INPUT_DATE_FORMAT);

            if($client_id == "0")
            {
                $current_user = $this->getCurrentUser();
                $client_id = $current_user->UserID;
            }

            $data = $this->repository->getCurrentAllocation($client_id, $current_date );

            if($data)
            {
                $data["start_date"] = Carbon::parse($data->StartDate)->format(Constants::INPUT_DATE_FORMAT);
                $data["end_date"] = Carbon::parse($data->EndDate)->format(Constants::INPUT_DATE_FORMAT);
            }
            else
            {
                $data["start_date"] = Carbon::now()->format(Constants::INPUT_DATE_FORMAT);
                $data["end_date"] = Carbon::now()->format(Constants::INPUT_DATE_FORMAT);
            }
        
            $result->data = $data;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }
        
        return response()->json($result, 200);
    }
}
