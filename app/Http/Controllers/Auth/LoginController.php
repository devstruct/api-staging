<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use Illuminate\Http\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

use Acme\Common\DataResult;
use Acme\Common\Constants;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    function authenticated(Request $request, $user)
    {
        $user->update([
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);
    }

     /**
     * Issuing of Access Token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @bodyParam username string required The username of the client account. Example: UnionBank01
     * @bodyParam password string required The password of the client account. Example: xxxxx11
     * @bodyParam client_id string required The generated Client ID upon account creation. Example: 13
     * @bodyParam client_secret string required The generated Client Secret upon account creation. Example: xsdh28495jjsdjsad1i9u4jtghaiiir
     * @bodyParam grant_type string required Type of access_token to generate. Example: password
     * @bodyParam scope string required Scopes included for the generated access_token Example: 
     * Transaction Successful.
     * @response {
     *       "message": "Successfully Logged In",
     *      "data": {
     *          "token_type": "Bearer",
     *          "expires_in": 1296000,
     *          "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ1OTFiNjA1Y2MwYTc3OTM2NjUxMmVkNGEzNGNmNWI5YjQxZGNmMGU4NjNiMmIzN2M5Nzk3ODdkNjczYTg1ZmIwNTE2NjA1ZjU4YTVmM2YwIn0.eyJhdWQiOiIzIiwianRpIjoiNDU5MWI2MDVjYzBhNzc5MzY2NTEyZWQ0YTM0Y2Y1YjliNDFkY2YwZTg2M2IyYjM3Yzk3OTc4N2Q2NzNhODVmYjA1MTY2MDVmNThhNWYzZjAiLCJpYXQiOjE1NjAwNjgwOTUsIm5iZiI6MTU2MDA2ODA5NSwiZXhwIjoxNTYxMzY0MDk1LCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.Spja1I8BVja79j2euve_6VySj1FzbMdsWEpzZgdagP99WeZU7159OXhvpt0Pu-IhJmq9jaTHbZ1Z6vwvrcnQGQuRcSuwzXeeUdfBsjf3BVLfrclP8kqxqNDXOUlYKgyH0T8_-rprGUZS7d-EO-6u_uHOYqVcSSTUXdmjx7zFwnWXG06VW-t1HeV-583v0dm2zb0jS8gBqDGTMZdnNgHdlwA3rgN0Sna4dc9rS1Ny7RmOFhCUjt8-Lc2hFUnkO-AUgd85qq5zEj5y4XKrZyK_mgMBxJKq3-G-YL0mXeORq8bSrjwKSJetQoeMzfEOmfsLO7sQ5k_YH1pYOBV943LJHbq2x3nm4p-WugQgbvhvPmE5Q9WdJ4EeJQ4H89dEtYKdDnRwK07GPJa38mZuIAP9qiNm3o0sFG7eSUgh1W3HR89d-m7C9wwanAr7WlHAQVI8KAWbL2wKVT5sEoj-s3D-xicLQp8WrlhWagawikcZTlmY7deT1BdRoKftFh0FjJvNnQuvqwZv8_0TzIVsdHH6_xIwEwhY5lnOikDW6R_cvjNOv1I0KI2EFV8qW7q1AYe8uoNVD3DahPA1ULunDJHEdoUxO-FghvFbuPcDjLTYoBenuHb-7k7dBLwV1IhJ0qMPqQPLTyxw6YfYg1ZKUuUvXV6A7i6inC4L__fh_8HIU4E",
     *          "refresh_token": "def50200130f0d295536ee8abfa57273d2a3d9d4f81ce1c8859995bd0fe2773ebc3f0c0d18853f4900c33aa132e65c67d0f7a0814c64650cdd6774f1e682b196a4384780fae341609ef91381c0a35c929d62a098c5b90151052aafb7b717c4fbc3d9cd90029896234d98100e65e45d26d0431f5c8eea822a50fce834d7efd5a583996fd97360723a45c40d67125275e95967f4eb3b843ce00f269080374d4a312c597e5d382fbb41eb58d0c9a3896358f4b34c33a50139798edde02ff9f240f836ead24a23fb8c27dbeee039d70d48ec228d963f124e17c5da10981ab8185480fd28efaf300fd1ff39ddf344927c4c8931fb5f630ec1d8363a26ac27b2c429827522ccc6dfd62fb9fdc9dd6a6d7faceed6bcb70f65a685528d224ac5ad8ea7de4c040db5551ca618ab199b31bc868634e715624004ab1f6ec5854f55ad0b6c34fe112c277867a4481e01157377c79ed6302bd8736f7d028d6bc333bb25b985b0aa"
     *      },
     *      "error": false,
     *      "tags": 0,
     *      "errorCodes": []
     *  }
    * @response{
    *       "error": "invalid_credentials",
    *       "message": "The user credentials were incorrect."
    *}
    * @return \Illuminate\Http\Response
    */

    function apilogin(Request $request)
    {
        $result = new DataResult;

        try{
            $http = new Client;
            $url = config("app.guzzle_url")."/oauth/token";

            $response = $http->post($url, [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $request->input("client_id"),
                    'client_secret' => $request->input("client_secret"),
                    'username' => $request->input("username"),
                    'password' => $request->input("password"),
                    'scope' => "",
                ],
                
            ]);

            $result->data = json_decode((string) $response->getBody(), true);
            $result->message = "Successfully LoggedIn";
            $result->error = false;
        }
        catch(ClientException $e)
        {
            $result->error = true;
            $result->data = $e;
            $result->message = "Unauthorized";
            $result->errorCode = [401];
        }
        

        return response()->json($result);

    }

    function apirefreshtoken(Request $request)
    {

        $result = new DataResult;

        try{
            $http = new Client;
            $url = config("app.url")."/oauth/token";

            $headers = [
                'Content-Type' => 'application/json',
            ];

            $response = $http->post( $url, [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $request->input("refresh_token"),
                    'client_id' => $request->input("client_id"),
                    'client_secret' => $request->input("client_secret"),
                    'scope' => '',
                ],
            ]);

            $result->data = json_decode((string) $response->getBody(), true);
            $result->message = "Token Refresh";
            $result->error = false;
        }
        catch(ClientException $e)
        {
            $result->error = true;
            $result->data = $e;
            $result->message = "Unauthorized";
            $result->errorCode = [401];
        }
        

        return response()->json($result);
    }

}
