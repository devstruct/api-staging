<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCode extends Model
{
    //
    protected $table = 'product_codes';

    protected $guarded = ['ProductCodeID'];
}
