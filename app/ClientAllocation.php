<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AllocationTransaction;
use App\CreditPayment;
use Acme\Common\DataFields\ClientAllocation as DataField;
use Acme\Common\DataFields\CreditPayment as PaymentDataField;

class ClientAllocation extends Model
{
    protected $table = DataField::TABLE_NAME;

    protected $guarded = [DataField::ID];
    
    public function transactions()
    {
        return $this->hasMany(AllocationTransaction::class, DataField::ID, DataField::ID);
    }

    public function payments()
    {
        return $this->hasMany(CreditPayment::class, DataField::ID, PaymentDataField::ALLOCATION_ID);
    }

}
