<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageOut extends Model
{
    //
    protected $table = 'messageout';

    protected $guarded = ['Id'];
}
