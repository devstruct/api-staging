<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditPayment extends Model
{
    protected $table = 'credit_payments';
    protected $guarded = ['PaymentID'];
}
