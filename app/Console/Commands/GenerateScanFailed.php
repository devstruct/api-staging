<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use Acme\Common\DataFields\ClientTransaction as ClientTransaction;

use Acme\Repositories\ClientTransactionRepository as ClientTransactionRepository;

use Acme\Common\CommonFunction;
use Acme\Common\Constants;

use Carbon\Carbon;

class GenerateScanFailed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:scanfailed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan transaction and mark as failed.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $clientTransactionRepo = new ClientTransactionRepository;
        $clientTransactionRepo->searchAndMarkTransactionAsFailed();
    }
}
