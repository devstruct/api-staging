<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use Acme\Common\DataFields\MessageIn as MessageIn;
use Acme\Common\DataFields\ClientTransaction as ClientTransaction;
use Acme\Common\DataFields\ClientAllocation as ClientAllocation;
use Acme\Common\DataFields\Gateway as Gateway;
use Acme\Common\DataFields\TransactionLog as TransactionLog;

use Acme\Common\Entity\TransactionLog as TransactionLogEntity;

use Acme\Repositories\MessageInRepository as MessageInRepository;
use Acme\Repositories\ClientTransactionRepository as ClientTransactionRepository;
use Acme\Repositories\ClientAllocationRepository as ClientAllocationRepository;
use Acme\Repositories\GatewayRepository as GatewayRepository;
use Acme\Repositories\TransactionLogRepository as TransactionLogRepository;

use Acme\Common\CommonFunction;
use Acme\Common\Constants;

class GenerateScanUntaggedMessages extends Command
{
    use CommonFunction;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:scan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan all messages from network reponse';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $messageInRepo = new MessageInRepository;
        $clientTransactionRepo = new ClientTransactionRepository;
        $allocation = new ClientAllocationRepository;
        $gatewayRepo = new GatewayRepository;
        $logRepo = new TransactionLogRepository;

        $list = $messageInRepo->getUnTagged();

        foreach($list as $message) {

            $number = $this->convertToStandard($message[MessageIn::MESSAGE_TO]);
            $gateway = $gatewayRepo->getByNumber($number);

            $data = [];
            $text = $message[MessageIn::MESSAGE_TEXT];

            if($gateway[Gateway::CARRIER_ID] == Constants::GLOBE) {
                $data = $this->TextMessageParseDataGlobe($text);
            } else {
                $data = $this->TextMessageParseData($text);
            }
        
            $send_time = $message[MessageIn::SEND_TIME];
            $recipient = $data['recipient'];
            $product_code = '';
            $cost = 0 - $data['cost'];
            $reference = $data['ref'];
            $wallet = $data['wallet'];

            $transaction = $clientTransactionRepo->searchTranscation($recipient , $send_time , $product_code);
            if($transaction) {

                $messageInRepo->updateScanned($message[MessageIn::ID]);
                $client_id = $transaction[ClientTransaction::CLIENT_ID];
                $transaction_id = $transaction[ClientTransaction::TRANSACTION_ID];
                $date = $transaction[ClientTransaction::CREATED_AT];

                $current_allocation = $allocation->getCurrentAllocation($client_id, $date);

                $allocation_id = $current_allocation[ClientAllocation::ID];
                $allocation->updateNumberOfTransactions($allocation_id, 1 ,Constants::SUCCESS);
                $clientTransactionRepo->updateReferenceNumber($transaction_id , $reference);

                $gateway_id = $gateway[Gateway::GATEWAY_ID];

                $gatewayRepo->adjustBalance($gateway_id, $cost);
                $gatewayRepo->updateBalanceByID($gateway_id, $wallet);

                $log_entity = new TransactionLogEntity;

                $input = [];
                $input["transaction_id"] = $transaction_id;
                $input["charged"] = $cost;
                $input["gsm_balance"] = $wallet;
                $input["gateway_id"] = $gateway_id;
                $input["discrepancy"] = 0;
                $input["status"] = 1;
                $input["is_read"] = 0;

                $log_entity->SetData($input);
                $logRepo->save($log_entity->Serialize());

                $clientTransactionRepo->scanListByGateway($transaction_id, $gateway_id, $wallet , $cost , $allocation_id);
            }

        }

    }
}
