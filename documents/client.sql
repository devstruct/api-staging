-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.39-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for apiserver
DROP DATABASE IF EXISTS `apiserver`;
CREATE DATABASE IF NOT EXISTS `apiserver` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `apiserver`;

-- Dumping structure for table apiserver.allocation_transactions
DROP TABLE IF EXISTS `allocation_transactions`;
CREATE TABLE IF NOT EXISTS `allocation_transactions` (
  `AllocationTransactionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AllocationID` int(11) NOT NULL DEFAULT '0',
  `Credits` double NOT NULL DEFAULT '0',
  `Rate` double NOT NULL DEFAULT '1',
  `Status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`AllocationTransactionID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.allocation_transactions: ~1 rows (approximately)
DELETE FROM `allocation_transactions`;
/*!40000 ALTER TABLE `allocation_transactions` DISABLE KEYS */;
INSERT INTO `allocation_transactions` (`AllocationTransactionID`, `AllocationID`, `Credits`, `Rate`, `Status`, `created_at`, `updated_at`) VALUES
	(1, 1, 10000, 1, 5, '2019-05-21 00:36:30', '2019-05-21 00:36:30');
/*!40000 ALTER TABLE `allocation_transactions` ENABLE KEYS */;

-- Dumping structure for table apiserver.carriers
DROP TABLE IF EXISTS `carriers`;
CREATE TABLE IF NOT EXISTS `carriers` (
  `CarrierID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CarrierName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`CarrierID`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.carriers: ~4 rows (approximately)
DELETE FROM `carriers`;
/*!40000 ALTER TABLE `carriers` DISABLE KEYS */;
INSERT INTO `carriers` (`CarrierID`, `CarrierName`, `Status`, `created_at`, `updated_at`) VALUES
	(1, 'Smart', 1, '2019-05-21 00:34:52', NULL),
	(2, 'Globe', 1, '2019-05-21 00:34:52', NULL),
	(3, 'Sun', 1, '2019-05-21 00:34:52', NULL),
	(99, 'System', 1, '2019-05-21 00:34:52', NULL);
/*!40000 ALTER TABLE `carriers` ENABLE KEYS */;

-- Dumping structure for table apiserver.client_allocations
DROP TABLE IF EXISTS `client_allocations`;
CREATE TABLE IF NOT EXISTS `client_allocations` (
  `AllocationID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ClientID` int(11) NOT NULL,
  `Interval` int(11) NOT NULL,
  `Budget` decimal(11,2) NOT NULL,
  `Consumed` decimal(11,2) NOT NULL,
  `Rate` decimal(11,2) NOT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Success` int(11) NOT NULL DEFAULT '0',
  `Failed` int(11) NOT NULL DEFAULT '0',
  `Status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`AllocationID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.client_allocations: ~1 rows (approximately)
DELETE FROM `client_allocations`;
/*!40000 ALTER TABLE `client_allocations` DISABLE KEYS */;
INSERT INTO `client_allocations` (`AllocationID`, `ClientID`, `Interval`, `Budget`, `Consumed`, `Rate`, `StartDate`, `EndDate`, `Success`, `Failed`, `Status`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, 10000.00, 14.37, 1.00, '2019-05-21 00:00:00', '2019-05-21 00:00:00', 0, 0, 5, '2019-05-21 00:36:30', '2019-05-21 00:43:26');
/*!40000 ALTER TABLE `client_allocations` ENABLE KEYS */;

-- Dumping structure for table apiserver.client_credits
DROP TABLE IF EXISTS `client_credits`;
CREATE TABLE IF NOT EXISTS `client_credits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `balance` double NOT NULL DEFAULT '0',
  `consumed` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.client_credits: ~0 rows (approximately)
DELETE FROM `client_credits`;
/*!40000 ALTER TABLE `client_credits` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_credits` ENABLE KEYS */;

-- Dumping structure for table apiserver.client_transactions
DROP TABLE IF EXISTS `client_transactions`;
CREATE TABLE IF NOT EXISTS `client_transactions` (
  `TransactionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ClientID` int(11) NOT NULL,
  `RecipientNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ProductCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CarrierID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Amount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `Balance` decimal(11,2) NOT NULL DEFAULT '0.00',
  `AmountCharged` decimal(11,2) NOT NULL DEFAULT '0.00',
  `Type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '0',
  `Reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`TransactionID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.client_transactions: ~1 rows (approximately)
DELETE FROM `client_transactions`;
/*!40000 ALTER TABLE `client_transactions` DISABLE KEYS */;
INSERT INTO `client_transactions` (`TransactionID`, `ClientID`, `RecipientNumber`, `ProductCode`, `CarrierID`, `Amount`, `Balance`, `AmountCharged`, `Type`, `Status`, `Reference`, `created_at`, `updated_at`) VALUES
	(1, 2, '', 'INITIAL CREDIT', '99', 10000.00, 0.00, 0.00, '5', 5, '0', '2019-05-21 00:36:30', '2019-05-21 00:36:30'),
	(2, 2, '09388257463', '15', '1', -15.00, 9985.63, -14.37, '3', 0, '0', '2019-05-21 00:43:26', '2019-05-21 00:43:26');
/*!40000 ALTER TABLE `client_transactions` ENABLE KEYS */;

-- Dumping structure for table apiserver.gateways
DROP TABLE IF EXISTS `gateways`;
CREATE TABLE IF NOT EXISTS `gateways` (
  `GatewayID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CarrierID` int(11) NOT NULL,
  `Address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SIM` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Type` int(11) NOT NULL,
  `Balance` double NOT NULL,
  `Minimum` double NOT NULL,
  `Priority` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Is_Deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`GatewayID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.gateways: ~2 rows (approximately)
DELETE FROM `gateways`;
/*!40000 ALTER TABLE `gateways` DISABLE KEYS */;
INSERT INTO `gateways` (`GatewayID`, `Name`, `CarrierID`, `Address`, `SIM`, `Type`, `Balance`, `Minimum`, `Priority`, `Status`, `created_at`, `updated_at`, `Is_Deleted`) VALUES
	(1, 'Globe01', 2, NULL, '09155243797', 1, 1000000, 100, 1, 1, '2019-05-21 00:34:52', NULL, 0),
	(2, 'Smart01', 1, NULL, '09997459245', 1, 800000, 100, 1, 1, '2019-05-21 00:34:52', NULL, 0);
/*!40000 ALTER TABLE `gateways` ENABLE KEYS */;

-- Dumping structure for table apiserver.inbox
DROP TABLE IF EXISTS `inbox`;
CREATE TABLE IF NOT EXISTS `inbox` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SendTime` datetime DEFAULT NULL,
  `ReceiveTime` datetime DEFAULT NULL,
  `MessageFrom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SMSC` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageText` text COLLATE utf8mb4_unicode_ci,
  `MessageType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageParts` int(11) DEFAULT NULL,
  `MessagePDU` text COLLATE utf8mb4_unicode_ci,
  `Gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.inbox: ~0 rows (approximately)
DELETE FROM `inbox`;
/*!40000 ALTER TABLE `inbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbox` ENABLE KEYS */;

-- Dumping structure for table apiserver.messagein
DROP TABLE IF EXISTS `messagein`;
CREATE TABLE IF NOT EXISTS `messagein` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SendTime` datetime DEFAULT NULL,
  `ReceiveTime` datetime DEFAULT NULL,
  `MessageFrom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SMSC` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageText` text COLLATE utf8mb4_unicode_ci,
  `MessageType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageParts` int(11) DEFAULT NULL,
  `MessagePDU` text COLLATE utf8mb4_unicode_ci,
  `Gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Scanned` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.messagein: ~0 rows (approximately)
DELETE FROM `messagein`;
/*!40000 ALTER TABLE `messagein` DISABLE KEYS */;
INSERT INTO `messagein` (`Id`, `SendTime`, `ReceiveTime`, `MessageFrom`, `MessageTo`, `SMSC`, `MessageText`, `MessageType`, `MessageParts`, `MessagePDU`, `Gateway`, `UserId`, `Scanned`, `created_at`, `updated_at`) VALUES
	(1, '2019-05-21 00:44:38', NULL, 'TNTLoad', '639997459245', NULL, '21-May 00:44: Transaction was not processed. Source account has insufficient funds. Please reload your account. Ref:676622903083', NULL, NULL, NULL, NULL, '', 0, '2019-05-21 00:46:02', NULL);
/*!40000 ALTER TABLE `messagein` ENABLE KEYS */;

-- Dumping structure for table apiserver.messagelog
DROP TABLE IF EXISTS `messagelog`;
CREATE TABLE IF NOT EXISTS `messagelog` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SendTime` datetime DEFAULT NULL,
  `ReceiveTime` datetime DEFAULT NULL,
  `StatusCode` int(11) DEFAULT NULL,
  `StatusText` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageFrom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageText` text COLLATE utf8mb4_unicode_ci,
  `MessageType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageGuid` text COLLATE utf8mb4_unicode_ci,
  `MessageInfo` text COLLATE utf8mb4_unicode_ci,
  `ErrorCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ErrorText` text COLLATE utf8mb4_unicode_ci,
  `Gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageParts` int(11) DEFAULT NULL,
  `MessagePDU` text COLLATE utf8mb4_unicode_ci,
  `Connector` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserInfo` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.messagelog: ~0 rows (approximately)
DELETE FROM `messagelog`;
/*!40000 ALTER TABLE `messagelog` DISABLE KEYS */;
INSERT INTO `messagelog` (`Id`, `SendTime`, `ReceiveTime`, `StatusCode`, `StatusText`, `MessageTo`, `MessageFrom`, `MessageText`, `MessageType`, `MessageId`, `MessageGuid`, `MessageInfo`, `ErrorCode`, `ErrorText`, `Gateway`, `MessageParts`, `MessagePDU`, `Connector`, `UserId`, `UserInfo`, `created_at`, `updated_at`) VALUES
	(1, '2019-05-21 00:44:39', NULL, 200, NULL, '343', NULL, '15 09388257463', NULL, '2:343:78', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL);
/*!40000 ALTER TABLE `messagelog` ENABLE KEYS */;

-- Dumping structure for table apiserver.messageout
DROP TABLE IF EXISTS `messageout`;
CREATE TABLE IF NOT EXISTS `messageout` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MessageTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageFrom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageText` text COLLATE utf8mb4_unicode_ci,
  `MessageType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageGuid` text COLLATE utf8mb4_unicode_ci,
  `MessageInfo` text COLLATE utf8mb4_unicode_ci,
  `Gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserInfo` text COLLATE utf8mb4_unicode_ci,
  `Priority` int(11) DEFAULT NULL,
  `Scheduled` datetime DEFAULT NULL,
  `ValidityPeriod` int(11) DEFAULT NULL,
  `IsSent` tinyint(4) NOT NULL DEFAULT '0',
  `IsRead` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.messageout: ~0 rows (approximately)
DELETE FROM `messageout`;
/*!40000 ALTER TABLE `messageout` DISABLE KEYS */;
INSERT INTO `messageout` (`Id`, `MessageTo`, `MessageFrom`, `MessageText`, `MessageType`, `MessageGuid`, `MessageInfo`, `Gateway`, `UserId`, `UserInfo`, `Priority`, `Scheduled`, `ValidityPeriod`, `IsSent`, `IsRead`, `created_at`, `updated_at`) VALUES
	(1, '343', '09388257463', '15 09388257463', '', NULL, NULL, 'Smart01', '2', '0', 0, '2019-05-21 00:43:26', 0, 1, 0, '2019-05-21 00:43:26', '2019-05-21 00:43:26');
/*!40000 ALTER TABLE `messageout` ENABLE KEYS */;

-- Dumping structure for table apiserver.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.migrations: ~47 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2019_02_24_053959_create_messageout_table', 1),
	(9, '2019_02_24_091620_create_messagein_table', 1),
	(10, '2019_02_24_091947_create_messagelog_table', 1),
	(11, '2019_02_24_161420_create_client_credits_table', 1),
	(12, '2019_02_24_161624_create_client_transactions_table', 1),
	(13, '2019_02_25_033108_create_userinfo_table', 1),
	(14, '2019_03_01_022120_create_carriers_table', 1),
	(15, '2019_03_01_022307_create_gateways_table', 1),
	(16, '2019_03_01_022403_create__product_code_table', 1),
	(17, '2019_03_01_080606_create__client_allocation_table', 1),
	(18, '2019_03_02_124326_change_field_names_userinfo_table', 1),
	(19, '2019_03_02_130153_change_field_names_client_transactions_table', 1),
	(20, '2019_03_09_101004_add_is_active_column_users_table', 1),
	(21, '2019_03_09_104018_remove_type_column_to_userinfo_table', 1),
	(22, '2019_03_09_104033_add_type_column_to_users_table', 1),
	(23, '2019_03_12_151854_add_username_last_login_fields_users_table', 1),
	(24, '2019_03_16_114313_add_product_code_amount_charged_columns_client_transactions_table', 1),
	(25, '2019_03_16_115009_add_amountcharged_product_codes_table', 1),
	(26, '2019_03_16_115213_add_interval_company_name_userinfo_table', 1),
	(27, '2019_03_16_115241_remove_name_users_table', 1),
	(28, '2019_03_16_115302_add_credit_transactions_table', 1),
	(29, '2019_03_16_132551_add_credits_column_userinfo_table', 1),
	(30, '2019_03_17_172023_add_balance_column_client_transactions', 1),
	(31, '2019_03_20_103150_change_address_default_null_gateways_table', 1),
	(32, '2019_03_20_111535_add_is_deleted_column_users_table', 1),
	(33, '2019_03_20_112450_remove_unique_email_users_table', 1),
	(34, '2019_03_20_113220_add_is_deleted_column_gateways_table', 1),
	(35, '2019_03_24_123411_add_is_deleted_column_product_code_table', 1),
	(36, '2019_03_29_002013_set_start_end_date_nullable_client_allocation_table', 1),
	(37, '2019_04_16_144101_add_name_column_product_codes_table', 1),
	(38, '2019_04_16_200019_create_prefix_table', 1),
	(39, '2019_04_20_171830_change_created_at_message_in_table', 1),
	(40, '2019_05_02_202452_add_scanned_to_message_in_table', 1),
	(41, '2019_05_02_212507_add_status_to_client_transactions_table', 1),
	(42, '2019_05_04_135217_add_is_recurring_to_user_info_table', 1),
	(43, '2019_05_04_212336_add_reference_to_client_transaction', 1),
	(44, '2019_05_10_165442_change_budget_datatype_client_allocations_table', 1),
	(45, '2019_05_10_170146_change_budget_datatype_client_transaction_table', 1),
	(46, '2019_05_12_104735_add_success_failed_to_client_allocation_table', 1),
	(47, '2019_05_19_150402_create_inbox_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table apiserver.oauth_access_tokens
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.oauth_access_tokens: ~0 rows (approximately)
DELETE FROM `oauth_access_tokens`;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('ceb0d75d8a47fed1ede6b204267227e6dff258040b06eea09cb0be5febb734dd866616f87c46c4ff', 2, 1, NULL, '[]', 0, '2019-05-21 00:40:39', '2019-05-21 00:40:39', '2020-05-21 00:40:39');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table apiserver.oauth_auth_codes
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.oauth_auth_codes: ~0 rows (approximately)
DELETE FROM `oauth_auth_codes`;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table apiserver.oauth_clients
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.oauth_clients: ~1 rows (approximately)
DELETE FROM `oauth_clients`;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Union Bank', 't8mGS70lr8No89DDCZqpoaQSnlrjf1mt17v35pFu', '', 0, 1, 0, '2019-05-21 00:36:30', '2019-05-21 00:36:30');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table apiserver.oauth_personal_access_clients
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.oauth_personal_access_clients: ~0 rows (approximately)
DELETE FROM `oauth_personal_access_clients`;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table apiserver.oauth_refresh_tokens
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.oauth_refresh_tokens: ~0 rows (approximately)
DELETE FROM `oauth_refresh_tokens`;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
	('7b23e3b238e5c2fe363fc1a187b458809c830282122507eb7e0e1189308ef7842aee174fc5fe06bb', 'ceb0d75d8a47fed1ede6b204267227e6dff258040b06eea09cb0be5febb734dd866616f87c46c4ff', 0, '2020-05-21 00:40:39');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table apiserver.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table apiserver.prefix
DROP TABLE IF EXISTS `prefix`;
CREATE TABLE IF NOT EXISTS `prefix` (
  `PrefixID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CarrierID` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PrefixID`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.prefix: ~72 rows (approximately)
DELETE FROM `prefix`;
/*!40000 ALTER TABLE `prefix` DISABLE KEYS */;
INSERT INTO `prefix` (`PrefixID`, `Code`, `CarrierID`, `created_at`, `updated_at`) VALUES
	(1, '0817', 2, '2019-05-21 00:34:52', NULL),
	(2, '0973', 2, '2019-05-21 00:34:52', NULL),
	(3, '09173', 2, '2019-05-21 00:34:52', NULL),
	(4, '09178', 2, '2019-05-21 00:34:52', NULL),
	(5, '09175', 2, '2019-05-21 00:34:52', NULL),
	(6, '09253', 2, '2019-05-21 00:34:52', NULL),
	(7, '09257', 2, '2019-05-21 00:34:52', NULL),
	(8, '09176', 2, '2019-05-21 00:34:52', NULL),
	(9, '09255', 2, '2019-05-21 00:34:52', NULL),
	(10, '09258', 2, '2019-05-21 00:34:52', NULL),
	(11, '0922', 3, '2019-05-21 00:34:52', NULL),
	(12, '0931', 3, '2019-05-21 00:34:52', NULL),
	(13, '0941', 3, '2019-05-21 00:34:52', NULL),
	(14, '0923', 3, '2019-05-21 00:34:52', NULL),
	(15, '0932', 3, '2019-05-21 00:34:52', NULL),
	(16, '0942', 3, '2019-05-21 00:34:52', NULL),
	(17, '0924', 3, '2019-05-21 00:34:52', NULL),
	(18, '0933', 3, '2019-05-21 00:34:52', NULL),
	(19, '0943', 3, '2019-05-21 00:34:52', NULL),
	(20, '0925', 3, '2019-05-21 00:34:52', NULL),
	(21, '0934', 3, '2019-05-21 00:34:52', NULL),
	(22, '0944', 3, '2019-05-21 00:34:52', NULL),
	(23, '0907', 1, '2019-05-21 00:34:52', NULL),
	(24, '0912', 1, '2019-05-21 00:34:52', NULL),
	(25, '0946', 1, '2019-05-21 00:34:52', NULL),
	(26, '0909', 1, '2019-05-21 00:34:52', NULL),
	(27, '0930', 1, '2019-05-21 00:34:52', NULL),
	(28, '0948', 1, '2019-05-21 00:34:52', NULL),
	(29, '0910', 1, '2019-05-21 00:34:52', NULL),
	(30, '0938', 1, '2019-05-21 00:34:52', NULL),
	(31, '0950', 1, '2019-05-21 00:34:52', NULL),
	(32, '0813', 1, '2019-05-21 00:34:52', NULL),
	(33, '0913', 1, '2019-05-21 00:34:52', NULL),
	(34, '0919', 1, '2019-05-21 00:34:52', NULL),
	(35, '0928', 1, '2019-05-21 00:34:52', NULL),
	(36, '0947', 1, '2019-05-21 00:34:52', NULL),
	(37, '0981', 1, '2019-05-21 00:34:52', NULL),
	(38, '0908', 1, '2019-05-21 00:34:52', NULL),
	(39, '0914', 1, '2019-05-21 00:34:52', NULL),
	(40, '0920', 1, '2019-05-21 00:34:52', NULL),
	(41, '0929', 1, '2019-05-21 00:34:52', NULL),
	(42, '0949', 1, '2019-05-21 00:34:52', NULL),
	(43, '0989', 1, '2019-05-21 00:34:52', NULL),
	(44, '0911', 1, '2019-05-21 00:34:52', NULL),
	(45, '0918', 1, '2019-05-21 00:34:52', NULL),
	(46, '0921', 1, '2019-05-21 00:34:52', NULL),
	(47, '0939', 1, '2019-05-21 00:34:52', NULL),
	(48, '0970', 1, '2019-05-21 00:34:52', NULL),
	(49, '0998', 1, '2019-05-21 00:34:52', NULL),
	(50, '0904', 2, '2019-05-21 00:34:52', NULL),
	(51, '0916', 2, '2019-05-21 00:34:52', NULL),
	(52, '0935', 2, '2019-05-21 00:34:52', NULL),
	(53, '0965', 2, '2019-05-21 00:34:52', NULL),
	(54, '0976', 2, '2019-05-21 00:34:52', NULL),
	(55, '0994', 2, '2019-05-21 00:34:52', NULL),
	(56, '0905', 2, '2019-05-21 00:34:52', NULL),
	(57, '0917', 2, '2019-05-21 00:34:52', NULL),
	(58, '0936', 2, '2019-05-21 00:34:52', NULL),
	(59, '0966', 2, '2019-05-21 00:34:52', NULL),
	(60, '0977', 2, '2019-05-21 00:34:52', NULL),
	(61, '0995', 2, '2019-05-21 00:34:52', NULL),
	(62, '0906', 2, '2019-05-21 00:34:52', NULL),
	(63, '0926', 2, '2019-05-21 00:34:52', NULL),
	(64, '0945', 2, '2019-05-21 00:34:52', NULL),
	(65, '0967', 2, '2019-05-21 00:34:52', NULL),
	(66, '0978', 2, '2019-05-21 00:34:52', NULL),
	(67, '0997', 2, '2019-05-21 00:34:52', NULL),
	(68, '0915', 2, '2019-05-21 00:34:52', NULL),
	(69, '0927', 2, '2019-05-21 00:34:52', NULL),
	(70, '0956', 2, '2019-05-21 00:34:52', NULL),
	(71, '0975', 2, '2019-05-21 00:34:52', NULL),
	(72, '0979', 2, '2019-05-21 00:34:52', NULL);
/*!40000 ALTER TABLE `prefix` ENABLE KEYS */;

-- Dumping structure for table apiserver.product_codes
DROP TABLE IF EXISTS `product_codes`;
CREATE TABLE IF NOT EXISTS `product_codes` (
  `ProductCodeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CarrierID` int(11) NOT NULL,
  `RegisterTo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Amount` double NOT NULL,
  `AmountCharged` double NOT NULL DEFAULT '0',
  `Status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Is_Deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProductCodeID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.product_codes: ~1 rows (approximately)
DELETE FROM `product_codes`;
/*!40000 ALTER TABLE `product_codes` DISABLE KEYS */;
INSERT INTO `product_codes` (`ProductCodeID`, `CarrierID`, `RegisterTo`, `Code`, `Name`, `Amount`, `AmountCharged`, `Status`, `created_at`, `updated_at`, `Is_Deleted`) VALUES
	(1, 1, '343', '15', 'Regular Load 15', 15, 14.37, 1, NULL, '2019-05-21 00:43:03', 0);
/*!40000 ALTER TABLE `product_codes` ENABLE KEYS */;

-- Dumping structure for table apiserver.userinfo
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE IF NOT EXISTS `userinfo` (
  `UserInfoID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `CompanyName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Interval` int(11) NOT NULL DEFAULT '1',
  `Credits` double NOT NULL DEFAULT '0',
  `IsRecurring` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`UserInfoID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.userinfo: ~1 rows (approximately)
DELETE FROM `userinfo`;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;
INSERT INTO `userinfo` (`UserInfoID`, `UserID`, `CompanyName`, `Interval`, `Credits`, `IsRecurring`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Union Bank', 1, 10000, 0, '2019-05-21 00:36:30', '2019-05-21 00:36:30');
/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;

-- Dumping structure for table apiserver.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` int(11) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `email`, `email_verified_at`, `password`, `is_active`, `remember_token`, `created_at`, `updated_at`, `type`, `last_login`, `is_deleted`) VALUES
	(1, 'admin', 'admin@gateway.com', NULL, '$2y$10$HBNc8dS0oOir2VUlOkn/.uZX8lXNuCmsF.OCuFCsxPVXyg.7Yn6Ly', 1, NULL, '2019-05-21 00:34:52', '2019-05-21 00:34:52', 1, NULL, 0),
	(2, 'unionbank', 'admin@unionbank.com', NULL, '$2y$10$r1/giyoLyojKTuQ0YdQf7.oBdTdssdzNaSEyhdY0BUIKBeCUWGrY6', 1, NULL, '2019-05-21 00:36:30', '2019-05-21 00:36:30', 2, NULL, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
