
if (typeof (System) === "undefined") {
    System = {};
};

System.Clients = function System$Clients(page) {

	//CONSTANTS -> move to seperate js file
	const FIELD = '.field';
	const REQUIRED = '.vrequired';
	const ID_SELECTOR = '#';
	const LABEL = 'label';
	const ID = 'id';
	const NAME = 'name';
	const USERNAME = 'username';
	const EMAIL = 'email';
	const PASSWORD = 'password';
	const TYPE = 'type';

	var entity = { 
		id: 'id',
		username: 'username',
		email: 'email',
		password: 'password',
		is_active: 'is_active',
		Interval: 'interval',
		CompanyName: 'company_name',
		Credits: 'credits',
		Budget: 'credits',
		adjust_credits: 'adjust-credits',
		IsRecurring: 'is_recurring'
	};

	var credentials = {
		ClientID: 'ClientID',
		ClientSecret: 'ClientSecret' 
	};

	var module = "client";
	var $this = this; 
	var $tips = $(".tips-message");
	var controller = BASE_URL + "clients";
	
	var $tmpl = $("#list-tmpl");
	var $list = $("#list-result");
	var $table = $("#list-table");

	var $transaction_tmpl = $("#transactions-list-tmpl");
	var $transaction_list = $("#transactions-list-result");
	var $transaction_table = $("#transactions-list-table");

	var $history_tmpl = $("#history-list-tmpl");
	var $history_list = $("#history-list-result");

	var $clients_pagination = $('#client-pagination');
	var $page_next = $('a[name="page-next"]');
	var $page_prev = $('a[name="page-previous"]');
	var $start_date = $('#start-date');
	var $end_date = $('#end-date');

	var $trans_type = $('#trans-type');
	var $trans_carrier = $('#trans-carrier');

	var $pagination = new LaravelPagination();

	var $mode_on = $('#mode-on');
	var $mode_off = $('#mode-off');
	var inputs = {} ;
	var $fields;

	var $save_btn = $('#save-btn');
	

	this.Load = function System$Load() 
	{
		var $temp = $();
		$(FIELD).each(function(){ 
			$temp = $temp.add( $(this) ); 
		});
		$this.$fields = $temp;
	}
	
	this.Validate = function System$$Validate() 
	{
		var retVal = true;
		$this.ClearError();
		
		//VALIDATION STARTS HERE
        $this.$fields.filter(REQUIRED).each(function(){
			if($('#'+ID).val() == '' || ($('#'+ID).val() != '' && $(this).attr(ID) != PASSWORD)) {
				Validate($tips, $(this), $(this).attr(LABEL)+ " is required");
			}
        });
		
		return retVal;
	}
	
	this.ListView = function System$ListView(url = null)
	{
		let data = {}
		let action = Common.Request;
		url = (url == null) ? $this.GetListUrl(controller + '/list') : url;

		action(url, data, "GET", function(res){
			let result = res.data.data;
			var list = result.data;

			var $templateMain = $tmpl.tmpl(list);
			$list.html($templateMain);
			$this.Pagination(result, 'ListView');
		})

	}
	
	this.Pagination = function System$Pagination(data, action)
	{
		$pagination.destroy();
		$pagination.generate({
			ulId: "client-pagination",
			pageIndex: data.current_page,
			pageSize: data.per_page,
			total: data.total,
			prevPageUrl: data.prev_page_url,
			nextPageUrl: data.next_page_url,
			from: data.from,
			to: data.to,
			dataUrl: data.path
		}, action);
	}

	this.ClientTransactionsListView = function System$ClientTransactionsListView(url)
	{
		let data = $this.GetFieldValues([entity.id]);
		let action = Common.Request;
		let start_date = $start_date.val();
		let end_date = $end_date.val();
		let carrier = $trans_carrier.val();
		let trans_type = $trans_type.val();
		let endpoint = BASE_URL + 'transactions/listbyclientallocation?id=';
		url = (url == null) ? endpoint : url + '&id=';
		url += data.id;
		
		if(start_date != null && start_date != '' && end_date != null && end_date != '') {
			url += '&startDate='+start_date+'&endDate='+end_date;
		}
		if(trans_type != null && trans_type != '') {
			url += '&type='+trans_type;
		}
		if(carrier != null && carrier != '') {
			url += '&carrier='+carrier;
		}
		action(url, {}, "GET", function(res){
			let result = res.data.data;
			var list = result.data;
			var page_current = result.current_page;
			let tags = res.data.tags;

			$("#total_credits").html(tags.Amount);
			$("#total_credits_charged").html(tags.AmountCharged);

			var $templateMain = $transaction_tmpl.tmpl(list);
			$transaction_list.html($templateMain);

			$this.Pagination(result, 'ClientTransactionsListView');
		})

	}

	this.CreditHistoryListView = function CreditHistoryListView(url)
	{
		let data = $this.GetFieldValues([entity.id]);
		let action = Common.Request
		let endpoint = BASE_URL + 'credit/history';
		url = (url == null) ? endpoint + '?id=' : url + '&id=';
		url += data.id;

		let start = $start_date.val();
		let end = $end_date.val()

		if(start != '') {
			url += '&startDate=' + start + '&endDate=' + end;
		}

		action(url, {}, "GET", function(res){
			let result = res.data.data;
			var list = result.data;
			var page_current = result.current_page; 
			var $templateMain = $history_tmpl.tmpl(list);
			$history_list.html($templateMain);

			$this.Pagination(result, 'CreditHistoryListView');
		})
	}

	this.GetTotalCreditsLoaded = function System$GetTotalCreditsLoaded()
	{
		let data = { };
		let action = Common.Request
		let totalLoaded = 0;
		let totalBalance = 0;
		let id = $('#id').val();
		let start_date = $start_date.val();
		let end_date = $end_date.val();

		var url = BASE_URL + 'clientallocation/getloadedcredit?';
		if(id != null && id != '') {
			url += 'clientID=' + id;	
		}

		if(start_date != null && start_date != '' && end_date != null && end_date != '') {
			url += (id != null && id != '') ? '&' : ''; 
			url += 'startDate='+ start_date +'&endDate='+ end_date;
		}
		
		action(url, {}, "GET", function(res){
			let result = res.data
			totalLoaded = result.data.totalLoaded;
			totalLoaded = (totalLoaded == null) ? 0 : totalLoaded.toLocaleString();

			totalBalance = result.data.totalBalance;
			totalBalance = (totalBalance == null) ? 0 : totalBalance.toLocaleString();
			$("#total-loaded-credits").html(totalLoaded);
			$("#total-balance-credits").html(totalBalance);
		})

	}

	this.Initialize = function System$Initialize(id) 
	{
		let data = {};
		let action = Common.Request

		action(controller+"/initialize/"+ id, data, "GET", function(res){
			let result = res.data
			var list = result.data;

			if(!result.error){
				$this.$fields.each(function(){
					$(this).val(list[$(this).attr(ID).toLowerCase()]).trigger('change');
					if($(this).attr('id') == 'is_recurring') {
						if($(this).val() == 1) {
							$mode_on.prop('checked', 'checked');
							$mode_off.prop('checked', false);
						} else {
							$mode_off.prop('checked', 'checked');
							$mode_on.prop('checked', false);
						}
					}
				});
				openModal(module+'-modal');
			}
			else{
				 alert(result.message);
			}	
		});
	}
	
	this.Save = function System$Save()
	{
		if(!$this.Validate()){
			return;
		}
		
		$save_btn.attr("disabled","disabled");
		let action = Common.Request
		let data = $this.GetFieldValues([
			entity.id,
			entity.CompanyName,
			entity.username,
			entity.email,
			entity.password,
			entity.Credits,
			entity.Interval,
			entity.IsRecurring
		]);
		//manipulate data here 
		data['type'] = 2;
		data['is_active'] = 1;
		data['password_confirmation'] = data['password'];

		action(controller + "/save", data, "POST", function(res)
        {
			$save_btn.removeAttr("disabled");
			let result = res.data

			if(!result.error){
				closeModal(module+"-modal");
				$this.ListView();
				$this.ClientTransactionsListView();
				$this.GetTotalCreditsLoaded();
				Dialog(SUCCESS_TITLE, result.message, SUCCESS_TYPE);
			}
			else{
				Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
			}
		});
	}
	
	this.Remove = function System$Remove(id, name)
	{	
		let data = { id: id};
		let action = Common.Request

		Dialog('Deactivation', 
		'Are your sure you want to deactivate ' + name + ' ?', 
		'warning',
		true,
		'Yes',
		function(isConfirm){
			if(isConfirm.value) {
				action(controller + "/deactivate",
				data,
				"POST",
				function(res){
					let result = res.data

					if(!result.error){
						Dialog("Deactivated!", name +" has been deactivated.", "success");
						$this.ListView();
					}
					else{
						Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
					}
				});
			} else {
				Dialog("Cancelled", "Cancelled", "error");
			}
		});
	}

	this.Activate = function System$Activate(id, name)
	{	
		let data = { id: id};
		let action = Common.Request

		Dialog('Activation', 
		'Are your sure you want to activate ' + name + ' ?', 
		'warning',
		true,
		'Yes',
		function(isConfirm){
			if(isConfirm.value) {
				action(controller + "/activate",
				data,
				"POST",
				function(res){
					let result = res.data

					if(!result.error){
						Dialog("Activated!", name +" has been activated.", "success");
						$this.ListView();
					}
					else{
						Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
					}
				});
			} else {
				Dialog("Cancelled", "Cancelled", "error");
			}
		});
	}

	this.GetCredentials = function System$GetCredentials(id) 
	{
		let data = { id: id };
		let action = Common.Request

		action(controller+"/getcredentials", data, "POST", function(res){
			let result = res.data
			var list = result.data;

			if(!result.error){
				$this.$fields.filter(ID_SELECTOR + credentials.ClientID).val(list.id).trigger('change');
				$this.$fields.filter(ID_SELECTOR + credentials.ClientSecret).val(list.secret).trigger('change');
				openModal(module + '-credentials-modal');
			}
			else{
				 alert(result.message);
			}	
		});
	}

	this.AdjustCredits = function System$AdjustCredits(mode) 
	{
		let data = $this.GetFieldValues([ entity.id, entity.adjust_credits ]);
		let action = Common.Request
		data['mode'] = mode;
		action(controller+"/adjustclientcredits", data, "POST", function(res){
			let result = res.data
			var list = result.data;

			if(!result.error){
				$('#adjust-credits').val(0);
				closeModal('adjust-credits-modal');
				$this.ClientTransactionsListView();
				$this.GetTotalCreditsLoaded();
				Dialog(SUCCESS_TITLE, 'Sucess!', SUCCESS_TYPE);
			}
			else{
			 	Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
			}	
		});
	}

	this.GetFieldValues = function System$GetFieldValues(columns)
	{
		let data = {};
		for(var i in columns){
			data[columns[i]] = $this.$fields.filter(ID_SELECTOR + columns[i]).val();
		}
		return data;
	}

	this.GetListUrl = function System$GetListUrl(url, id = null)
	{
		url = (id != null) ? url+ '&id='+id : url+ '?id=' +id;
		return url;
	}
	
	this.Clear = function System$Clear()
	{
		$this.DisableFields(false);
		$this.ClearError();
		$this.$fields.val("");
	}
	
	this.ClearError = function System$ClearError()
	{
		$this.$fields.removeClass("input-error");
		$tips.html("").hide();
	}

	this.SetReadOnly = function System$SetReadOnly(value)
	{
		$this.$fields.attr('disabled', value);
	}
	
	this.DisableFields = function System$DisableFields(isTrue)
	{
		$this.ClearError();
		$this.$fields.attr('disabled',isTrue);
	}

	this.GeneratePassword =  function System$GeneratePassword()
	{
		var password = Math.random().toString(36).slice(-8);
		$this.$fields.filter(ID_SELECTOR+PASSWORD).val(password).trigger('change');
	}

	this.GetTransactionSummary = function System$GetTransactionSummary(client_id , start, end)
	{
		let data = {
			start : start,
			end : end,
			client_id : client_id
		};

		let action = Common.Request

		action(BASE_URL + "/transactions/gettransactionbystatus", data, "POST", function(res){
			let result = res.data
			var data = result.data;

			if(!result.error){
				$("#total-successful-transactions").html(data.success);
				$("#total-failed-transactions").html(data.failed);
			}
			else{
				 alert(result.message);
			}	
		});
	}

	this.setDefaultDate  = function System$setDefaultDate(callback)
	{
		let start_date = moment().format("YYYY-MM-DD");
		let end_date = moment().format("YYYY-MM-DD");

		$start_date.val(start_date);
		$end_date.val(end_date);

		if(typeof(callback) == "function")
		{
			callback(true);
		}
	}

	this.GetCurrentAllocation = function System$GetCurrentAllocation(id , callback)
	{
		let data = {
			
		};

		let client_id =  id;

		let action = Common.Request

		action(BASE_URL + `/clientallocation/currentallocation/${client_id}`, data, "GET", function(res){
			let result = res.data
			var data = result.data;

			if(!result.error){
				$start_date.val(data.start_date);
				$end_date.val(data.end_date);

				if(typeof(callback) == "function")
				{
					callback(data);
				}
			}
			else{
				 alert(result.message);
			}	
		});

	}
}
