var module;
$(document).ready(function()
{
	let client_id = $("#id").val();
	let $recurring = $('#is_recurring');

	module  = new System.Clients(); // <--- Initialize the module
	module.ListView();
	module.Load();

	// module.GetTotalCreditsLoaded();
	module.GetCurrentAllocation(0 , function(data){
		module.ClientTransactionsListView();
		module.GetTransactionSummary(client_id ,data.start_date ,data.end_date);
	});
	module.setDefaultDate(function(){
		let start = $("#start-date").val();
		let end = $("#end-date").val();

		module.GetTotalCreditsLoaded();
		module.GetTransactionSummary("" , start , end);
	});
	
	$("#save-btn").click(function(){
		module.Save();
	});
	
	$("#client-clear").click(function(){
		module.Clear();
	});

	$('#apply-filters-btn').click(function() {
		module.ClientTransactionsListView();
	});

	$("#filter").click(function(){
		let start = $("#start-date").val();
		let end = $("#end-date").val();

		module.GetTotalCreditsLoaded();
		module.GetTransactionSummary("" , start , end);
	});

});

function Remove(id, name)
{
	module.Remove(id, name);
}

function Activate(id, name)
{
	module.Activate(id, name);
}

function Initialize(id)
{
	module.Clear();
	module.Initialize(id);
	module.SetReadOnly(false);
}

function View(url)
{
	window.location.href = url;
}

function ViewCredentials(id)
{
	module.GetCredentials(id);
}

function ListView(url)
{
	module.ListView(url);
}

function AddModuleForm()
{
	module.Clear();
	$('#interval').val(0);
	$('#mode-on').prop('checked', 'checked');
	$('#mode-off').prop('checked', false);
	$('#is_recurring').val(1);
	openModal('client-modal');
}

function ChangeAllocationMode(value)
{
	$('#is_recurring').val(value);
}

function GeneratePassword()
{
	module.GeneratePassword();
}

function TogglePassword()
{
	var $password = $('#password');
	let type = $password.attr('type');
	type = ( type == 'text' ) ? 'password' : 'text';
	$password.attr('type', type);
}

function GoToHistory(url)
{
	window.location.href = url;
}
