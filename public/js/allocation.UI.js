var module;
var payment;
$(document).ready(function()
{
	let client_id = $("#id").val();

	payment  = new System.Payment();
    module  = new System.ClientAllocation(); // <--- Initialize the module
	module.Load();
	payment.Load();
	module.ListView();

	module.setDefaultDate(function(){
		let start = $("#start-date").val();
		let end = $("#end-date").val();
	});
	
	$("#save-payment-btn").click(function(){
		payment.Save();
	});
	
	// $("#client-clear").click(function(){
	// 	module.Clear();
	// });

	// $('#apply-filters-btn').click(function() {
	// 	module.ClientTransactionsListView();
	// });

	// $("#filter").click(function(){
	// 	let start = $("#start-date").val();
	// 	let end = $("#end-date").val();

	// 	module.GetTotalCreditsLoaded();
	// 	module.GetTransactionSummary("" , start , end);
	// });

});

function Remove(id, name)
{
	module.Remove(id, name);
}

function Initialize(id)
{
	module.Clear();
	module.Initialize(id);
	module.SetReadOnly(false);
}

function View(url)
{
	window.location.href = url;
}

function ListView(url)
{
	module.ListView(url);
}

function AddPaymentForm(id, allocation_dates)
{
	payment.Clear();
	let $date = $('#date');
	$date.val(moment().format('YYYY-MM-DD')).trigger('change');

	$('#allocation-id').val(id);
	$('#allocation-dates').text(allocation_dates);
	openModal('payment-modal');
}

function GoToPaymentHistory(url)
{
	window.location.href = url;
}
