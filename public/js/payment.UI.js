var module;
$(document).ready(function()
{
	let client_id = $("#id").val();

    module  = new System.Payment(); // <--- Initialize the module
	module.Load();
	module.ListView();

	module.setDefaultDate(function(){
		let start = $("#start-date").val();
		let end = $("#end-date").val();
	});

});

function Remove(id, name)
{
	module.Remove(id, name);
}

function Initialize(id)
{
	module.Clear();
	module.Initialize(id);
	module.SetReadOnly(false);
}

function View(url)
{
	window.location.href = url;
}

function ListView(url)
{
	module.ListView(url);
}

function AddPaymenrForm(id, allocation_dates)
{
	payment.Clear();
	let $date = $('#date');
	$date.val(moment().format('YYYY-MM-DD')).trigger('change');

	$('#allocation-id').val(id);
	$('#allocation-dates').text(allocation_dates);
	openModal('payment-modal');
}

function GoToPaymentHistory(url)
{
	window.location.href = url;
}
