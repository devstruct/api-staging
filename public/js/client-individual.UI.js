var module;
$(document).ready(function()
{
	let client_id = $("#id").val();
	
	module  = new System.Clients(); // <--- Initialize the module
	module.Load();

	module.GetCurrentAllocation(client_id,function(data){
		module.ClientTransactionsListView();
		module.GetTransactionSummary(client_id ,data.start_date ,data.end_date);
	});

	module.setDefaultDate(function(){
		let start = $("#start-date").val();
		let end = $("#end-date").val();

		module.GetTotalCreditsLoaded();
		// module.GetTransactionSummary("" , start , end);
	});

	$("#save-btn").click(function(){
		module.Save();
	});

	$("#client-clear").click(function(){
		module.Clear();
	});

	$('#adjust-btn').click(function(){
		module.AdjustCredits('add');
	});

	$('#remove-btn').click(function(){
		module.AdjustCredits('remove');
	});

	$('#apply-filters-btn').click(function() {
		module.ClientTransactionsListView();
	});

	$("#filter").click(function(){
		let start = $("#start-date").val();
		let end = $("#end-date").val();

		module.GetTotalCreditsLoaded();
		module.GetTransactionSummary("" , start , end);
	});

});

function Remove(id, name)
{
	module.Remove(id);
}

function Initialize(id)
{
	$('#adjustModalTitle').html('Edit Client');
	module.Clear();
	module.ClearError();
	module.Initialize(id);
	module.SetReadOnly(false);
}

function View(id)
{
	$('#adjustModalTitle').html('Edit Client');
	module.Clear();
	// module.DisableFields();
	module.SetReadOnly(true);
	module.Initialize(id);
}

function ViewSettings()
{
	// module.Clear();
	openModal('client-modal');
}

function ViewCreditHistory(url)
{
	window.location.href = url;
}

function AdjustCredits()
{
	openModal('adjust-credits-modal');
}

function ClientTransactionsListView(url)
{
	module.ClientTransactionsListView(url);
}

function GeneratePassword()
{
	module.GeneratePassword();
}

function ChangeAllocationMode(value)
{
	$('#is_recurring').val(value);
}

function TogglePassword()
{
	var $password = $('#password');
	let type = $password.attr('type');
	type = ( type == 'text' ) ? 'password' : 'text';
	$password.attr('type', type);
}
