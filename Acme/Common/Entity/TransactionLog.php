<?php
namespace Acme\Common\Entity;
use Acme\Common\DataFields\TransactionLog as DataField;

class TransactionLog
{
    public $ID = "";
    public $TransactionID = "";
    public $Charged = 0;
    public $GSMBalance = 0;
    public $GatewayID = 0;
    public $Discrepancy = 0;
    public $Status = 0;
    public $IsRead = 0;
    public $CreatedAt = "";
    public $UpdatedAt = "";

    public function Validate()
    {
       
    }
    
    public function SetData($input)
    {
        $this->ID = !isset($input["id"])? 0:$input["id"];
        $this->TransactionID = $input["transaction_id"];
        $this->Charged = $input["charged"];
        $this->GSMBalance = $input["gsm_balance"];
        $this->GatewayID = $input["gateway_id"];
        $this->Discrepancy = $input["discrepancy"];
        $this->Status = $input["status"];
        $this->IsRead = $input["is_read"];
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->ID;
        $data[Datafield::TRANSACTION_ID] = $this->TransactionID;
        $data[Datafield::CHARGED] = $this->Charged;
        $data[Datafield::GSM_BALANCE] = $this->GSMBalance;
        $data[Datafield::GATEWAY_ID] = $this->GatewayID;
        $data[Datafield::DISCREPANCY] = $this->Discrepancy;
        $data[Datafield::STATUS] = $this->Status;
        $data[Datafield::IS_READ] = $this->IsRead;


        return $data;
    }

    public function Deserialize($data)
    {
        $result = array();

        $result['id'] = $data[Datafield::ID];
        $result['transaction_id'] = $data[Datafield::TRANSACTION_ID];
        $result['charged'] = $data[Datafield::CHARGED];
        $result['gsm_balance'] = $data[Datafield::GSM_BALANCE];
        $result['gateway_id'] = $data[Datafield::GATEWAY_ID];
        $result['discrepancy'] = $data[Datafield::DISCREPANCY];
        $result['status'] = $data[Datafield::STATUS];
        $result['is_read'] = $data[Datafield::IS_READ];


        return $result;
    }

}


?>