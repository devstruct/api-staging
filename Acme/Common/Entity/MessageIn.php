<?php
namespace Acme\Common\Entity;

use Acme\Common\DataFields\MessageOut as DataField;
use Acme\Common\Constants as Constants;

class MessageIn
{
    public $Id = "";
    public $SendTime = "";
    public $ReceiveTime = "";
    public $MessageFrom = "";
    public $MessageTo = "";
    public $SMSC = "";
    public $MessageText = "";
    public $MessageType = "";
    public $MessageParts = "";
    public $MessagePDU = "";
    public $Gateway = "";
    public $UserId = "";
    public $Scanned = 0;

    public function Validate()
    {
       
    }

    public function SetData($input)
    {
        $this->Id = isset($input["id"])?$input['id']:0;
        $this->SendTime = isset($input["send_time"])?$input['message_from']:date(Constants::ROW_DATE_TIME_FORMAT);
        $this->ReceiveTime = isset($input["receive_time"])?$input['message_text']:date(Constants::ROW_DATE_TIME_FORMAT);
        $this->MessageFrom = isset($input["message_from"])?$input['message_from']:Constants::EMPTY;
        $this->MessageTo = isset($input["message_to"])?$input['message_to']:Constants::EMPTY;
        $this->SMSC = isset($input["smsc"])?$input['smsc']:Constants::EMPTY;
        $this->MessageText = isset($input["message_text"])?$input['message_text']:Constants::EMPTY;
        $this->MessageType = isset($input["message_type"])?$input['message_type']:Constants::EMPTY;
        $this->MessageParts = isset($input["message_parts"])?$input['message_parts']:Constants::EMPTY;
        $this->MessagePDU = isset($input["message_pdu"])?$input['message_pdu']:Constants::EMPTY;
        $this->Gateway = isset($input["gateway"])?$input['gateway']:Constants::EMPTY;
        $this->UserId = isset($input["user_id"])?$input['user_id']:0;
        $this->Scanned = isset($input["scanned"])?$input['scanned']:0;
        
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->Id;
        $data[Datafield::SEND_TIME] = $this->SendTime;
        $data[Datafield::RECEIVE_TIME] = $this->ReceiveTime;
        $data[Datafield::MESSAGE_FROM] = $this->MessageFrom;
        $data[Datafield::MESSAGE_TO] = $this->MessageTo;
        $data[Datafield::SMSC] = $this->SMSC;
        $data[Datafield::MESSAGE_TEXT] = $this->MessageText;
        $data[Datafield::MESSAGE_TYPE] = $this->MessageType;
        $data[Datafield::MESSAGE_PARTS] = $this->MessageParts;
        $data[Datafield::MESSAGE_PDU] = $this->MessagePDU;
        $data[Datafield::GATEWAY] = $this->Gateway;
        $data[Datafield::USER_ID] = $this->UserId;
        $data[Datafield::SCANNED] = $this->Scanned;

        return $data;
    }
}


?>