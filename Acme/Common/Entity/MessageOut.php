<?php
namespace Acme\Common\Entity;

use Acme\Common\DataFields\MessageOut as DataField;
use Acme\Common\Constants as Constants;

class MessageOut
{
    public $Id = "";
    public $MessageTo = "";
    public $MessageFrom = "";
    public $MessageText = "";
    public $MessageType = "";
    public $MessageGuid = "";
    public $MessageInfo = "";
    public $Gateway = "";
    public $UserId = "";
    public $UserInfo = "";
    public $Priority = "";
    public $Scheduled = "";
    public $ValidityPeriod = "";
    public $IsSent = "";
    public $IsRead = "";

    public function Validate()
    {
       
    }

    public function SetData($input)
    {
        $this->MessageTo = isset($input["message_to"])?$input['message_to']:"";
        $this->MessageFrom = isset($input["message_from"])?$input['message_from']:"";
        $this->MessageText = isset($input["message_text"])?$input['message_text']:"";
        $this->MessageType = isset($input["message_type"])?$input['message_type']:"";
        $this->MessageGuid = isset($input["message_guid"])?$input['message_guid']:"";
        $this->MessageInfo = isset($input["message_info"])?$input['message_info']:"";
        $this->Gateway = isset($input["gateway"])?$input['gateway']:0;
        $this->UserId = isset($input["user_id"])?$input['user_id']:0;
        $this->UserInfo = isset($input["user_info"])?$input['user_info']:0;
        $this->Priority = isset($input["priority"])?$input['priority']:0;
        $this->Scheduled = isset($input["scheduled"])?$input['scheduled']: date(Constants::ROW_DATE_TIME_FORMAT);
        $this->ValidityPeriod = isset($input["validity_period"])?$input['validity_period']:0;
        $this->IsSent = isset($input["isSent"])?$input['isSent']:0;
        $this->IsRead = isset($input["isRead"])?$input['isRead']:0;
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->Id;
        $data[Datafield::MESSAGE_TO] = $this->MessageTo;
        $data[Datafield::MESSAGE_FROM] = $this->MessageFrom;
        $data[Datafield::MESSAGE_TEXT] = $this->MessageText;
        $data[Datafield::MESSAGE_TYPE] = $this->MessageType;
        $data[Datafield::GATEWAY] = $this->Gateway;
        $data[Datafield::USER_ID] = $this->UserId;
        $data[Datafield::USER_INFO] = $this->UserInfo;
        $data[Datafield::PRIORITY] = $this->Priority;
        $data[Datafield::SCHEDULED] = $this->Scheduled;
        $data[Datafield::VALIDITY_PERIOD] = $this->ValidityPeriod;
        $data[Datafield::IS_SENT] = $this->IsSent;
        $data[Datafield::IS_READ] = $this->IsRead;


        return $data;
    }
}


?>