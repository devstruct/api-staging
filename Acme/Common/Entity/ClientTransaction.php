<?php
namespace Acme\Common\Entity;

use Acme\Common\DataFields\ClientTransaction as DataField;
use Acme\Common\Constants as Constants;

class ClientTransaction
{
    public $TransactionID = "";
    public $ClientID = "";
    public $RecipientNumber = "";
    public $ProductCode ="";
    public $CarrierID ="";
    public $Amount ="";
    public $AmountCharged = "";
    public $Balance = "";
    public $Type ="";
    public $Status = 0;
    public $Reference = "";

    public function Validate()
    {
       
    }

    public function SetData($input)
    {

        if(isset($input["transaction_id"]))
        {
            $this->TransactionID = $input["transaction_id"];
        }
        else
        {
            $this->TransactionID = 0;
        }

        if(isset($input["client_id"]))
        {
            $this->ClientID = $input["client_id"];
        }
        else
        {
            $this->ClientID = 0;
        }
        

        if(isset($input["recipient_number"]))
        {
            $this->RecipientNumber = $input["recipient_number"];
        }
        else
        {
            $this->RecipientNumber = "";
        }

        if(isset($input["product_code"]))
        {
            $this->ProductCode = $input["product_code"];
        }
        else
        {
            $this->ProductCode = "INITIAL CREDIT";
        }

        if(isset($input["carrier_id"]))
        {
            $this->CarrierID = $input["carrier_id"];
        }
        else
        {
            $this->CarrierID = Constants::SYSTEM;
        }

        if(isset($input["amount"]))
        {
            $this->Amount = $input["amount"];
        }
        else
        {
            $this->Amount = 0;
        }

        if(isset($input["amount_charged"]))
        {
            $this->AmountCharged = $input["amount_charged"];
        }
        else
        {
            $this->AmountCharged = 0;
        }

        if(isset($input["balance"]))
        {
            $this->Balance = $input["balance"];
        }
        else
        {
            $this->Balance = 0;
        }

        if(isset($input["transaction_type"]))
        {
            $this->Type = $input["transaction_type"];
        }
        else
        {
            $this->Type = 5;
        }

        if(isset($input["status"]))
        {
            $this->Status = $input["status"];
        }
        else
        {
            $this->Status = 0;
        }

        if(isset($input["reference"]))
        {
            $this->Reference = $input["reference"];
        }
        else
        {
            $this->Reference = 0;
        }
        

        return $input;
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::TRANSACTION_ID] = $this->TransactionID;
        $data[Datafield::CLIENT_ID] = $this->ClientID;
        $data[Datafield::RECIPIENT_NUMBER] = $this->RecipientNumber;
        $data[Datafield::PRODUCT_CODE] = $this->ProductCode;
        $data[Datafield::CARRIER_ID] = $this->CarrierID;
        $data[Datafield::AMOUNT] = $this->Amount;
        $data[Datafield::AMOUNT_CHARGED] = $this->AmountCharged;
        $data[Datafield::BALANCE] = $this->Balance;
        $data[Datafield::TYPE] = $this->Type;
        $data[Datafield::STATUS] = $this->Status;
        $data[Datafield::REFERENCE] = $this->Reference;

        return $data;
    }

    public function Format($data)
    {
        $this->TransactionID = $data->TransactionID;
        $this->ClientID = $data->ClientID;
        $this->RecipientNumber = $data->RecipientNumber;
        $this->ProductCode = $data->ProductCode;
        $this->CarrierID = $data->CarrierID;
        $this->Amount = $data->Amount;
        $this->AmountCharged = $data->AmountCharged;
        $this->Balance = $data->Balance;
        $this->Type = $data->Type;
        $this->Status = $data->Status;
        $this->Reference = $data->Reference;

        return $data;
    }
}


?>