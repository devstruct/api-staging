<?php
namespace Acme\Common\Entity;
use Acme\Common\DataFields\Prefix as DataField;

class Prefix
{
    public $PrefixID = "";
    public $CarrierID = "";
    public $Code = "";


    public function Validate()
    {
       
    }
    
    public function SetData($input)
    {
        $this->PrefixID = $input["id"];
        $this->CarrierID = $input["carrier_id"];
        $this->Code = $input["code"];
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->PrefixID;
        $data[Datafield::CARRIER_ID] = $this->CarrierID;
        $data[Datafield::CODE] = $this->Code;

        return $data;
    }

    public function Deserialize($data)
    {
        $result = array();

        $result['id'] = $data[Datafield::ID];
        $result['carrier_id'] = $data[Datafield::CARRIER_ID];
        $result['code'] = $data[Datafield::CODE];

        return $result;
    }

}


?>