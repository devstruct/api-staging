<?php
namespace Acme\Common\DataFields;

class Carrier
{
    const TABLE_NAME = "carriers";


    const ID = "CarrierID";
    const CARRIER_NAME = "CarrierName";
    const STATUS = "Status";
    const CREATED_AT = "created_at";
    const UPDATED = "updated_at";
}


?>