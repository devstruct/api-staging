<?php
namespace Acme\Common\DataFields;

class ClientTransaction
{
    const TABLE_NAME = "client_transactions";

    const TRANSACTION_ID = "TransactionID";
    const CLIENT_ID = "ClientID";
    const RECIPIENT_NUMBER = "RecipientNumber";
    const PRODUCT_CODE = "ProductCode";
    const CARRIER_ID = "CarrierID";
    const AMOUNT = "Amount";
    const BALANCE = "Balance";
    const AMOUNT_CHARGED = "AmountCharged";
    const TYPE = "Type";
    const STATUS = "Status";
    const REFERENCE = "Reference";
    const CREATED_AT = 'created_at';
}


?>