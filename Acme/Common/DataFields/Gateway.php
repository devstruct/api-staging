<?php
namespace Acme\Common\DataFields;

class Gateway
{
    const TABLE_NAME = "gateways";
    const PATH = "gateways";

    const GATEWAY_ID = "GatewayID";
    const NAME = "Name";
    const CARRIER_ID = "CarrierID";
    const ADDRESS = "Address";
    const SIM = "SIM";
    const TYPE = "Type";
    const BALANCE = "Balance";
    const MINIMUM = "Minimum";
    const PRIORITY = "Priority";
    const STATUS = "Status";
    const IS_DELETED = 'Is_Deleted';
}