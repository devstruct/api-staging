<?php
namespace Acme\Common\DataFields;

class User
{
    const ID = "id";
    const USERNAME = "username";
    const EMAIL = "email";
    const PASSWORD = "password";
    const IS_ACTIVE = "is_active";
    const TYPE = "type";
    const IS_DELETED = 'is_deleted';
}


?>