<?php
namespace Acme\Common\DataFields;

class CreditPayment
{
    const TABLE_NAME = "credit_payments";


    const ID = "PaymentID";
    const ALLOCATION_ID = "AllocationID";
    const AMOUNT = "Amount";
    const DATE = "Date";
    const CREATED_AT = "created_at";
    const UPDATED = "updated_at";
}


?>