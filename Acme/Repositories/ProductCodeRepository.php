<?php
namespace Acme\Repositories;

use App\ProductCode as Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;
use Acme\Repositories\CarrierRepository as Carrier;
use Acme\Common\DataFields\ProductCode as DataField;
use Acme\Common\Entity\ProductCode as Entity;

use Acme\Common\Constants as Constants;
use Acme\Common\Pagination as Pagination;




class ProductCodeRepository extends Repository{

    protected $model;

    use Pagination;
	
	public function __construct()
	{
        $this->model = new Model;
        $this->carrier = new Carrier;
	}

     public function getByID($id){
        $result = $this->model->where(DataField::ID, $id)->first();

        return $result;
    }

    public function list($request){
        $this->SetPage($request);
        $query = $this->model->where(DataField::STATUS,'1')
                             ->where(DataField::IS_DELETED,0);
        
        if ($request->has(Constants::KEYWORD)) {
            $search = trim($request->input(Constants::KEYWORD));
            $query = $query->where(function ($query) use ($search) {
                $query->orWhere(DataField::CODE, 'LIKE', '%' . $search . '%');
                $query->orWhere(DataField::NAME, 'LIKE', '%' . $search . '%');
                $query->orWhere(DataField::AMOUNT, 'LIKE', '%' . $search . '%');
                $query->orWhere(DataField::AMOUNT_CHARGED, 'LIKE', '%' . $search . '%');
            });
        }

        if ($request->has(DataField::CARRIER_ID)) {
            $carrier = trim($request->input(DataField::CARRIER_ID));
            $query = $query->where(DataField::CARRIER_ID,$carrier);
        }

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $paginated =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy(DataField::ID, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);

        $carriers = $this->carrier->getOptionList();
        $transformedData = $paginated->getCollection()->transform(function($item) use ($carriers) {
            $item->Carrier = $carriers[$item->CarrierID];

            return $item;
        });

        $result = new LengthAwarePaginator(
            $transformedData,
            $paginated->total(),
            $paginated->perPage(),
            $paginated->currentPage(), 
            [
                'path' => $request->url(),
                'query' => [ 'page' => $paginated->currentPage() ]
            ]);

        return $result;
    }

    public function show($id){
        $result = $this->model->find($id);

        return $result;
    }

    public function destroy($id){
        $result = $this->model->where(DataField::ID, $id)->delete();

        return $result;
    }

    public function create($entity){
       $result = $this->model->create($entity);
       return $result;
    }

    public function update($entity , $id){
       
       $result = $this->model->where(DataField::ID,$id)->update($entity);

       return $result;
    }

    public function save($entity){
        $result = null;
        
        if($entity[DataField::ID] == ""){
           $result = $this->model->create($entity);
        }
        else{
           $result = $this->model->where(DataField::ID,$entity[DataField::ID])->update($entity);
        }

        return $result;
    }

    public function delete($id)
    {
        $result = null;
        $result = $this->model->where(DataField::ID, $id)->update([DataField::IS_DELETED => 1]);

        return $result;
    }

    public function getByCode($key, $carrier_id){
        $result = $this->model
                       ->where(DataField::CODE, $key)
                       ->where(DataField::CARRIER_ID, $carrier_id)
                       ->first();

        return $result;
    }
}