<?php
namespace Acme\Repositories;

use App\CreditPayment as Model;
use Illuminate\Support\Facades\Validator;

use Acme\Common\DataFields\CreditPayment as DataField;
use Acme\Common\DataFields\ClientAllocation as ClientAllocationDataField;

use Acme\Common\Entity\CreditPayment as Entity;

use Acme\Common\Constants as Constants;
use Acme\Common\Pagination as Pagination;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\VarDumper\Cloner\Data;
use Carbon\Carbon;

class CreditPaymentRepository extends Repository{

    protected $model;

    use Pagination;
	
	public function __construct()
	{
		$this->model = new Model;
	}

     public function getByID($id){
        $result = $this->model->where(DataField::ID, $id)->first();

        return $result;
    }

    public function list($request){
        $this->SetPage($request);
        $query = $this->model;

        if($request->has('id')) {
            $joinTable = ClientAllocationDataField::TABLE_NAME;
            $joinTableID = $joinTable.'.'.ClientAllocationDataField::ID;
            $tableAllocationID = DataField::TABLE_NAME.'.'.DataField::ALLOCATION_ID;

            $query = $query->join($joinTable, $joinTableID, '=', $tableAllocationID)
                        ->where(ClientAllocationDataField::CLIENT_ID, $request->input('id'));
        }
        
        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $paginated =  $query->paginate($this->PageSize,
                        [Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);

        $transformedData = $paginated->getCollection()->transform(function($item) {
            $item->StartDate = Carbon::parse($item->StartDate)->format('M j, Y');
            $item->EndDate = Carbon::parse($item->EndDate)->format('M j, Y');
            return $item;
        });

        $result = new LengthAwarePaginator(
            $transformedData,
            $paginated->total(),
            $paginated->perPage(),
            $paginated->currentPage(), 
            [
                'path' => $request->url(),
                'query' => [ 'page' => $paginated->currentPage() ]
            ]);

        return $result;
    }

    public function show($id){
        $result = $this->model->find($id);

        return $result;
    }

    public function destroy($id){
        $result = $this->model->where(DataField::ID, $id)->delete();

        return $result;
    }

    public function create($entity){
       $result = $this->model->create($entity);
       return $result;
    }

    public function update($entity , $id){
       
       $result = $this->model->where(DataField::ID,$id)->update($entity);

       return $result;
    }

    public function save($entity){
        $result = null;
        
        if($entity[DataField::ID] == ""){
           $result = $this->model->create($entity);
        }
        else{
           $result = $this->model->where(DataField::ID,$entity[DataField::ID])->update($entity);
        }

        return $result;
    }

    public function delete($id)
    {
        $result = null;
        $result = $this->model->where(DataField::ID, $id)->update([DataField::IS_DELETED => 1]);

        return $result;
    }
}