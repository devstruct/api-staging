<?php
namespace Acme\Repositories;

use App\ClientCredit as Model;
use Acme\Common\Pagination as Pagination;
use Illuminate\Support\Facades\Validator;


class ClientCreditRepository extends Repository{
    
    use Pagination;
    protected $model;
    
    public function __construct()
    {
        $this->model = new Model;
    }

    public function listView(){
        return $this->model->all();
    }

    public function findId($id){
        return $this->model->where('Id', $id)->first();
    }

    public function show($id){
        return $this->model->find($id);
    }

    public function destroy($id){
        $this->model->where('Id', $id)->delete();
    }

    public function save($request){
        $input = $request->all();
        $id = $input['id'];
        
        if($id == null){
            $this->model->create($input);
        }
        else{
            $this->model->where('Id', $id)->update($input);
        }

        return ['status' => true, 'results' => 'Success'];
    }
}