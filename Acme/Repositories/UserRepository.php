<?php
namespace Acme\Repositories;

use App\User as Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Passport\ClientRepository as Client;
use Acme\Common\DataResult as DataResult;
use Acme\Common\DataFields\User as DataField;
use Acme\Common\DataFields\UserInfo as InfoDataField;
use Acme\Common\DataFields\AllocationTransaction as AllocationTransactionDataField;
use Acme\Common\DataFields\ClientAllocation as ClientAllocationDataField;
use Acme\Common\DataFields\ClientTransaction as ClientTransactionDataField;
use Acme\Common\DataFields\Carrier as CarrierDataField;

use Acme\Common\Constants as Constants;
use Acme\Common\Entity\User as Entity;

use Acme\Repositories\UserInfoRepository as UserInfo;
use Acme\Repositories\ClientAllocationRepository as ClientAllocation;
use Acme\Repositories\AllocationTransactionRepository as AllocationTransaction;
use Acme\Repositories\ClientTransactionRepository as ClientTransaction;
use Acme\Repositories\CarrierRepository as Carrier;
use Acme\Repositories\MessageOutRepository as MessageOut;

use Acme\Common\Pagination as Pagination;
use Illuminate\Pagination\LengthAwarePaginator;
use Acme\Common\CommonFunction;
use Carbon\Carbon;
use Schema;

class UserRepository extends Repository{

    protected $model;
    protected $client;
    protected $userInfo;

    use Pagination;
    use CommonFunction;

    public function __construct()
    {
        $this->model = new Model;
        $this->client = new Client;
        $this->userInfo = new UserInfo;
        $this->clientAllocation = new ClientAllocation;
        $this->allocationTransaction = new AllocationTransaction;
        $this->clientTransaction = new ClientTransaction;
        $this->carrier = new Carrier;
        $this->SortBy = DataField::ID;
    }
    
    public function clientRules($data)
    {
        $rules = [
            // 'username' => 'required|string|max:255|unique:users,is_deleted,0',
            // 'email' => 'required|string|max:255|unique:users,is_deleted,0',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ];
        $rules['username'] = [
            'required',
            Rule::unique('users')->where(function ($query) {
                $query->where('is_deleted', 0);
            })
        ];
        $rules['email'] = [
            'required',
            Rule::unique('users')->where(function ($query) {
                $query->where('is_deleted', 0);
            })
        ];

        if($data['type'] == 2) {
            $rules = $rules + $this->userInfo->rules();
        }

        if($data['id'] != '' && $data[DataField::ID] != null) {
            $rules['username'] = 'required|string|max:255|unique:users,username,'.$data['id'].',id,is_deleted,0';
            $rules['email'] = 'required|string|email|max:255|unique:users,email,'.$data['id'].',id,is_deleted,0';
            // $rules['username'] = [
            //     'required',
            //     Rule::unique('users')->ignore($data['id'])->where(function ($query) {
            //         $query->where('is_active', 1);
            //     })
            // ];
            // $rules['email'] = [
            //     'required',
            //     Rule::unique('users')->ignore($data['id'])->where(function ($query) {
            //         $query->where('is_active', 1);
            //     })
            // ];

            if($data[DataField::PASSWORD] == '' || $data[DataField::PASSWORD] == null) {
                unset($rules[DataField::PASSWORD]);
                unset($rules['password_confirmation']);
            }
        }
        return $rules;
    }

    public function getByID($id){
        $result = $this->model->where(DataField::ID, $id)->first();

        return $result;
    }

    public function show($id){
        $result = $this->model->with('info')->find($id);

        return $result;
    }

    public function destroy($id){
        $result = $this->model->where(DataField::ID, $id)->delete();

        return $result;
    }

    public function create($request){
        $input = $request->all();
        $result = $this->model->create($input);
 
        return $result;
     }

    public function save($request){
        $input = $request->all();
        $result = null;
        
        if(!isset($input[Constants::ID])){
            $result = $this->model->create($input);
        }
        else{
            if($input[DataField::PASSWORD] == ''){
                unset($input[DataField::PASSWORD]);
            } else {
                $input[DataField::PASSWORD] = bcrypt($input[DataField::PASSWORD]);
            }
            unset($input['password_confirmation']);
            $result = $this->model->where(DataField::ID, $input[Constants::ID])->update($input);
        }

        return $result;
    }

    public function update($request , $id){
        $input = $request->all();
        $result= $this->model->where(DataField::ID,$id)->update($input);
 
        return $result;
     }

    public function saveClient($request){
        $input = $request->all();
        $result = null;
        $entity = new Entity;

        if(!isset($input[Constants::ID]) || empty($input[Constants::ID])) { 
            // Save New Client
            unset($input[Constants::PASSWORD_CONFIRMATION]);
            $input[DataField::PASSWORD] = bcrypt($input[DataField::PASSWORD]);

            $entity->SetData($input);
            $user = $this->model->create($entity->serialize());
            
            $request->merge([ InfoDataField::USER_ID => $user->id ]);
            $input['user_id'] = $user->id;
            $userInfo = $this->userInfo->process($input);

            // Create credit allocation
            $interval = $input['interval'];
            $start = null;
            $end = null;
            if(!empty($interval)) {
                $startDate = Carbon::now();
                $daysToAdd = Constants::INTERVAL[$interval];
                if($daysToAdd == 30) {
                    $endDate = Carbon::now()->addMonths(1);
                } else {
                    $endDate = Carbon::now()->addDays($daysToAdd);
                }
                $start = $startDate->startOfDay();
                $end = $endDate->startOfDay();
                
                
            }

            if($interval == Constants::NONE_INTERVAL)
            {
                $startDate = Carbon::now();
                $start = $startDate->startOfDay();
            }

            $input['client_id'] = $user->id;
            $input['start_date'] = $start;
            $input['end_date'] = $end;
            $input['rate'] = Constants::RATE;
            $input['status'] = Constants::ACTIVE;
            $input['consumed'] = 0;
            $input['budget'] = $input['credits'];
            $input['amount'] = $input['credits'];
            $input['amount_charged'] = $input['credits'];
            $input['allocation_id'] = null;
            $input['allocation_transaction_id'] = 0;
            $input['transaction_type'] = Constants::INITIAL_CREDITS;
            $input["carrier_id"] = Constants::SYSTEM;

            $allocation = $this->clientAllocation->process($input);

            $input["allocation_id"] = $allocation->id;
            $this->allocationTransaction->create($input);

            $input['status'] = Constants::SYSTEM_STATUS;
            $this->clientTransaction->create($input);

            if($input[DataField::TYPE] == 2) {
                $client = $this->client->createPasswordGrantClient(
                    $user->id, 
                    $input['company_name'], 
                    Constants::EMPTY
                    )->makeVisible(Constants::SECRET);
                $result = $client; 
            }
        } else { 

           
            #Update existing client
            $entity->SetData($input);
            $entity = $entity->serialize();
            if($entity[DataField::PASSWORD] == '' || $entity[DataField::PASSWORD] == null) {
                unset($entity[DataField::PASSWORD]);
            } else {
                $entity[DataField::PASSWORD] = bcrypt($entity[DataField::PASSWORD]);
            }
            $input['user_id'] = $entity[DataField::ID];
            $this->model->where(DataField::ID, $input[Constants::ID])->update($entity);
            $this->userInfo->processClientInfo($input);

            #Update Allocation
            $start_date = Carbon::now()->format(Constants::INPUT_DATE_FORMAT);

            $this->clientAllocation->changeCurrentAllocation(
                $input[Constants::ID], 
                $input['interval'], 
                $start_date, 
                $input['credits']);
        }

        return $result;
    }

    public function adjustClientCredit($input)
    {
        // Credit Transactions
        $carrier = $this->carrier->getAdmin();
        $current_date = Carbon::now()->format(Constants::INPUT_DATE_FORMAT);
        $result = null;

        $currentAllocation = $this->clientAllocation->getCurrentAllocation($input[DataField::ID] , $current_date );
 
        if(isset($input['adjust-credits'])) {
            if( $currentAllocation != null) {
                $budget = $currentAllocation->Budget - $currentAllocation->Consumed;
                $input['allocation_transaction_id'] = '';
                $input['allocation_id'] = $currentAllocation->AllocationID;
                $input['rate'] = 1;
                $input['status'] = Constants::ADJUSTMENT;
                if($input['mode'] == Constants::ADD) {
                    $input['credits'] = $input['adjust-credits'];
                    $input['transaction_type'] = 0;
                    $input['balance'] = $budget + $input['adjust-credits'];
                    $result = $this->processTansactionData($input,Constants::ADD_CREDITS, $carrier[CarrierDataField::ID]);
                } else if($input['mode'] == Constants::REMOVE) {
                    $input['credits'] = $input['adjust-credits'] * (-1);
                    $input['transaction_type'] = 1;
                    $input['balance'] = $budget - $input['adjust-credits'];
                    $result = $this->processTansactionData($input,Constants::REMOVE_CREDITS, $carrier[CarrierDataField::ID]);
                }

                return true;
            }
            else
            {
                return false;
            }

        }
        else
        {
            return false;
        }
    }

    public function processResult($data)
    {
        $result = $this->processForInput($data);
        $result = array_merge($result, $this->userInfo->processForInput($data->info));
        
        return $result;
    }

    public function processForInput($row)
    {
        $data = [];
        $data["id"] = $row->id;
        $data["email"] = $row->email;
        $data["username"] = $row->username;
        $data["is_active"] = $row->is_active;
        $data["type"] = $row->type;

        return $data;
    }

    public function processTansactionData($input, $code, $carrierID)
    {
        $allocation = $this->allocationTransaction->process($input);
        $input['client_id'] = $input[DataField::ID];
        $input['recipient_number'] = '';
        $input['product_code'] = $code;
        $input['carrier_id'] = $carrierID;
        $input['amount'] = $input['credits'];
        $input['amount_charged'] = $input['credits'];
        $result = $this->clientTransaction->processInput($input);

        return $result;
    }

    public function deactivateAccount($id)
    {
        $result = $this->model->where(DataField::ID, $id)->update([DataField::IS_ACTIVE => 0, DataField::IS_DELETED => 1]);

        return $result;
    }

    public function activateAccount($id)
    {
        $result = $this->model->where(DataField::ID, $id)->update([DataField::IS_ACTIVE => 1, DataField::IS_DELETED => 0]);

        return $result;
    }

    public function getClientList($request)
    {
        $this->SetPage($request);
        $query =  $this->model->with('info')
                ->with(['activeAllocation' => function($query)
                {
                    $query->where(ClientAllocationDataField::STATUS, 1);
                }])
                // ->where(DataField::IS_DELETED, 0)
                ->where(DataField::TYPE, Constants::CLIENT);

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $paginated =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy($order_by, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);

        $clientAllocation = new ClientAllocation;
        
        $transformedData = $paginated->getCollection()->transform(function($item) use($clientAllocation){
            //$allocation = $item->activeAllocation()->first();
            $today = Carbon::now()->format(Constants::INPUT_DATE_FORMAT);
            $allocation = $this->clientAllocation->getCurrentAllocation($item->id , $today);
            if($allocation != null) {
                $item->loaded = $this->formatDouble($allocation->Budget);
                $item->balance = $this->formatDouble($allocation->Budget - $allocation->Consumed);
                if($item->last_login != null) {
                    $item->last_login = Carbon::parse($item->last_login)->format('M j, y g:ia');
                } else {
                    $item->last_login = '';
                }
                
            } else {
                $item->loaded = 0;
                $item->balance = 0;
            }

            return $item;
        });

        $result = new LengthAwarePaginator(
        $transformedData,
        $paginated->total(),
        $paginated->perPage(),
        $paginated->currentPage(), 
        [
            'path' => $request->url(),
            'query' => [ 'page' => $paginated->currentPage() ]
        ]);
            
        return $result;
    }

    public function allocationTransactionList($request)
    {
        $result = $this->clientAllocation->listCurrentTransactions($request);

        return $result;
    }

    public function getClientCredentials($id)
    {
        $result = $this->client->forUser($id)->first()->makeVisible(Constants::SECRET);

        return $result;
    }

    public function getFields()
    {
        $result = Schema::getColumnListing(Constants::USERS);
        
        return $result;
    }
    
    public function destroyByClientID($id)
    {
        # HARD DELETE All linked by Clients

        $user_info = new UserInfo;
        $transaction = new ClientTransaction;
        $message_out = new MessageOut;
        $allocation = new ClientAllocation;

        $result = $this->model->where(DataField::ID, $id)->delete();
        $user_info->destroyByClientID($id);
        $transaction->destroyByClientID($id);
        $message_out->destroyByClientID($id);
        $allocation->destroyByClientID($id);
        

        return $result;
    }
}