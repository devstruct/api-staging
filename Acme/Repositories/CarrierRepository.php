<?php
namespace Acme\Repositories;

use App\Carrier as Model;
use Illuminate\Support\Facades\Validator;

use Acme\Common\DataFields\Carrier as DataField;

use Acme\Common\Entity\Carrier as Entity;

use Acme\Common\Constants as Constants;
use Acme\Common\Pagination as Pagination;

class CarrierRepository extends Repository{

    protected $model;

    use Pagination;
	
	public function __construct()
	{
		$this->model = new Model;
	}

     public function getByID($id){
        $result = $this->model->where(DataField::ID, $id)->first();

        return $result;
    }

    public function getOptionList($excludeIds = [])
    {
        $result = $this->model->whereNotIn(DataField::ID, $excludeIds)
                    ->get()
                    ->pluck(DataField::CARRIER_NAME, DataField::ID)
                    ->toArray();

        return $result;
    }

    public function list($request){
        $this->SetPage($request);
        $query = $this->model->where(DataField::STATUS,'1');
        
        if ($request->has(Constants::KEYWORD)) {
            $search = trim($request->input(Constants::KEYWORD));
            $query = $query->where(function ($query) use ($search) {
                $query->where(DataField::CARRIER_NAME, 'LIKE', '%' . $search . '%');
            });
        }

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $result =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy($order_by, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);
        

        return $result;
    }

    public function show($id){
        $result = $this->model->find($id);

        return $result;
    }

    public function destroy($id){
        $result = $this->model->where(DataField::ID, $id)->delete();

        return $result;
    }

    public function create($request){
       $input = $request->all();
       $result = $this->model->create($input);

       return $result;
    }

    public function update($request , $id){
       $input = $request->all();
       $result= $this->model->where(DataField::ID,$id)->update($input);

       return $result;
    }

    public function save($request){
        $input = $request->all();
        $result = null;
        
        if(!isset($input[Constants::ID])){
           $result = $this->model->create($input);
        }
        else{
           $result = $this->model->where(DataField::ID, $input[Constants::ID])->update($input);
        }

        return $result;
    }

    public function getAdmin()
    {
        /*
        $result = $this->model->where(DataField::CARRIER_NAME, 'Admin')->first();

        if(!$result)
        {
            $entity = new Entity;
            $result = $entity->Serialize();
        }
        */

        $entity = new Entity;
        $result = $entity->Serialize();

        return $result;
    }
	
	public function getAll()
	{
		$result = $this->model->get(); 
	}
}