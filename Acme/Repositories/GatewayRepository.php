<?php
namespace Acme\Repositories;

use App\Gateway as Model;
use Illuminate\Support\Facades\Validator;

use Acme\Common\Entity\Gateway as Entity;
use Acme\Common\DataFields\Gateway as DataField;
use Acme\Common\Constants as Constants;
use Acme\Common\Pagination as Pagination;
use Illuminate\Pagination\LengthAwarePaginator;

class GatewayRepository extends Repository{

    protected $model;
    use Pagination;
	
	public function __construct()
	{
        $this->model = new Model;
        $this->SortBy = DataField::GATEWAY_ID;
    }
    
    public function rules($data)
    {
        $rules = [
            'name' => 'required|string|max:255|unique:gateways',
            'carrier_id' => 'required',
            'sim' => 'required',
            'type' => 'required',
            'balance' => 'required',
            'minimum' => 'required',
            'status' => 'required'
        ];

        if($data[Constants::ID] != '' && $data[Constants::ID] != null) {
            $rules['name'] = 'required|string|max:255|unique:gateways,name,'.$data[Constants::ID].',GatewayID,Is_Deleted,0';
        }

        return $rules;
    }


    public function getByID($id){
        $result = $this->model->where(DataField::GATEWAY_ID, $id)->first();

        return $result;
    }

    public function show($id){
        $result = $this->model->find($id);

        return $result;
    }

    public function destroy($id){
        $result = $this->model->where(DataField::GATEWAY_ID, $id)->delete();

        return $result;
    }

    public function create($request){
       $input = $request->all();
       $result = $this->model->create($input);

       return $result;
    }

    public function update($request , $id){
       $input = $request->all();
       $result= $this->model->where(DataField::GATEWAY_ID, $id)->update($input);

       return $result;
    }

    public function save($request){
        $input = $request->all();
        $result = null;
        
        if(!isset($input[Constants::ID])){
           $result = $this->model->create($input);
        }
        else{
           $result = $this->model->where(DataField::GATEWAY_ID, $input[Constants::ID])->update($input);
        }

        return $result;
    }

    public function keyValue()
    {
        $result = $this->model->where(DataField::STATUS, Constants::ACTIVE)
                    ->where(DataField::IS_DELETED, 0)
                    ->pluck(DataField::NAME, DataField::GATEWAY_ID);
        return $result;
    }

    public function list($request) 
    {
        $this->SetPage($request);
        $query =  $this->model->where(DataField::STATUS, Constants::ACTIVE)
                    ->where(DataField::IS_DELETED, 0);

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $paginated =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy($order_by, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);

        $carriers = config('gateway.carriers');
        $transformedData = $paginated->getCollection()->transform(function($item) use ($carriers) {
            $item->Carrier = $carriers[$item->CarrierID];

            return $item;
        });

        $transaformedPaginated = new LengthAwarePaginator(
            $transformedData,
            $paginated->total(),
            $paginated->perPage(),
            $paginated->currentPage(), 
            [
                'path' => $request->url(),
                'query' => [ 'page' => $paginated->currentPage() ]
            ]);
            
        return $transaformedPaginated;
    }

    public function delete($id)
    {
        $result = null;
        $result = $this->model->where(DataField::GATEWAY_ID, $id)->update([DataField::IS_DELETED => 1]);

        return $result;
    }

    public function processForInput($row)
    {
        $data = [];
        $data[Constants::ID] = $row->GatewayID;
        $data['name'] = $row->Name;
        $data['carrier_id'] = $row->CarrierID;
        $data['address'] = $row->Address;
        $data['sim'] = $row->SIM;
        $data['type'] = $row->Type;
        $data['balance'] = $row->Balance;
        $data['minimum'] = $row->Minimum;
        $data['priority'] = $row->Priority;
        $data['status'] = $row->Status;

        return $data;
    }

    public function adjustBalance($id, $value)
    {
        $this->model->where(DataField::GATEWAY_ID, $id)
                ->increment(DataField::BALANCE, $value);
                
        return true;
    }

    public function adjustBalanceByName($name, $value)
    {
        $this->model->where(DataField::NAME, $name)
                ->increment(DataField::BALANCE, $value);
                
        return true;
    }
    
    public function process($request)
    {
        $input = $request->all();
        $entity = new Entity;
        $entity->SetData($input);
        
        if($entity->GatewayID != '' && $entity->GatewayID != null) {
            $result = $this->model->where(DataField::GATEWAY_ID, $entity->GatewayID)
                        ->update($entity->Serialize());
        } else {
            $result = $this->model->create($entity->Serialize());
        }
        return $result;
    }

    public function getAvailableGateway($carrier , $amount)
    {
        $result = null;

        $result = $this->model->where(DataField::CARRIER_ID, $carrier)
                       ->where(DataField::BALANCE ,">", $amount)
                       ->where(DataField::IS_DELETED , 0)
                       ->orderBy(DataField::BALANCE, 'desc')
                       ->first();

        return $result;
    }

    public function getByNumber($number)
    {
        $result = null;

        $result = $this->model->where(DataField::SIM, $number)
                       ->where(DataField::IS_DELETED , 0)
                       ->first();

        return $result;
    }

    public function updateBalanceByID($id , $balance)
    {
        $result= $this->model
                        ->where(DataField::GATEWAY_ID, $id)
                        ->update([DataField::BALANCE => $balance]);
    }

    public function getActive()
    {
        $result = null;

        $result = $this->model
                       ->where(DataField::STATUS , 1)
                       ->get();

        return $result;
    }

}