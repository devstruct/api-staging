-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for apiserver
DROP DATABASE IF EXISTS `apiserver`;
CREATE DATABASE IF NOT EXISTS `apiserver` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `apiserver`;

-- Dumping structure for table apiserver.allocation_transactions
DROP TABLE IF EXISTS `allocation_transactions`;
CREATE TABLE IF NOT EXISTS `allocation_transactions` (
  `AllocationTransactionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AllocationID` int(11) NOT NULL DEFAULT '0',
  `Credits` double NOT NULL DEFAULT '0',
  `Rate` double NOT NULL DEFAULT '1',
  `Status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`AllocationTransactionID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.allocation_transactions: ~11 rows (approximately)
DELETE FROM `allocation_transactions`;
/*!40000 ALTER TABLE `allocation_transactions` DISABLE KEYS */;
INSERT INTO `allocation_transactions` (`AllocationTransactionID`, `AllocationID`, `Credits`, `Rate`, `Status`, `created_at`, `updated_at`) VALUES
	(1, 1, 150000, 1, 5, '2019-05-25 20:24:17', '2019-05-25 20:24:17'),
	(2, 2, 200000, 1, 5, '2019-05-25 22:03:31', '2019-05-25 22:03:31'),
	(3, 2, 100, 1, 4, '2019-05-25 22:36:01', '2019-05-25 22:36:01'),
	(4, 2, -100, 1, 4, '2019-05-25 22:36:15', '2019-05-25 22:36:15'),
	(5, 1, 100, 1, 4, '2019-05-25 22:42:31', '2019-05-25 22:42:31'),
	(6, 1, -100, 1, 4, '2019-05-25 22:42:42', '2019-05-25 22:42:42'),
	(7, 1, 1, 1, 4, '2019-05-25 22:48:31', '2019-05-25 22:48:31'),
	(8, 1, -1, 1, 4, '2019-05-25 22:48:38', '2019-05-25 22:48:38'),
	(9, 1, 100, 1, 4, '2019-05-25 23:19:46', '2019-05-25 23:19:46'),
	(10, 1, -100, 1, 4, '2019-05-25 23:31:29', '2019-05-25 23:31:29'),
	(11, 1, 100, 1, 4, '2019-05-25 23:31:48', '2019-05-25 23:31:48');
/*!40000 ALTER TABLE `allocation_transactions` ENABLE KEYS */;

-- Dumping structure for table apiserver.carriers
DROP TABLE IF EXISTS `carriers`;
CREATE TABLE IF NOT EXISTS `carriers` (
  `CarrierID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CarrierName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`CarrierID`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.carriers: ~4 rows (approximately)
DELETE FROM `carriers`;
/*!40000 ALTER TABLE `carriers` DISABLE KEYS */;
INSERT INTO `carriers` (`CarrierID`, `CarrierName`, `Status`, `created_at`, `updated_at`) VALUES
	(1, 'Smart', 1, '2019-05-25 20:23:31', NULL),
	(2, 'Globe', 1, '2019-05-25 20:23:31', NULL),
	(3, 'Sun', 1, '2019-05-25 20:23:31', NULL),
	(99, 'System', 1, '2019-05-25 20:23:31', NULL);
/*!40000 ALTER TABLE `carriers` ENABLE KEYS */;

-- Dumping structure for table apiserver.client_allocations
DROP TABLE IF EXISTS `client_allocations`;
CREATE TABLE IF NOT EXISTS `client_allocations` (
  `AllocationID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ClientID` int(11) NOT NULL,
  `Interval` int(11) NOT NULL,
  `Budget` decimal(11,2) NOT NULL,
  `Consumed` decimal(11,2) NOT NULL,
  `Rate` decimal(11,2) NOT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Success` int(11) NOT NULL DEFAULT '0',
  `Failed` int(11) NOT NULL DEFAULT '0',
  `Status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`AllocationID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.client_allocations: ~2 rows (approximately)
DELETE FROM `client_allocations`;
/*!40000 ALTER TABLE `client_allocations` DISABLE KEYS */;
INSERT INTO `client_allocations` (`AllocationID`, `ClientID`, `Interval`, `Budget`, `Consumed`, `Rate`, `StartDate`, `EndDate`, `Success`, `Failed`, `Status`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, 150100.00, 51.82, 1.00, '2019-05-25 00:00:00', '2019-05-25 00:00:00', 0, 3, 5, '2019-05-25 20:24:17', '2019-05-25 23:31:48'),
	(2, 3, 2, 200000.00, 28.74, 1.00, '2019-05-25 00:00:00', '2019-05-31 00:00:00', 1, 0, 5, '2019-05-25 22:03:30', '2019-05-25 22:49:52');
/*!40000 ALTER TABLE `client_allocations` ENABLE KEYS */;

-- Dumping structure for table apiserver.client_credits
DROP TABLE IF EXISTS `client_credits`;
CREATE TABLE IF NOT EXISTS `client_credits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `balance` double NOT NULL DEFAULT '0',
  `consumed` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.client_credits: ~0 rows (approximately)
DELETE FROM `client_credits`;
/*!40000 ALTER TABLE `client_credits` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_credits` ENABLE KEYS */;

-- Dumping structure for table apiserver.client_transactions
DROP TABLE IF EXISTS `client_transactions`;
CREATE TABLE IF NOT EXISTS `client_transactions` (
  `TransactionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ClientID` int(11) NOT NULL,
  `RecipientNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ProductCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CarrierID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Amount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `Balance` decimal(11,2) NOT NULL DEFAULT '0.00',
  `AmountCharged` decimal(11,2) NOT NULL DEFAULT '0.00',
  `Type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '0',
  `Reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`TransactionID`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.client_transactions: ~20 rows (approximately)
DELETE FROM `client_transactions`;
/*!40000 ALTER TABLE `client_transactions` DISABLE KEYS */;
INSERT INTO `client_transactions` (`TransactionID`, `ClientID`, `RecipientNumber`, `ProductCode`, `CarrierID`, `Amount`, `Balance`, `AmountCharged`, `Type`, `Status`, `Reference`, `created_at`, `updated_at`) VALUES
	(1, 2, '', 'INITIAL CREDIT', '99', 150000.00, 0.00, 0.00, '5', 5, '0', '2019-05-25 20:24:17', '2019-05-25 20:24:17'),
	(2, 2, '09386520297', 'GAANSURF10', '1', -10.00, 149990.63, -9.37, '3', 1, '670556658987', '2019-05-25 20:27:43', '2019-05-25 20:31:13'),
	(11, 2, '09398861266', 'AT10', '1', -10.00, 149981.27, -9.36, '3', 2, '0', '2019-05-25 20:58:29', '2019-05-25 23:18:47'),
	(12, 2, '09398861266', '10', '1', -10.00, 149971.91, -9.36, '3', 1, '676424601428', '2019-05-25 21:00:00', '2019-05-25 21:01:47'),
	(13, 2, '09083140480', '10', '1', -10.00, 149962.55, -9.36, '3', 1, '676622999462', '2019-05-25 21:11:49', '2019-05-25 21:14:36'),
	(14, 2, '09495682320', '10', '1', -10.00, 149953.19, -9.36, '3', 1, '676722600355', '2019-05-25 21:24:13', '2019-05-25 21:26:05'),
	(15, 2, '09388257463', '15', '1', -15.00, 149938.82, -14.37, '3', 1, '676822499740', '2019-05-25 21:40:56', '2019-05-25 21:42:54'),
	(16, 3, '', 'INITIAL CREDIT', '99', 200000.00, 0.00, 0.00, '5', 5, '0', '2019-05-25 22:03:31', '2019-05-25 22:03:31'),
	(17, 3, '09388257463', '15', '1', -15.00, 199985.63, -14.37, '3', 1, '676424602216', '2019-05-25 22:11:32', '2019-05-25 22:34:08'),
	(18, 3, '', 'ADD CREDITS', '99', 100.00, 200100.00, 0.00, '0', 4, '0', '2019-05-25 22:36:01', '2019-05-25 22:36:01'),
	(19, 3, '', 'REMOVE CREDITS', '99', -100.00, 200000.00, 0.00, '1', 4, '0', '2019-05-25 22:36:15', '2019-05-25 22:36:15'),
	(20, 2, '', 'ADD CREDITS', '99', 100.00, 150038.82, 0.00, '0', 4, '0', '2019-05-25 22:42:31', '2019-05-25 22:42:31'),
	(21, 2, '', 'REMOVE CREDITS', '99', -100.00, 149938.82, 0.00, '1', 4, '0', '2019-05-25 22:42:42', '2019-05-25 22:42:42'),
	(22, 2, '', 'ADD CREDITS', '99', 1.00, 149939.82, 0.00, '0', 4, '0', '2019-05-25 22:48:31', '2019-05-25 22:48:31'),
	(23, 2, '', 'REMOVE CREDITS', '99', -1.00, 149938.82, 0.00, '1', 4, '0', '2019-05-25 22:48:38', '2019-05-25 22:48:38'),
	(24, 3, '09398861266', '15', '1', -15.00, 199971.26, -14.37, '3', 1, '676623000366', '2019-05-25 22:49:52', '2019-05-25 22:52:07'),
	(28, 2, '09398861266', 'AT10', '1', 10.00, 149948.18, 9.36, '6', 6, '11', '2019-05-25 23:18:47', '2019-05-25 23:18:47'),
	(29, 2, '', 'ADD CREDITS', '99', 100.00, 150048.18, 0.00, '0', 4, '0', '2019-05-25 23:19:47', '2019-05-25 23:19:47'),
	(30, 2, '', 'REMOVE CREDITS', '99', -100.00, 149948.18, 0.00, '1', 4, '0', '2019-05-25 23:31:29', '2019-05-25 23:31:29'),
	(31, 2, '', 'ADD CREDITS', '99', 100.00, 150048.18, 0.00, '0', 4, '0', '2019-05-25 23:31:48', '2019-05-25 23:31:48');
/*!40000 ALTER TABLE `client_transactions` ENABLE KEYS */;

-- Dumping structure for table apiserver.gateways
DROP TABLE IF EXISTS `gateways`;
CREATE TABLE IF NOT EXISTS `gateways` (
  `GatewayID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CarrierID` int(11) NOT NULL,
  `Address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SIM` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Type` int(11) NOT NULL,
  `Balance` double NOT NULL,
  `Minimum` double NOT NULL,
  `Priority` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Is_Deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`GatewayID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.gateways: ~2 rows (approximately)
DELETE FROM `gateways`;
/*!40000 ALTER TABLE `gateways` DISABLE KEYS */;
INSERT INTO `gateways` (`GatewayID`, `Name`, `CarrierID`, `Address`, `SIM`, `Type`, `Balance`, `Minimum`, `Priority`, `Status`, `created_at`, `updated_at`, `Is_Deleted`) VALUES
	(1, 'Globe01', 2, NULL, '09155243797', 1, 1000000, 100, 1, 1, '2019-05-25 20:23:31', NULL, 0),
	(2, 'Smart01', 1, NULL, '09997459245', 1, 212.65999999999997, 100, 1, 1, '2019-05-25 20:23:31', '2019-05-25 22:52:07', 0);
/*!40000 ALTER TABLE `gateways` ENABLE KEYS */;

-- Dumping structure for table apiserver.inbox
DROP TABLE IF EXISTS `inbox`;
CREATE TABLE IF NOT EXISTS `inbox` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SendTime` datetime DEFAULT NULL,
  `ReceiveTime` datetime DEFAULT NULL,
  `MessageFrom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SMSC` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageText` text COLLATE utf8mb4_unicode_ci,
  `MessageType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageParts` int(11) DEFAULT NULL,
  `MessagePDU` text COLLATE utf8mb4_unicode_ci,
  `Gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.inbox: ~0 rows (approximately)
DELETE FROM `inbox`;
/*!40000 ALTER TABLE `inbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbox` ENABLE KEYS */;

-- Dumping structure for table apiserver.messagein
DROP TABLE IF EXISTS `messagein`;
CREATE TABLE IF NOT EXISTS `messagein` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SendTime` datetime DEFAULT NULL,
  `ReceiveTime` datetime DEFAULT NULL,
  `MessageFrom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SMSC` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageText` text COLLATE utf8mb4_unicode_ci,
  `MessageType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageParts` int(11) DEFAULT NULL,
  `MessagePDU` text COLLATE utf8mb4_unicode_ci,
  `Gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Scanned` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.messagein: ~8 rows (approximately)
DELETE FROM `messagein`;
/*!40000 ALTER TABLE `messagein` DISABLE KEYS */;
INSERT INTO `messagein` (`Id`, `SendTime`, `ReceiveTime`, `MessageFrom`, `MessageTo`, `SMSC`, `MessageText`, `MessageType`, `MessageParts`, `MessagePDU`, `Gateway`, `UserId`, `Scanned`, `created_at`, `updated_at`) VALUES
	(1, '2019-05-25 20:27:51', NULL, 'TNTLOAD', '639997459245', NULL, '25-May 20:27:09997459245 has loaded Gaan Surf 10 (P9.55) to 09386520297. New Load Wallet Balance:P284.30. Ref:670556658987', NULL, NULL, NULL, NULL, '', 1, '2019-05-25 20:29:13', '2019-05-25 20:31:13'),
	(2, '2019-05-25 20:58:36', NULL, 'SMART', '639997459245', NULL, 'The keyword sent by you is invalid, please try again using the correct keyword.', NULL, NULL, NULL, NULL, '', 0, '2019-05-25 20:58:42', NULL),
	(3, '2019-05-25 21:00:07', NULL, 'SMARTLoad', '639997459245', NULL, '25-May 21:00:09997459245 has loaded All Text 10 (P9.55) to 09398861266. New Load Wallet Balance:P274.75. Ref:676424601428', NULL, NULL, NULL, NULL, '', 1, '2019-05-25 21:01:30', '2019-05-25 21:01:47'),
	(4, '2019-05-25 21:11:55', NULL, 'SMARTLoad', '639997459245', NULL, '25-May 21:11:09997459245 has loaded All Text 10 (P9.55) to 09083140480. New Load Wallet Balance:P265.20. Ref:676622999462', NULL, NULL, NULL, NULL, '', 1, '2019-05-25 21:13:17', '2019-05-25 21:14:36'),
	(5, '2019-05-25 21:24:18', NULL, 'SMARTLoad', '09997459245', NULL, '25-May 21:24:09997459245 has loaded All Text 10 (P9.55) to 09495682320. New Load Wallet Balance:P255.65. Ref:676722600355', NULL, NULL, NULL, NULL, '', 1, '2019-05-25 21:25:40', '2019-05-25 21:26:05'),
	(6, '2019-05-25 21:41:01', NULL, 'TNTLoad', '639997459245', NULL, '25-May 21:41:09997459245 has loaded Regular Load P15 (P14.33) to 09388257463. New Load Wallet: P241.32. Ref:676822499740', NULL, NULL, NULL, NULL, '', 1, '2019-05-25 21:42:20', '2019-05-25 21:42:54'),
	(7, '2019-05-25 22:11:40', NULL, 'TNTLoad', '639997459245', NULL, '25-May 22:11:09997459245 has loaded Regular Load P15 (P14.33) to 09388257463. New Load Wallet: P226.99. Ref:676424602216', NULL, NULL, NULL, NULL, '', 1, '2019-05-25 22:13:01', '2019-05-25 22:34:08'),
	(8, '2019-05-25 22:49:56', NULL, 'SMARTLoad', '639997459245', NULL, '25-May 22:49:09997459245 has loaded Regular Load P15 (P14.33) to 09398861266. New Load Wallet: P212.66. Ref:676623000366', NULL, NULL, NULL, NULL, '', 1, '2019-05-25 22:51:20', '2019-05-25 22:52:06');
/*!40000 ALTER TABLE `messagein` ENABLE KEYS */;

-- Dumping structure for table apiserver.messagelog
DROP TABLE IF EXISTS `messagelog`;
CREATE TABLE IF NOT EXISTS `messagelog` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SendTime` datetime DEFAULT NULL,
  `ReceiveTime` datetime DEFAULT NULL,
  `StatusCode` int(11) DEFAULT NULL,
  `StatusText` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageFrom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageText` text COLLATE utf8mb4_unicode_ci,
  `MessageType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageGuid` text COLLATE utf8mb4_unicode_ci,
  `MessageInfo` text COLLATE utf8mb4_unicode_ci,
  `ErrorCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ErrorText` text COLLATE utf8mb4_unicode_ci,
  `Gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageParts` int(11) DEFAULT NULL,
  `MessagePDU` text COLLATE utf8mb4_unicode_ci,
  `Connector` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserInfo` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.messagelog: ~8 rows (approximately)
DELETE FROM `messagelog`;
/*!40000 ALTER TABLE `messagelog` DISABLE KEYS */;
INSERT INTO `messagelog` (`Id`, `SendTime`, `ReceiveTime`, `StatusCode`, `StatusText`, `MessageTo`, `MessageFrom`, `MessageText`, `MessageType`, `MessageId`, `MessageGuid`, `MessageInfo`, `ErrorCode`, `ErrorText`, `Gateway`, `MessageParts`, `MessagePDU`, `Connector`, `UserId`, `UserInfo`, `created_at`, `updated_at`) VALUES
	(1, '2019-05-25 20:27:51', NULL, 200, NULL, '4540', NULL, 'GAANSURF10 09386520297', NULL, '18:4540:81', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL),
	(2, '2019-05-25 20:58:36', NULL, 200, NULL, '343', NULL, 'AT10 09398861266', NULL, '18:343:82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', NULL, NULL, NULL),
	(3, '2019-05-25 21:00:07', NULL, 200, NULL, '343', NULL, '10 09398861266', NULL, '18:343:83', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', NULL, NULL, NULL),
	(4, '2019-05-25 21:11:54', NULL, 200, NULL, '343', NULL, '10 09083140480', NULL, '18:343:84', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL),
	(5, '2019-05-25 21:24:18', NULL, 200, NULL, '343', NULL, '10 09495682320', NULL, '18:343:85', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '14', NULL, NULL, NULL),
	(6, '2019-05-25 21:41:01', NULL, 200, NULL, '343', NULL, '15 09388257463', NULL, '18:343:86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', NULL, NULL, NULL),
	(7, '2019-05-25 22:11:40', NULL, 200, NULL, '343', NULL, '15 09388257463', NULL, '18:343:87', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '17', NULL, NULL, NULL),
	(8, '2019-05-25 22:49:56', NULL, 200, NULL, '343', NULL, '15 09398861266', NULL, '18:343:88', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '24', NULL, NULL, NULL);
/*!40000 ALTER TABLE `messagelog` ENABLE KEYS */;

-- Dumping structure for table apiserver.messageout
DROP TABLE IF EXISTS `messageout`;
CREATE TABLE IF NOT EXISTS `messageout` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MessageTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageFrom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageText` text COLLATE utf8mb4_unicode_ci,
  `MessageType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MessageGuid` text COLLATE utf8mb4_unicode_ci,
  `MessageInfo` text COLLATE utf8mb4_unicode_ci,
  `Gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UserInfo` text COLLATE utf8mb4_unicode_ci,
  `Priority` int(11) DEFAULT NULL,
  `Scheduled` datetime DEFAULT NULL,
  `ValidityPeriod` int(11) DEFAULT NULL,
  `IsSent` tinyint(4) NOT NULL DEFAULT '0',
  `IsRead` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.messageout: ~8 rows (approximately)
DELETE FROM `messageout`;
/*!40000 ALTER TABLE `messageout` DISABLE KEYS */;
INSERT INTO `messageout` (`Id`, `MessageTo`, `MessageFrom`, `MessageText`, `MessageType`, `MessageGuid`, `MessageInfo`, `Gateway`, `UserId`, `UserInfo`, `Priority`, `Scheduled`, `ValidityPeriod`, `IsSent`, `IsRead`, `created_at`, `updated_at`) VALUES
	(1, '4540', '09386520297', 'GAANSURF10 09386520297', 'Default', NULL, NULL, 'Smart01', '2', '0', 0, '2019-05-25 20:27:43', 0, 1, 0, '2019-05-25 20:27:43', '2019-05-25 20:27:43'),
	(2, '343', '09398861266', 'AT10 09398861266', 'Default', NULL, NULL, 'Smart01', '11', '0', 0, '2019-05-25 20:58:29', 0, 1, 0, '2019-05-25 20:58:29', '2019-05-25 20:58:29'),
	(3, '343', '09398861266', '10 09398861266', 'Default', NULL, NULL, 'Smart01', '12', '0', 0, '2019-05-25 21:00:00', 0, 1, 0, '2019-05-25 21:00:00', '2019-05-25 21:00:00'),
	(4, '343', '09083140480', '10 09083140480', 'Default', NULL, NULL, 'Smart01', '13', '0', 0, '2019-05-25 21:11:49', 0, 1, 0, '2019-05-25 21:11:49', '2019-05-25 21:11:49'),
	(5, '343', '09495682320', '10 09495682320', 'Default', NULL, NULL, 'Smart01', '14', '0', 0, '2019-05-25 21:24:13', 0, 1, 0, '2019-05-25 21:24:13', '2019-05-25 21:24:13'),
	(6, '343', '09388257463', '15 09388257463', 'Default', NULL, NULL, 'Smart01', '15', '0', 0, '2019-05-25 21:40:56', 0, 1, 0, '2019-05-25 21:40:56', '2019-05-25 21:40:56'),
	(7, '343', '09388257463', '15 09388257463', 'Default', NULL, NULL, 'Smart01', '17', '0', 0, '2019-05-25 22:11:33', 0, 1, 0, '2019-05-25 22:11:33', '2019-05-25 22:11:33'),
	(8, '343', '09398861266', '15 09398861266', 'Default', NULL, NULL, 'Smart01', '24', '0', 0, '2019-05-25 22:49:52', 0, 1, 0, '2019-05-25 22:49:52', '2019-05-25 22:49:52');
/*!40000 ALTER TABLE `messageout` ENABLE KEYS */;

-- Dumping structure for table apiserver.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.migrations: ~49 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2019_02_24_053959_create_messageout_table', 1),
	(9, '2019_02_24_091620_create_messagein_table', 1),
	(10, '2019_02_24_091947_create_messagelog_table', 1),
	(11, '2019_02_24_161420_create_client_credits_table', 1),
	(12, '2019_02_24_161624_create_client_transactions_table', 1),
	(13, '2019_02_25_033108_create_userinfo_table', 1),
	(14, '2019_03_01_022120_create_carriers_table', 1),
	(15, '2019_03_01_022307_create_gateways_table', 1),
	(16, '2019_03_01_022403_create__product_code_table', 1),
	(17, '2019_03_01_080606_create__client_allocation_table', 1),
	(18, '2019_03_02_124326_change_field_names_userinfo_table', 1),
	(19, '2019_03_02_130153_change_field_names_client_transactions_table', 1),
	(20, '2019_03_09_101004_add_is_active_column_users_table', 1),
	(21, '2019_03_09_104018_remove_type_column_to_userinfo_table', 1),
	(22, '2019_03_09_104033_add_type_column_to_users_table', 1),
	(23, '2019_03_12_151854_add_username_last_login_fields_users_table', 1),
	(24, '2019_03_16_114313_add_product_code_amount_charged_columns_client_transactions_table', 1),
	(25, '2019_03_16_115009_add_amountcharged_product_codes_table', 1),
	(26, '2019_03_16_115213_add_interval_company_name_userinfo_table', 1),
	(27, '2019_03_16_115241_remove_name_users_table', 1),
	(28, '2019_03_16_115302_add_credit_transactions_table', 1),
	(29, '2019_03_16_132551_add_credits_column_userinfo_table', 1),
	(30, '2019_03_17_172023_add_balance_column_client_transactions', 1),
	(31, '2019_03_20_103150_change_address_default_null_gateways_table', 1),
	(32, '2019_03_20_111535_add_is_deleted_column_users_table', 1),
	(33, '2019_03_20_112450_remove_unique_email_users_table', 1),
	(34, '2019_03_20_113220_add_is_deleted_column_gateways_table', 1),
	(35, '2019_03_24_123411_add_is_deleted_column_product_code_table', 1),
	(36, '2019_03_29_002013_set_start_end_date_nullable_client_allocation_table', 1),
	(37, '2019_04_16_144101_add_name_column_product_codes_table', 1),
	(38, '2019_04_16_200019_create_prefix_table', 1),
	(39, '2019_04_20_171830_change_created_at_message_in_table', 1),
	(40, '2019_05_02_202452_add_scanned_to_message_in_table', 1),
	(41, '2019_05_02_212507_add_status_to_client_transactions_table', 1),
	(42, '2019_05_04_135217_add_is_recurring_to_user_info_table', 1),
	(43, '2019_05_04_212336_add_reference_to_client_transaction', 1),
	(44, '2019_05_10_165442_change_budget_datatype_client_allocations_table', 1),
	(45, '2019_05_10_170146_change_budget_datatype_client_transaction_table', 1),
	(46, '2019_05_12_104735_add_success_failed_to_client_allocation_table', 1),
	(47, '2019_05_19_150402_create_inbox_table', 1),
	(48, '2019_05_24_223639_add_template_to_product_code_table', 1),
	(49, '2019_05_25_172719_change_ussd_template_default_product_codes_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table apiserver.oauth_access_tokens
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.oauth_access_tokens: ~2 rows (approximately)
DELETE FROM `oauth_access_tokens`;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('299cec6a68a5d3127d079eefe567e9bef90099b238b107fde8c7dc214683626d77ab1bda8eb72d53', 2, 1, NULL, '[]', 0, '2019-05-25 20:24:33', '2019-05-25 20:24:33', '2020-05-25 20:24:33'),
	('2fb28a68054303de232f26814d261ebca8a6f3c481e32ff7e7470987792bf92dd1a314898c8c865c', 3, 2, NULL, '[]', 0, '2019-05-25 22:04:09', '2019-05-25 22:04:09', '2020-05-25 22:04:09');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table apiserver.oauth_auth_codes
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.oauth_auth_codes: ~0 rows (approximately)
DELETE FROM `oauth_auth_codes`;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table apiserver.oauth_clients
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.oauth_clients: ~2 rows (approximately)
DELETE FROM `oauth_clients`;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Union Bank', 'jjmQ6QjY7gBofNkBAMekjVzczgrkkNxGubaISPth', '', 0, 1, 0, '2019-05-25 20:24:17', '2019-05-25 20:24:17'),
	(2, 3, 'PNB', 'PwfxJeXD3DCoS7NJ0L0uaUUAV3cVjFhxxU0mlFIe', '', 0, 1, 0, '2019-05-25 22:03:31', '2019-05-25 22:03:31');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table apiserver.oauth_personal_access_clients
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.oauth_personal_access_clients: ~0 rows (approximately)
DELETE FROM `oauth_personal_access_clients`;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table apiserver.oauth_refresh_tokens
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.oauth_refresh_tokens: ~2 rows (approximately)
DELETE FROM `oauth_refresh_tokens`;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
	('c423d854d61d7cf963534882f5e99645beaf306447963d63365aa9b89ac1559659549bf888a8043d', '2fb28a68054303de232f26814d261ebca8a6f3c481e32ff7e7470987792bf92dd1a314898c8c865c', 0, '2020-05-25 22:04:09'),
	('de807b351b67b2cfc91f20858e75ec3e2d0fc669142ce9e3dbdfdebed49b5df210dd07a17e36eac7', '299cec6a68a5d3127d079eefe567e9bef90099b238b107fde8c7dc214683626d77ab1bda8eb72d53', 0, '2020-05-25 20:24:33');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table apiserver.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table apiserver.prefix
DROP TABLE IF EXISTS `prefix`;
CREATE TABLE IF NOT EXISTS `prefix` (
  `PrefixID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CarrierID` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PrefixID`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.prefix: ~72 rows (approximately)
DELETE FROM `prefix`;
/*!40000 ALTER TABLE `prefix` DISABLE KEYS */;
INSERT INTO `prefix` (`PrefixID`, `Code`, `CarrierID`, `created_at`, `updated_at`) VALUES
	(1, '0817', 2, '2019-05-25 20:23:31', NULL),
	(2, '0973', 2, '2019-05-25 20:23:31', NULL),
	(3, '09173', 2, '2019-05-25 20:23:31', NULL),
	(4, '09178', 2, '2019-05-25 20:23:31', NULL),
	(5, '09175', 2, '2019-05-25 20:23:31', NULL),
	(6, '09253', 2, '2019-05-25 20:23:31', NULL),
	(7, '09257', 2, '2019-05-25 20:23:31', NULL),
	(8, '09176', 2, '2019-05-25 20:23:31', NULL),
	(9, '09255', 2, '2019-05-25 20:23:31', NULL),
	(10, '09258', 2, '2019-05-25 20:23:31', NULL),
	(11, '0922', 3, '2019-05-25 20:23:31', NULL),
	(12, '0931', 3, '2019-05-25 20:23:31', NULL),
	(13, '0941', 3, '2019-05-25 20:23:31', NULL),
	(14, '0923', 3, '2019-05-25 20:23:31', NULL),
	(15, '0932', 3, '2019-05-25 20:23:31', NULL),
	(16, '0942', 3, '2019-05-25 20:23:31', NULL),
	(17, '0924', 3, '2019-05-25 20:23:31', NULL),
	(18, '0933', 3, '2019-05-25 20:23:31', NULL),
	(19, '0943', 3, '2019-05-25 20:23:31', NULL),
	(20, '0925', 3, '2019-05-25 20:23:31', NULL),
	(21, '0934', 3, '2019-05-25 20:23:31', NULL),
	(22, '0944', 3, '2019-05-25 20:23:31', NULL),
	(23, '0907', 1, '2019-05-25 20:23:31', NULL),
	(24, '0912', 1, '2019-05-25 20:23:31', NULL),
	(25, '0946', 1, '2019-05-25 20:23:31', NULL),
	(26, '0909', 1, '2019-05-25 20:23:31', NULL),
	(27, '0930', 1, '2019-05-25 20:23:31', NULL),
	(28, '0948', 1, '2019-05-25 20:23:31', NULL),
	(29, '0910', 1, '2019-05-25 20:23:31', NULL),
	(30, '0938', 1, '2019-05-25 20:23:31', NULL),
	(31, '0950', 1, '2019-05-25 20:23:31', NULL),
	(32, '0813', 1, '2019-05-25 20:23:31', NULL),
	(33, '0913', 1, '2019-05-25 20:23:31', NULL),
	(34, '0919', 1, '2019-05-25 20:23:31', NULL),
	(35, '0928', 1, '2019-05-25 20:23:31', NULL),
	(36, '0947', 1, '2019-05-25 20:23:31', NULL),
	(37, '0981', 1, '2019-05-25 20:23:31', NULL),
	(38, '0908', 1, '2019-05-25 20:23:31', NULL),
	(39, '0914', 1, '2019-05-25 20:23:31', NULL),
	(40, '0920', 1, '2019-05-25 20:23:31', NULL),
	(41, '0929', 1, '2019-05-25 20:23:31', NULL),
	(42, '0949', 1, '2019-05-25 20:23:31', NULL),
	(43, '0989', 1, '2019-05-25 20:23:31', NULL),
	(44, '0911', 1, '2019-05-25 20:23:31', NULL),
	(45, '0918', 1, '2019-05-25 20:23:31', NULL),
	(46, '0921', 1, '2019-05-25 20:23:31', NULL),
	(47, '0939', 1, '2019-05-25 20:23:31', NULL),
	(48, '0970', 1, '2019-05-25 20:23:31', NULL),
	(49, '0998', 1, '2019-05-25 20:23:31', NULL),
	(50, '0904', 2, '2019-05-25 20:23:31', NULL),
	(51, '0916', 2, '2019-05-25 20:23:31', NULL),
	(52, '0935', 2, '2019-05-25 20:23:31', NULL),
	(53, '0965', 2, '2019-05-25 20:23:31', NULL),
	(54, '0976', 2, '2019-05-25 20:23:31', NULL),
	(55, '0994', 2, '2019-05-25 20:23:31', NULL),
	(56, '0905', 2, '2019-05-25 20:23:31', NULL),
	(57, '0917', 2, '2019-05-25 20:23:31', NULL),
	(58, '0936', 2, '2019-05-25 20:23:31', NULL),
	(59, '0966', 2, '2019-05-25 20:23:31', NULL),
	(60, '0977', 2, '2019-05-25 20:23:31', NULL),
	(61, '0995', 2, '2019-05-25 20:23:31', NULL),
	(62, '0906', 2, '2019-05-25 20:23:31', NULL),
	(63, '0926', 2, '2019-05-25 20:23:31', NULL),
	(64, '0945', 2, '2019-05-25 20:23:31', NULL),
	(65, '0967', 2, '2019-05-25 20:23:31', NULL),
	(66, '0978', 2, '2019-05-25 20:23:31', NULL),
	(67, '0997', 2, '2019-05-25 20:23:31', NULL),
	(68, '0915', 2, '2019-05-25 20:23:31', NULL),
	(69, '0927', 2, '2019-05-25 20:23:31', NULL),
	(70, '0956', 2, '2019-05-25 20:23:31', NULL),
	(71, '0975', 2, '2019-05-25 20:23:31', NULL),
	(72, '0979', 2, '2019-05-25 20:23:31', NULL);
/*!40000 ALTER TABLE `prefix` ENABLE KEYS */;

-- Dumping structure for table apiserver.product_codes
DROP TABLE IF EXISTS `product_codes`;
CREATE TABLE IF NOT EXISTS `product_codes` (
  `ProductCodeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CarrierID` int(11) NOT NULL,
  `RegisterTo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `USSDTemplate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `Amount` double NOT NULL,
  `AmountCharged` double NOT NULL DEFAULT '0',
  `Status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Is_Deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProductCodeID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.product_codes: ~32 rows (approximately)
DELETE FROM `product_codes`;
/*!40000 ALTER TABLE `product_codes` DISABLE KEYS */;
INSERT INTO `product_codes` (`ProductCodeID`, `CarrierID`, `RegisterTo`, `Code`, `Name`, `USSDTemplate`, `Amount`, `AmountCharged`, `Status`, `created_at`, `updated_at`, `Is_Deleted`) VALUES
	(1, 1, '343', '15', 'Regular Load 15', '', 15, 14.37, 1, NULL, NULL, 0),
	(2, 1, '343', '30', 'Regular Load 30', '', 30, 28.07, 1, NULL, NULL, 0),
	(3, 1, '343', '50', 'Regular Load 50', '', 50, 46.78, 1, NULL, NULL, 0),
	(4, 1, '343', '60', 'Regular Load 60', '', 60, 56.14, 1, NULL, NULL, 0),
	(5, 1, '343', '100', 'Regular Load 100', '', 100, 93.56, 1, NULL, NULL, 0),
	(6, 1, '343', '115', 'Regular Load 115', '', 115, 107.59, 1, NULL, NULL, 0),
	(7, 1, '343', '200', 'Regular Load 200', '', 200, 187.12, 1, NULL, NULL, 0),
	(8, 1, '343', '300', 'Regular Load 300', '', 300, 280.68, 1, NULL, NULL, 0),
	(9, 1, '343', '500', 'Regular Load 500', '', 500, 467.8, 1, NULL, NULL, 0),
	(10, 1, '343', '1000', 'Regular Load 1000', '', 1000, 935.6, 1, NULL, NULL, 0),
	(11, 1, '6406', 'Text50', 'Big Unlitext 50', '', 50, 46.78, 1, NULL, NULL, 0),
	(12, 1, '6406', 'Call100', 'Big Calls 100', '', 100, 93.56, 1, NULL, NULL, 0),
	(13, 1, '343', 'AT30', 'All Text 30 Plus', '', 30, 28.07, 1, NULL, NULL, 0),
	(14, 1, '343', '10', 'All Text 10', '\'\'', 10, 9.36, 1, NULL, '2019-05-25 20:59:52', 0),
	(15, 1, '343', 'ALLIN99', 'All-In 99', '', 99, 92.62, 1, NULL, NULL, 0),
	(16, 1, '343', 'AI250', 'Mega All-In 250', '', 250, 233.9, 1, NULL, NULL, 0),
	(17, 1, '6707', 'UCT25', 'Unli Call & Text 25', '', 25, 23.39, 1, NULL, NULL, 0),
	(18, 1, '6707', 'UCT30', 'Unli Call & Text 30', '', 30, 28.07, 1, NULL, NULL, 0),
	(19, 1, '6707', 'UCT50', 'Unli Call & Text 50', '', 50, 46.78, 1, NULL, NULL, 0),
	(20, 1, '4540', 'GT10', 'GaanTxt 10', '', 10, 9.36, 1, NULL, NULL, 0),
	(21, 1, '4540', 'GT20', 'GaanTxt 20', '', 20, 18.71, 1, NULL, NULL, 0),
	(22, 1, '4540', 'GA15', 'Gaan All-in-one 15', '', 15, 14.03, 1, NULL, NULL, 0),
	(23, 1, '4540', 'GA20', 'Gaan All-in-one 20', '', 20, 18.71, 1, NULL, NULL, 0),
	(24, 1, '4540', 'GA30', 'Gaan All-in-one 30', '', 30, 28.07, 1, NULL, NULL, 0),
	(25, 1, '4540', 'GU15', 'Gaan UnliTxt Plus 15', '', 15, 14.03, 1, NULL, NULL, 0),
	(26, 1, '4540', 'GU20', 'Gaan UnliTxt Plus 20', '', 20, 18.71, 1, NULL, NULL, 0),
	(27, 1, '4540', 'GU30', 'Gaan UnliTxt Plus 30', '', 30, 28.07, 1, NULL, NULL, 0),
	(28, 1, '4540', 'TOT10', 'Patok-O-Tex 10', '', 10, 9.36, 1, NULL, NULL, 0),
	(29, 1, '4540', 'T20', 'UnliTalkPlus20', '', 20, 18.71, 1, NULL, NULL, 0),
	(30, 1, '4540', 'T100', 'UnliTalkPlus100', '', 100, 93.56, 1, NULL, NULL, 0),
	(31, 2, '100', '10', 'Regular Load 10', '', 10, 9.55, 1, NULL, NULL, 0),
	(32, 1, '4540', 'GAANSURF10', 'Gaan Surf 10', '\'\'', 10, 9.37, 1, '2019-05-25 20:27:36', '2019-05-25 20:27:36', 0);
/*!40000 ALTER TABLE `product_codes` ENABLE KEYS */;

-- Dumping structure for table apiserver.userinfo
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE IF NOT EXISTS `userinfo` (
  `UserInfoID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `CompanyName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Interval` int(11) NOT NULL DEFAULT '1',
  `Credits` double NOT NULL DEFAULT '0',
  `IsRecurring` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`UserInfoID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.userinfo: ~2 rows (approximately)
DELETE FROM `userinfo`;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;
INSERT INTO `userinfo` (`UserInfoID`, `UserID`, `CompanyName`, `Interval`, `Credits`, `IsRecurring`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Union Bank', 1, 150000, 0, '2019-05-25 20:24:17', '2019-05-25 20:24:17'),
	(2, 3, 'PNB', 2, 200000, 0, '2019-05-25 22:03:30', '2019-05-25 22:03:30');
/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;

-- Dumping structure for table apiserver.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` int(11) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table apiserver.users: ~3 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `email`, `email_verified_at`, `password`, `is_active`, `remember_token`, `created_at`, `updated_at`, `type`, `last_login`, `is_deleted`) VALUES
	(1, 'admin', 'admin@gateway.com', NULL, '$2y$10$M7LThf8ImYChiRVdAAG48OdgDyD7T1AkVtkkbqV2mPw44cBxPCLGq', 1, NULL, '2019-05-25 20:23:31', '2019-05-25 20:23:31', 1, NULL, 0),
	(2, 'unionbank', 'admin@unionbank.com', NULL, '$2y$10$AjVfrAjzSatgaN21Vq94BO.UHtq4y6x/4aLRUrHjp09CTNISliQZ2', 1, 'HkNXiK2ZvTPLw3h64FuSsDgarCWtibjZFxHArk9wXrolU28474Ypm0kijNyk', '2019-05-25 20:24:17', '2019-05-25 20:55:14', 2, '2019-05-25 20:55:14', 0),
	(3, 'pnb', 'admin@pnb.com', NULL, '$2y$10$WUfW38AA4p3mn4WQt9Yy6.HKHxrdLe0s6rcq997F0EkijLejUdWpm', 1, NULL, '2019-05-25 22:03:30', '2019-05-25 22:09:03', 2, '2019-05-25 22:09:03', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
