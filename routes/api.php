<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group([
    'prefix' => 'account'
], function () {
    // Route::post('create', 'AccountController@store');
});

Route::group([
    'prefix' => 'transactions',
    'middleware' => 'auth:api'
], function () {
    Route::post('process', 'ClientTransactionController@process');
    Route::post('status/{id?}', 'ClientTransactionController@getTransactionStatus');
    Route::get('list/{recipient?}', 'ClientTransactionController@getByRecipient');
});

Route::post('login', ['uses' => 'Auth\LoginController@apilogin']);
Route::post('refresh/token', ['uses' => 'Auth\LoginController@apirefreshtoken']);
