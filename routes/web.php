<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', ['as' => 'home', 'middleware' => 'auth', 'uses' => 'HomeController@index']);

Route::group([
    'prefix' => 'users',
    'middleware' => 'auth',
], function(){
    Route::get('getfields', ['as' => 'user.getfields', 'uses' => 'UserController@getFields']);
});
  
//Using user controller -> for url naming
Route::group([
    'prefix' => 'account',
    'middleware' => ['auth'],
], function () {
    Route::get('edit/{id}', ['as' => 'account.edit', 'uses' => 'UserController@edit']);
    Route::get('register', ['as' => 'account.register', 'uses' => 'UserController@create']);
    Route::post('save', ['as' => 'account.save', 'uses' => 'UserController@store']);
    Route::get('/', ['as' => 'account.client.show', 'uses' => 'ClientController@initializeAccount']);
});

Route::group([
    'prefix' => 'payment',
    'middleware' => ['auth'],
], function () {
    Route::post('save', ['as' => 'payment.save', 'uses' => 'CreditPaymentController@store']);
    Route::get('list', ['as' => 'payment.list', 'uses' => 'CreditPaymentController@getList']);
    Route::get('client/{id}', ['as' => 'payment.showbyclient', 'uses' => 'CreditPaymentController@showByClient']);
});

Route::group([ 
    'prefix' => 'clients',
    'middleware' => ['auth'],
],function () {
    Route::get('/', ['as' => 'admin.clients', 'uses' => 'ClientController@index']);
    Route::get('getfields', ['as' => 'admin.client.getfields', 'uses' => 'ClientController@getFields']);
    Route::get('list', ['as' => 'admin.client.list', 'uses' => 'ClientController@list']);
    Route::get('{id?}', ['as' => 'admin.client.show', 'uses' => 'ClientController@show']);
    Route::get('initialize/{id}', ['as' => 'admin.client.edit', 'uses' => 'ClientController@edit']);
    Route::get('credit-history/{id?}', ['as' => 'admin.client.credit.history', 'uses' => 'ClientController@showCreditHistory']);
    Route::post('save', ['as' => 'admin.client.save', 'uses' => 'ClientController@store']);
    Route::post('deactivate', ['as' => 'admin.client.remove', 'uses' => 'ClientController@destroy']);
    Route::post('activate', ['as' => 'admin.client.activate', 'uses' => 'ClientController@activate']);
    Route::post('getcredentials', ['as' => 'admin.client.showcredentials', 'uses' => 'ClientController@showClientCredentials']);
    Route::post('allocationlist', ['as' => 'admin.client.allocation.list', 'uses' => 'ClientController@allocationTransactionList']);
    Route::post('adjustclientcredits', ['as' => 'admin.client.adjutscredits', 'uses' => 'ClientController@adjustClientCredits']);
});


Route::group([
    'prefix' => 'transactions',
    'middleware' => ['auth'],
], function () {
    Route::get('listbyclientallocation', ['as' => 'admin.client.allocation.list', 'uses' => 'ClientTransactionController@listByClientAllocation']);
    Route::post('gettransactionbystatus', ['uses' => 'ClientTransactionController@getTransactionByStatus']);
});

Route::group([
    'prefix' => 'credit',
    'middleware' => ['auth'],
], function () {
    Route::get('history', ['as' => 'admin.client.allocation.history', 'uses' => 'AllocationTransactionController@listView']);
});

Route::group([
    'prefix' => 'carrier',
    'middleware' => 'auth',
], function () {
    Route::get('list', ['as' => 'carrier', 'uses' => 'CarrierController@index']);
});
Route::group([ 
    'prefix' => 'gateway',
    'middleware' => ['auth'],
],function () {
    Route::get('/', ['as' => 'admin.gateway.index', 'uses' => 'GatewayController@index']);
    Route::post('save', ['as' => 'admin.gateway.save', 'uses' => 'GatewayController@store']);
    Route::post('delete', ['as' => 'admin.gateway.delete', 'uses' => 'GatewayController@delete']);
    Route::post('adjustbalance', ['as' => 'admin.gateway.adjust', 'uses' => 'GatewayController@adjustBalance']);
    Route::get('list', ['as' => 'admin.gateway.list', 'uses' => 'GatewayController@list']);
    Route::get('keyvalue', ['as' => 'admin.gateway.keyvalue', 'uses' => 'GatewayController@keyValue']);
    Route::get('initialize/{id}', ['as' => 'admin.gateway.edit', 'uses' => 'GatewayController@edit']);
});

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::group([
    'prefix' => 'productcode',
    'middleware' => 'auth',
], function () {
    Route::get('/', ['as' => 'admin.productcode', 'uses' => 'ProductCodeController@index']);
    Route::post('list', ['as' => 'admin.productcode.list', 'uses' => 'ProductCodeController@list']);
    Route::get('initialize/{id}', ['as' => 'admin.productcode.edit', 'uses' => 'ProductCodeController@edit']);
    Route::post('save', ['as' => 'admin.productcode.save', 'uses' => 'ProductCodeController@store']);
    Route::post('delete', ['as' => 'admin.productcode.delete', 'uses' => 'ProductCodeController@delete']);
});

Route::group([
    'prefix' => 'clientallocation',
    'middleware' => 'auth',
], function () {
    Route::get('getallbyclientid/{id}', ['as' => 'admin.clientallocation.getallbyclientid', 'uses' => 'ClientAllocationController@getAllByClient']);
    Route::get('getasoptions/{id}', ['as' => 'admin.clientallocation.getAsOptions', 'uses' => 'ClientAllocationController@getAsOptions']);
    Route::get('showall/{id}', ['as' => 'admin.clientallocation.showall', 'uses' => 'ClientAllocationController@getByClientID']);
    Route::get('getloadedcredit', ['as' => 'admin.clientallocation.getloadedcredit', 'uses' => 'ClientAllocationController@getLoadedCredits']);
    Route::get('currentallocation/{client_id}', ['uses' => 'ClientAllocationController@getCurrentAllocation']);
    Route::get('{id?}', ['as' => 'admin.clientallocation.showallbyclientid', 'uses' => 'ClientAllocationController@showByClient']);
});

Route::get('/passportmanager', function () {
    return view('passportmanager'); 
});   


Route::get('test', ['uses' => 'ClientTransactionController@test']);
