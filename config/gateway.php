<?php

return [
    'carriers' => [
        '0' => 'System',
        '1' => 'Smart',
        '2' => 'Globe',
        '3' => 'Sun'
    ],
    'type' => [
        '1' => 'GSM'
    ]
];