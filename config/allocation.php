<?php

return [
    'interval' => [
        '0' => 'N/A',
        '1' => 'Daily',
        '2' => 'Weekly',
        '3' => 'Monthly'
    ]
];